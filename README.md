# YANOMAPP

YANOMAPP comienza como un sistema registro de datos de salud del Programa de eliminación oncocercosis activado por cada paciente por su huella digital para garantizar el éxito del tratamiento individualizado por paciente y un registro organizado por comunidad.

## Requerimientos

* [Node.js](https://nodejs.org) Se sugiere la versión 8.9.0 o superior.
* [npm](https://www.npmjs.com/):  Se recomienda la versión 5.5.1 o superior.
* [NativeScript](): Se recomienda usar la última versión disponible (4.1.0).

```bash
npm install -g nativescript
```

### Android 

* Sistema operativo Windows, Linux o Mac. 
* Java development kit 8 o superior.
* Al menos 4 GB de espacio en disco duro.
* Al menos 4 GB disponibles de RAM.
* Dispositivo Android de nivel 21 (5.0, Lollipop) o superior.

### iOS

* Sistema operativo Mac.
* Xcode 7 o superior.
* Al menos 5 GB de espacio en disco duro.
* Al menos 4 GB disponibles de RAM.
* Dispositivo iOS 9 o superior.


> Para mayor información visite:
> * [docs.nativescript.org](https://docs.nativescript.org/start/quick-setup)

## Ejecución

Cree su espacio de trabajo:

```bash
mkdir -p ~/workspace/workspace-nativescript
git clone http://git.sigis.com.ve/yanomap/yanomap-mobile.git
```
Instale las dependencias npm:

```bash
cd ~/workspace/workspace-nativescript/yanomap-mobile/
npm install
tns platform add <platform>
tns prepare <platform>
tns run android
```

> Donde **< platform >** se refiere a la plataforma que se usará, puede ser **android** o **ios**

### Iconos
Si dispone de un ícono y desea generar su equivalente para cada una de las resoluciones disponibles, puede ejecutar el siguiente comando, siendo **< path >** la dirección hacia la imagen:

```bash
tns resources generate icons <path>
```

#### badge 
Puede agregar algún badge que define si el app esta en modo alfa, beta o incluso definir la versión. para ello puede hacer uso de la herramienta  de línea de comando [`badge`](https://github.com/HazAT/badge). Puede instalarla y usar de la siguiente forma:
```bash
sudo apt-get install rubygems build-essential ruby-dev libcurl3-dev
sudo gem install badge
```
Luego desde la raíz del proyecto ejecute:

```bash
badge --alpha badge --glob ""/**/drawable*/*icon.{png,PNG}""
```
> Para mayor información sobre  ésta herramienta visite:
> * [github badge project](https://github.com/HazAT/badge)

### Splash
Similarmente, si desea generar las imágenes para splash screen, puede emplear el siguiente comando:

```bash
tns resources generate splashes <path>
```
### Devices

Puede ejecutar su proyecto utilizando Android Studio, o bien con el siguiente comando:

```bash
tns run --device <deviceId>
```

Donde **< platform >** es la plataforma que se instalará (android o ios) y **< deviceId >** representa el identificador del dispositivo en donde se ejecutará la aplicación. Si desea listar sus dispositivos, puede utilizar el siguiente comando:

```bash
tns device <platform> --available-devices
```

Con esto, obtendrá el listado de emuladores disponibles, así como los dispositivos conectados.

### Packages
Si desea generar un APK en modo debug sin firma, puede ejecutar el siguiente comando:
```bash
tns build android
```

Por otro lado, si desea un APK firmado, ejecute lo siguiente:
```bash
tns build android --release --key-store-path < key-store-path > --key-store-password < key-store-password > --key-store-alias < key-store-alias > --key-store-alias-password < key-store-alias-password > --copy-to < path-to-apk >
```

Donde:
* **< key-store-path >** es la ubicación del archivo keystore.
* **< key-store-password >** es la contraseña asociada al keystore.
* **< key-store-alias >** es el alias del keystore.
* **< key-store-alias-password >** es la contraseña del alias del keystore.
* **< path-to-apk >** se refiere al nombre y ubicación del APK generado.

## Estructura

Posee la siguiente estructura:
```bash
    .
    ├── app                    #Carpeta que contiene el proyecto NativeScript. Área de trabajo.
    |   ├── app.component      #Archivos raíz a cargar. No hace falta modificar.
    |   ├── app.css            #Estilos de alcance global.
    |   ├── app.module.ts      #Indica los components, servicios y módulos a usar en la aplicación.
    |   ├── app.routing.ts     #Posee las rutas de la aplicación móvil.
    |   ├── App_Resources      #Contiene los recursos propios de cada plataforma, tales como imágenes, gradle, manifiesto, etc.
    |   ├── fonts              #Carpeta con las fuentes a ser empleadas en la aplicación.
    |   ├── models             #Carpeta con todos los modelos a ser empleados.
    |   ├── services           #Carpeta con todos los servicios a utilizar.
    |   ├── i18n               #Configuración de lenguajes
    |   └── components         #Carpeta con todos los componentes creados.
    ├── hooks                  #Scripts de automatización de tareas.
    ├── platforms              #Carpetas con traducción del proyecto según plataforma. Se ignora su subida.
    ├── .gitignore             #Lista los archivo que no se suben al repositorio.
    ├── README.md              #Este archivo.
    ├── package-lock.json      #Representación del árbol de dependencias a instalar.
    ├── package.json           #Dependencias de Node.js/NativeScript.
    └── tsconfig.json          #Indicador de raíz de proyecto NativeScript.
```

## Plugins

* [nativescript-hf4000p-fingerprint](http://git.sigis.com.ve/yanomap/nativescript-hf4000p-fingerprint) Permite la comunicación con el dispositivo capta huella
* [nativescript-angular](https://github.com/NativeScript/nativescript-angular): Permite aplicar la notación de Angular en NativeScript.
* [nativescript-geolocation](https://github.com/NativeScript/nativescript-geolocation): Provee funciones de geolocalización.
* [nativescript-theme-core](https://github.com/NativeScript/theme) Provee estilos adicionales.
* [nativescript-ui-dataform](https://www.npmjs.com/package/nativescript-ui-dataform): Provee facilidades para la inclusión de formularios.
* [nativescript-ui-sidedrawer](https://www.npmjs.com/package/nativescript-ui-sidedrawer) Permite agregar un menú lateral a la aplicación.
* nativescript-ui-list: Permite el manejo de listas.
* nativescript-cardview: Provee el componente de tarjetas.
* [nativescript-couchbase](https://github.com/couchbaselabs/nativescript-couchbase) Permite crear base de datos locales de couchbase y conectar por Sync Gateway al servidor.
* nativescript-localstorage: Permite realizar almacenamiento local de datos.
* nativescript-toast: Permite agregar mensajes temporales (toast).
* ng2-translate: Plugin de internacionalización.
* nativescript-camera: Solicita el uso de la cámara.
* [nativescript-mapbox](https://github.com/eddyverbruggen/nativescript-mapbox): Plugin que permite la descarga de mapas para posterior visualización.
* [nativescript-floatingactionbutton](https://github.com/bradmartin/nativescript-floatingactionbutton): Permite agregar floating buttons en la aplicación.
* [nativescript-dev-version](https://github.com/jacargentina/nativescript-dev-version): Permite el uso de la versión del package.json para crear las versiones de la aplicación en Android y iOS.
* [nativescript-appversion](https://github.com/EddyVerbruggen/nativescript-appversion): Permite obtener el identificador y versión de la aplicación.
* [nativescript-hf4000p-fingerprint](http://git.sigis.com.ve/yanomap/nativescript-hf4000p-fingerprint): Es un plugin interno desarrollado en SIGIS, el cual permite realizar la comparación y registro de huellas dactilares.
* [nativescript-local-notifications](https://www.npmjs.com/package/nativescript-local-notifications): Permite el manejo de notificaciones locales.

## Troubleshooting

* **nativescript-ui-dataform**: Se sugiere usar la versión 3.6.0, pues las más recientes no son del todo estables y pueden provocar errores durante la carga.
* **nativescript-hf4000p-fingerprint**: Debe ser descargado desde el repositorio, hacerle su build y luego agregarlo al proyecto. Nótese que por el momento no se ha implementado la versión para iOS.

```bash
#Desde el directorio raíz del plugin
cd src
npm run build
#Desde la raíz de este proyecto
tns plugin add <pathToPluginSrc>
#Posteriormente, agregar la plataforma y realizar el prepare
```

## Autores

* [Abdalah Masrie](abdalah.masrie@sigis.com.ve)

## Copyright

(C) Soluciones Integrales GIS, C.A.
