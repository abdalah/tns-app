import { Component } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { Page } from "tns-core-modules/ui/page";
import { RouterExtensions } from "nativescript-angular/router";
import { TranslateService } from 'ng2-translate';
import { CouchbaseService } from "../../services/couchbase.service";
import { ListViewEventData, RadListView } from "nativescript-ui-listview";
import { BaseComponent } from "../base/base.component";
import * as moment from 'moment';

@Component({
    selector: "consultation-list",
    moduleId: module.id,
    styleUrls: ["consultation-list.component.css"],
    templateUrl: "./consultation-list.component.html"
})

/**
    Clase que posee las funciones asociadas a la lista de consultas para un paciente
    @class ConsultationListComponent
    @author Abdalah Masrie <abdalah.masrie@sigis.com.ve>
    @copyright SIGIS Soluciones Integrales GIS, C.A
    created on 2018/07/23
*/
export class ConsultationListComponent extends BaseComponent{
    /**
        Listado de consultas
        @property _consultations
        @type {any}
    */
    private _consultations: any = [];

    /**
        Método constructor
        @method constructor
        @param {Router} routes Objeto que permite navegar entre vistas
        @param {Page} page Referencia a la página
        @param {ActivatedRoute} activatedRoute Objeto que permite obtener los parámetros de query
        @param {RouterExtensions} routerExtensions Objeto que permite navegar por la pila de vistas
        @param {TranslateService} translate Objeto que permite traducir elementos
    */
    public constructor(
        router: Router,
        private page: Page,
        activatedRoute: ActivatedRoute,
        routerExtensions: RouterExtensions,
        translate: TranslateService
    ) {
        super(routerExtensions, translate, router, activatedRoute, null);

        this.page.on(Page.navigatingToEvent, (() => {
            this.initComponent();
        }));
    }

    /**
        Método que se ejecuta al entrar al componente, independientemente de si es por primera vez o por efecto de back
        @method initComponent
    */
    private initComponent(){
        this._consultations = [];
        let cb = new CouchbaseService();
        //obtener el id del paciente
        const patientId = this.activatedRoute.snapshot.params["id"];
        //obtener las consultas asociadas al paciente
        cb.getDocumentDB(patientId).then(patient =>{
            let consultationsList = patient["data"]["con_pac"];
            this.showLoadingIndicator(this.makeTranslation("loading"));
            this.recursiveConsultationList(0, cb, consultationsList);
        });
    }

    /**
        Método que obtiene datos de la lista de consultas del paciente
        @method recursiveConsultationList
        @param {number} position Índice de elemento recorrido
        @param {any} cb Instancia de couchbase
        @param {any} consultationsList Lista de consultas del paciente
    */
    public recursiveConsultationList(position, cb, consultationsList){
        if(position < consultationsList.length){
            cb.getDocumentDB(consultationsList[position]).then(consultation => {
                let json = {
                    "_id": consultation._id,
                    "date": moment(consultation["data"]["fec_cre"]).utcOffset(0,true).local().format("DD/MM/YYYY HH:mm")
                }
                cb.getDocumentDB(consultation["data"]["doc_id"]).then(doctor => {
                    let msg = this.makeTranslation("by_doctor");
                    json["doctor"] = msg + doctor.data.nom_usu + " " + doctor.data.ape_usu;
                    this._consultations.push(json);
                    this.recursiveConsultationList(position+1, cb, consultationsList);
                });    
            });
        } else{
            this.hideLoadingIndicator();
        }
    }

    /**
        Función que retorna el listado de consultas
        @method getConsultations
        @return {any}
    */
    public getConsultations(): any {
        return this._consultations;
    }

    /**
        Método que es disparado por un evento de selección de elemento. Redirecciona al formulario de la consulta seleccionada
        @method onItemSelected
        @property {ListViewEventData} args Contenido asociado al elemento
    */
    public onItemSelected(args: ListViewEventData){
        const patientId = this.activatedRoute.snapshot.params["id"];
        const listView = args.object as RadListView;
        const selectedItems = listView.getSelectedItems() as Array<any>;
        listView.deselectItemAt(args.index);
        this.navigate("/consultation/"+patientId+"/"+selectedItems[0]._id);
    }
}
