import { Component } from '@angular/core';
import { BaseComponent } from "../base/base.component";
import { Router } from '@angular/router';
import { TranslateService } from 'ng2-translate';
import { RouterExtensions } from 'nativescript-angular/router';
import { Mapbox, MapStyle } from "nativescript-mapbox";
import { Page } from "tns-core-modules/ui/page";
import { Config } from '~/config/config';
import * as localStorage from 'nativescript-localstorage';
import * as dialogs from "tns-core-modules/ui/dialogs";
import { Color } from "tns-core-modules/color";
import { View } from 'ui/core/view';
import * as Toast from 'nativescript-toast';
import { android, on as applicationOn, AndroidApplication, AndroidActivityBundleEventData } from "tns-core-modules/application";
import { calcBindingFlags, markParentViewsForCheck } from '@angular/core/src/view/util';
import { isAndroid } from "platform";
import { confirm } from "tns-core-modules/ui/dialogs";
import * as SocialShare from "nativescript-social-share";
import { CouchbaseService } from "../../services/couchbase.service";

@Component({
    moduleId: module.id,
    selector: 'app-map',
    templateUrl: './map.component.html'  
})

/**
    Componente para la administración del mapa.
    @class MapComponent
    @author Robert Bruno <robert.bruno@sigis.com.ve>
*/
export class MapComponent extends BaseComponent {

    /** 
        Valores almacenados del mapa previos a una pausa
        @property pauseValues
        @type {any}
    */
    private pauseValues: any;

    /** 
        Indicador de inicio del Activity
        @property start
        @type {boolean}
    */
    private start: boolean;

    /** 
        Indicador de permiso de ubicación
        @property permission
        @type {boolean}
    */
    private permission: boolean;

    /**
        Última opción seleccionada de capas
        @property layerSelected
        @type {number}
    */
    private layerSelected: number;

    /**
        Lista de áreas
        @property areaList
        @type {any}
    */
    private areaList = [];

    /**
        Lista de subáreas
        @property subAreaList
        @type {any}
    */
    private subAreaList = [];

    /**
        Lista de comunidades
        @property communityList
        @type {any}
    */
    private communityList = [];

    /**
        Lista de consultas
        @property consultationList
        @type {any}
    */
    private consultationList = [];

    /**
        Lista de alertas
        @property alertList
        @type {any}
    */
    private alertList = [];

    /**
        Indicador de elementos con ubicación
        @property locationFound
        @type {boolean}
    */
    private locationFound = false;

    /**
        Indicador de mapa iniciado
        @property mapStarted
        @type {boolean}
    */
    private mapStarted = false;

    /**
        Mensaje a compartir proveniente del último popup seleccionado
        @property lastPopup
        @type {string}
    */
    private lastPopup: string;

    /**
        Última posición almacenada
        @property lastPosition
        @type {any}
    */
    private lastPosition = {lat: null, lng: null};

    /**
        Instancia del servicio de Couchbase
        @property couchbaseService
        @type {CouchbaseService}
    */
    private couchbaseService: CouchbaseService;

    /**
        Método constructor
        @constructor
        @param {Page} page Referencia a la página
        @param {TranslateService} translate Objeto que permite traducir elementos
        @param {Router} router Objeto que permite navegar entre vistas
        @param {RouterExtensions} routerExtensions Objeto que permite navegar por la pila de vistas
    */
    constructor(private page: Page, translate: TranslateService, router: Router, routerExtensions: RouterExtensions) {
        super(routerExtensions, translate, router, null, null);

        //Adding back event
        this.page.on(Page.loadedEvent, _ => {
            if (android) {
                android.on(AndroidApplication.activityBackPressedEvent, (this.backMapBefore).bind(this));
            }
        });
        
        //Remove back event
        this.page.on(Page.unloadedEvent, _ => {
            if (android) {
                android.removeEventListener(AndroidApplication.activityBackPressedEvent);
            }
        });

        //Exit event
        this.page.on('navigatingFrom', _ => {
            if(isAndroid){
                    android.off(AndroidApplication.activityPausedEvent);
                    android.off(AndroidApplication.activityResumedEvent);
            }
            if(this.mapbox){
                this.mapbox.destroy();
            }
        });

    	if(isAndroid){
            //Pause event
            android.on(AndroidApplication.activityPausedEvent, _ => {
                console.log("pause")
                if(this.mapStarted){
                    this.getMapValues(()=>{
                        if(this.mapbox){
                            this.mapbox.destroy();
                        }
                    });
                }
            });
        
            //Resume event
            android.on(AndroidApplication.activityResumedEvent, _ => {
                console.log("resume")
                if(this.mapStarted){
                    this.initComponent(false);
                }
            });
	    }

        //Initial event
        this.page.on(Page.navigatingToEvent, (() => {
            this.initComponent(true);
        }));
        this.start = true;
    }


    /**
        Método que se ejecuta al entrar al componente, independientemente de si es por primera vez o por efecto de back
        @method initComponent
        @property {boolean} isNew Indicador de carga de mapa por navegación o resume
    */
    public initComponent(isNew = false){
        let pauseValues = localStorage.getItem(Config.storage.map_config);
        this.pauseValues = pauseValues ? JSON.parse(pauseValues) : {};
        this.layerSelected = null;
        this.mapbox = new Mapbox();
        this.couchbaseService = new CouchbaseService();
        setTimeout( () => {
            var actionbar = <View>this.page.getViewById("actionBar");
            var height = actionbar.getActualSize().height;
            if(this.permission || (!this.permission && !this.start)){
                this.doShow(height, isNew);
            }else{
                this.getPermission( () => {
                    this.doShow(height, isNew);
                });
            }
        },500)
    }

    /** 
        Método que se encarga de recorrer las áreas y generar sus capas si 
        corresponde con el nivel solicitado
        @method iterateOverAreas
        @param {number} level Nivel de distribución geográfica solicitada, 
            siendo 1 áreas, 2 subáreas y 3 comunidades
    */
    iterateOverAreas(level){
        let flag = false;
        let list: any;
        this.mapbox.removePolygons();
        this.mapbox.removeMarkers();
        switch(level){
            case 1:
                list = localStorage.getItem(Config.storage.map_areas);
                break;
            case 2:
                list = localStorage.getItem(Config.storage.map_subareas);
                break;
            case 3:
                list = localStorage.getItem(Config.storage.map_communities);
                break;
        }
        list = list ? JSON.parse(list) : null;
        if(list && list.length > 0){
            flag = true;
            this.genericIteration(list);
        }
        if(!flag){
            this.couchbaseService.getDocumentDB(Config.id.areas).then(areasList => {
                if(areasList && areasList["data"]){
                    this.recursiveIterationArea(0, areasList, level);
                }else{
                    this.hideLoadingIndicator();
                }
            });
        }
    }

    /**
        Método que itera sobre una lista para insertar sus elementos
        @method genericIteration
        @param {any} list Lista de elementos sobre el cual se hará la iteración 
    */
    public genericIteration(list){
        let format = localStorage.getItem(Config.storage.format) == "degrees";
        let msg;
        for(let i = 0; i < list.length; i++){
            if(list[i].polygon){
                //caso de polígono, no tiene geometría específica (sin formato)
                this.coordinatesToPointList(list[i].polygon.name, list[i].polygon.msg,
                    list[i].polygon.color, list[i].polygon.coordinates, list[i].polygon.marker
                );
            }else{
                //caso de punto (debe incluir formato)
                msg = format ? list[i].point.degree : list[i].point.decimal ;
                this.lastPosition.lat = list[i].point.coordinates[1];
                this.lastPosition.lng = list[i].point.coordinates[0];
                this.addMarker(list[i].point.coordinates, msg, list[i].point.marker, list[i].point.unique, true);
            }
        }
        this.mapSetCenter(this.lastPosition.lat, this.lastPosition.lng);
        this.hideLoadingIndicator();
    }

    /**
        Método que cada área para obtener sus coordenadas
        @method recursiveIterationArea
        @param {number} positionArea Posición del arreglo Area que se recorre
        @param {any} areasList Lista de áreas
        @param {number} level Nivel de distribución geográfica solicitada, 
            siendo 1 áreas, 2 subáreas y 3 comunidades
    */
    public recursiveIterationArea(positionArea, areasList, level){
        let msgArea;
        if(positionArea < areasList["data"].length){
            let documentAreas = areasList["data"][positionArea];
            msgArea = this.makeTranslation("first_procedence") + ": " + 
                documentAreas["nom_are_geo"].replace(/_/g, " ");
            if(level == 1){
                this.couchbaseService.getDocumentDB(documentAreas["the_geom"]).then(geomAreaDoc => {
                    if(geomAreaDoc["data"] && geomAreaDoc["data"]["the_geom"]){
                        var coordinates = geomAreaDoc["data"]["the_geom"]["coordinates"];
                        this.areaList.push({polygon: {name: documentAreas["nom_are_geo"], msg: msgArea,
                        color: "#0033cc", coordinates: coordinates, marker: Config.markers.blue}});
                        this.coordinatesToPointList(documentAreas["nom_are_geo"], msgArea,
                            "#0033cc", coordinates, Config.markers.blue
                        );
                        this.recursiveIterationArea(positionArea+1, areasList, level);
                    }
                });
            }else{
                this.iterateOverSubAreas(level, areasList, documentAreas, msgArea, positionArea);
            }
        }else{
            if(this.areaList.length > 0){
                localStorage.setItem(Config.storage.map_areas, JSON.stringify(this.areaList));
                this.areaList = [];
            }
            if(this.subAreaList.length > 0){
                localStorage.setItem(Config.storage.map_subareas, JSON.stringify(this.subAreaList));
                this.subAreaList = [];
            }
            if(this.communityList.length > 0){
                localStorage.setItem(Config.storage.map_communities, JSON.stringify(this.communityList));
                this.communityList = [];
            }
            this.mapSetCenter(this.lastPosition.lat, this.lastPosition.lng);
            this.hideLoadingIndicator();
        }
    }

    /** 
        Método que se encarga de recorrer las subáreas y generar sus capas si 
        corresponde con el nivel solicitado
        @method iterateOverSubAreas
        @param {number} level Nivel de distribución geográfica solicitada, 
            siendo 2 subáreas y 3 comunidades
        @param {any} areasList Listado de áreas
        @param {any} DocumentAreas Documento específico de áreas
        @param {string} msgArea Mensaje de área
        @param {number} positionArea Posición del arreglo Area que se recorre
    */
    iterateOverSubAreas(level, areasList, documentAreas, msgArea, positionArea){
        this.getChild(areasList, 
            documentAreas["id_are_geo"], "id_are_geo", "lis_sub_are", (subareasList)=>{
                if(subareasList && subareasList["data"]){
                    this.recursiveIterationSubArea(positionArea, 0, areasList, subareasList, level, msgArea);
                }else{
                    this.recursiveIterationArea(positionArea+1, areasList, level);
                }
            });
    }

    /**
        Método que cada subárea para obtener sus coordenadas
        @method recursiveIterationSubArea
        @param {number} positionArea Posición del arreglo Area que se recorre
        @param {number} positionSubArea Posición del arreglo SubArea que se recorre
        @param {any} areasList Lista de áreas
        @param {any} subareasList Lista de subáreas
        @param {number} level Nivel de distribución geográfica solicitada, 
            siendo 1 áreas, 2 subáreas y 3 comunidades
        @param {string} msgArea Mensaje de área
    */
    public recursiveIterationSubArea(positionArea, positionSubArea, areasList, subareasList, level, msgArea){
        let msgSubarea;
        if(positionSubArea < subareasList["data"].length){
            let documentSubAreas = subareasList["data"][positionSubArea];
            msgSubarea = this.makeTranslation("second_procedence") + ": " + 
                documentSubAreas["nom_sub_are"].replace(/_/g, " ");
            if(level == 2){
                this.couchbaseService.getDocumentDB(documentSubAreas["the_geom"]).then(geomSubAreaDoc => {
                    if(geomSubAreaDoc["data"] && geomSubAreaDoc["data"]["the_geom"]){
                        let coordinates = geomSubAreaDoc["data"]["the_geom"]["coordinates"];
                        let msg = isAndroid ? msgArea + "\n" + msgSubarea : msgSubarea;
                        this.subAreaList.push({polygon: {name: documentSubAreas["nom_sub_are"], msg: msg,
                        color: "#f4aa42", coordinates: coordinates, marker: Config.markers.yellow}});
                        this.coordinatesToPointList(documentSubAreas["nom_sub_are"],
                            msg, "#f4aa42", coordinates, 
                            Config.markers.yellow
                        );
                        this.recursiveIterationSubArea(positionArea, positionSubArea+1, areasList, subareasList, level, msgArea);
                    }
                });
            }else{
                this.iterateOverCommunities(positionArea, positionSubArea, 0, 
                    areasList, subareasList, documentSubAreas, msgArea, msgSubarea, level);
            }
        }else{
            this.recursiveIterationArea(positionArea+1, areasList, level);
        }
    }

    /** 
        Método que se encarga de recorrer las comunidades y generar sus puntos
        @method iterateOverCommunities
            siendo 2 subáreas y 3 comunidades
        @param {number} positionArea Posición del arreglo Area que se recorre
        @param {number} positionSubArea Posición del arreglo SubArea que se recorre
        @param {number} positionCommunity Posición del arreglo Comunidad que se recorre
        @param {any} areasList Lista de áreas
        @param {any} subareasList Listado de subáreas
        @param {any} documentSubAreas Documento específico de subáreas
        @param {string} msgArea Mensaje de área
        @param {string} msgSubarea Mensaje de subárea
        @param {number} level Nivel de distribución geográfica solicitada, 
            siendo 1 áreas, 2 subáreas y 3 comunidades
    */
    iterateOverCommunities(positionArea, positionSubArea, positionCommunity, areasList, subareasList, documentSubAreas, msgArea, msgSubarea, level){
        this.getChild(subareasList, 
            documentSubAreas["id_sub_are"], "id_sub_are", "lis_com", (communitiesList)=>{
                if(communitiesList && communitiesList["data"]){
                    this.recursiveIterationCommunity(positionArea, positionSubArea, positionCommunity, areasList, subareasList, communitiesList, level, msgArea, msgSubarea);
                }
            });
    }

    /**
        Método que cada comunidad para obtener sus coordenadas
        @method recursiveIterationCommunity
        @param {number} positionArea Posición del arreglo Area que se recorre
        @param {number} positionSubArea Posición del arreglo SubArea que se recorre
        @param {number} positionCommunity Posición del arreglo Comunidad que se recorre
        @param {any} areasList Lista de áreas
        @param {any} subareasList Lista de subáreas
        @param {any} communitiesList Lista de comunidades
        @param {number} level Nivel de distribución geográfica solicitada, 
            siendo 1 áreas, 2 subáreas y 3 comunidades
        @param {string} msgArea Mensaje de área
        @param {string} msgSubarea Mensaje de subárea
    */
    public recursiveIterationCommunity(positionArea, positionSubArea, positionCommunity, areasList, subareasList, communitiesList, level, msgArea, msgSubarea){
        let msgCommunity;
        if(positionCommunity < communitiesList["data"].length){
            let documentCommunities = communitiesList["data"][positionCommunity];
            if(documentCommunities["ubi_com"]){
                msgCommunity = this.makeTranslation("community")+ ": " + 
                    documentCommunities["nom_com"].replace(/_/g, " ");
                let coordinates = documentCommunities["ubi_com"]["coordinates"];
                this.lastPosition.lat = coordinates[1];
                this.lastPosition.lng = coordinates[0];
                let gradeFormat = isAndroid ? msgArea + "\n" + 
                    msgSubarea + "\n" + msgCommunity + "\n(" + 
                    this.transformDecimal(coordinates[0], false, 2) + 
                    "," + this.transformDecimal(coordinates[1], true, 2) + 
                    ")" : msgCommunity;
                let decimalFormat = isAndroid ? msgArea + "\n" + 
                    msgSubarea + "\n" + msgCommunity + "\n(" + 
                    coordinates[0] + "," + coordinates[1] + 
                    ")" : msgCommunity;
                let msg = localStorage.getItem(Config.storage.format) == "degrees" ? gradeFormat : decimalFormat;
                this.communityList.push({point: {coordinates: coordinates, degree: gradeFormat, decimal: decimalFormat, marker: Config.markers.red, unique: false}});
                this.addMarker(
                    coordinates, msg, Config.markers.red, false, true
                );
            }
            this.recursiveIterationCommunity(positionArea, positionSubArea, positionCommunity+1, areasList, subareasList, communitiesList, level, msgArea, msgSubarea);
        }else{
            this.recursiveIterationSubArea(positionArea, positionSubArea+1, areasList, subareasList, level, msgArea);
        }
    }

    /** 
        Método que busca las consultas locales y genera marcadores correspondientes
        @method iterateOverConsultations
    */
    public iterateOverConsultations(){
        this.mapbox.removePolygons();
        this.mapbox.removeMarkers();
        //obtengo el listado de id consultas locales
        let list = localStorage.getItem(Config.storage.map_consultations);
        list = list ? JSON.parse(list) : null ;
        if(list && list.length > 0){
            this.genericIteration(list);
        }else{
            this.couchbaseService.getDocumentDB(Config.id.local_patients).then(doc => {
                let list = doc["consultations"];
                if(!list.length){
                    Toast.makeText(this.makeTranslation("no_consultations")).show();
                    this.hideLoadingIndicator();
                }else{
                    this.couchbaseService.getDocumentDB(Config.id.areas).then(areas =>{
                        //recorro la lista
                        this.recursiveListConsultationMarkers(list, areas, list.length == 1);
                    });
                }
            });
        }
    }

    /** 
        Método que busca las alertas locales y genera marcadores correspondientes
        @method iterateOverAlerts
    */
    public iterateOverAlerts(){
        this.mapbox.removePolygons();
        this.mapbox.removeMarkers();
        let list = localStorage.getItem(Config.storage.map_alerts);
        list = list ? JSON.parse(list) : null ;
        if(list && list.length > 0){
            this.genericIteration(list);
        }else{
            this.couchbaseService.getDocumentDB(Config.id.local_observations).then(doc => {
                let list = doc["data"];
                if(!list.length){
                    Toast.makeText(this.makeTranslation("no_alerts")).show();
                    this.hideLoadingIndicator();
                }else{
                    this.recursiveListAlertMarkers(list, list.length == 1);
                }
            });
        }
    }

    /**
        Método que recorre recursivamente la lista de observaciones
        @method recursiveListAlertMarkers
        @param {any} list Lista de alertas
        @param {boolean} unique Indicador de elemento único
        @param {any} location Última ubicación
    */
    public recursiveListAlertMarkers(list, unique, location = null){
        if(list.length > 0){
            let elem = list.pop();
            if(elem.obs_loc){
                this.locationFound = true;
                let type = elem.obs_typ == 1 ? this.makeTranslation("obs_log") : (elem.obs_typ == 2 ? this.makeTranslation("obs_epi") : this.makeTranslation("obs_pob") );
                let marker = elem.obs_typ == 1 ? Config.markers.yellow : (elem.obs_typ == 2 ? Config.markers.red : Config.markers.green );
                let msg = elem.obs_msg.length > 20 ? (elem.obs_msg).substring(0,20) + "..." : elem.obs_msg;
                let gradeFormat = type + ": " + msg + "\n(" + 
                    this.transformDecimal(elem.obs_loc.longitude, false, 2) + 
                    "," + this.transformDecimal(elem.obs_loc.latitude, true, 2) + ")";
                let decimalFormat = type + ": " + msg + "\n(" + 
                    elem.obs_loc.longitude + "," + elem.obs_loc.latitude + ")";
                let fullMsg = localStorage.getItem(Config.storage.format) == "degrees" ? gradeFormat : decimalFormat;
                this.alertList.push({point: {coordinates: [elem.obs_loc.longitude, elem.obs_loc.latitude], 
                    degree: gradeFormat, decimal: decimalFormat, marker: marker, unique: unique}});
                this.addMarker([
                    elem.obs_loc.longitude,
                    elem.obs_loc.latitude
                ], fullMsg , marker, unique, true);
            }
            this.recursiveListAlertMarkers(list, unique, elem);
        }else{
            if(!this.locationFound){
                Toast.makeText(this.makeTranslation("no_alerts")).show();
            }else{
                localStorage.setItem(Config.storage.map_alerts, JSON.stringify(this.alertList));
                this.locationFound = false;
            }
            if(location){
                this.mapSetCenter(location.obs_loc.latitude, location.obs_loc.longitude);
            }
            this.hideLoadingIndicator();
        }
    }

    /**
        Método que centra el mapa
        @method mapSetCenter
        @param {any} lat Latitud
        @param {any} lng Longitud
    */
    public mapSetCenter(lat, lng){
        this.mapbox.setCenter({
            lat: parseInt(lat),
            lng: parseInt(lng),
            animated: true
        });
    }

    /**
        Método que recorre la lsita de consultas no sincronizadas de forma recursiva
        @method recursiveListConsultationMarkers
        @param {any} list Lista de consultas
        @param {any} areas Lista de áreas
        @param {boolean} unique Inidcador de elemento único
        @param {any} location Última ubicación
    */
    public recursiveListConsultationMarkers(list, areas, unique, location = null){
        let gradeFormat, decimalFormat, title;
        if(list.length > 0){
            let matches = 1;
            let toDelete = [];
            let communitiesCount = [];
            //obtengo el id del último elemento de la lista
            let elem = list.pop();
            this.couchbaseService.getDocumentDB(elem).then(document => {
                if(document["data"]["loc_con"]){
                    this.locationFound = true;
                    //obtengo el paciente asociado
                    this.couchbaseService.getDocumentDB(document["data"]["pac_id"]).then(patient => {
                        //obtengo el área del paciente
                        this.getCommunity(patient, areas, ["are_geo_pac", "sub_are_pac", "id_com"], community => {
                            communitiesCount.push({
                                community: community["nom_com"],
                                count: 1
                            });
                            //título base
                            let consultationPosition = document["data"]["loc_con"];
                            gradeFormat = this.makeTranslation("name") + ": " + patient["data"]["nom_pac"] + "\n" + 
                                this.makeTranslation("community") + ": " + community["nom_com"] + 
                                "\n(" + this.transformDecimal(consultationPosition.longitude, false, 2) + 
                                "," + this.transformDecimal(consultationPosition.latitude, true, 2) + ")";
                            decimalFormat = this.makeTranslation("name") + ": " + patient["data"]["nom_pac"] + "\n" + 
                                this.makeTranslation("community") + ": " + community["nom_com"] + 
                                "\n(" + consultationPosition.longitude + 
                                "," + consultationPosition.latitude + ")";
                            for(var item in consultationPosition){
                                consultationPosition[item] = Number(consultationPosition[item]);
                            }
                            //para cada una de las otras consultas, se debe verificar si están cerca
                            this.recursiveCheckConsultationCloser(areas, 0, list, consultationPosition, matches, toDelete, communitiesCount, (newCommunitiesCount, matches) => {
                                //se eliminan las consultas que estaban cerca
                                list = list.filter( element => {
                                    return toDelete.indexOf(element) === -1;
                                })
                                //se obtiene la comunidad predominante, si aplica
                                let predominant;
                                for(var i = 0; i < newCommunitiesCount.length; i++){
                                    if(!predominant || predominant.count < newCommunitiesCount[i].count){
                                        predominant = newCommunitiesCount[i];
                                    }
                                }
                                if(matches > 1){
                                    //si hay cercanías, el título debe cambiar
                                    gradeFormat = isAndroid ? this.makeTranslation("total_consultations") + matches + "\n" +
                                        this.makeTranslation("predominant_community") + predominant.community + "\n" + 
                                        "(" + this.transformDecimal(consultationPosition.longitude, false, 2) + "," + 
                                        this.transformDecimal(consultationPosition.latitude, true, 2) + ")": 
                                        this.makeTranslation("predominant_community") + predominant.community;
                                    decimalFormat = isAndroid ? this.makeTranslation("total_consultations") + matches + "\n" +
                                        this.makeTranslation("predominant_community") + predominant.community + "\n" + 
                                        "(" + consultationPosition.longitude + "," + 
                                        consultationPosition.latitude + ")": 
                                        this.makeTranslation("predominant_community") + predominant.community;
                                }
                                //creo el marcador, usando la comunidad predominante y el total de coincidencias, si aplica
                                title = localStorage.getItem(Config.storage.format) == "degrees" ? gradeFormat : decimalFormat;
                                this.consultationList.push({point: {coordinates: [consultationPosition.longitude, 
                                    consultationPosition.latitude], degree: gradeFormat, 
                                    decimal: decimalFormat, marker: Config.markers.green, unique: unique}});
                                this.addMarker([
                                    consultationPosition.longitude,
                                    consultationPosition.latitude
                                ], title, Config.markers.green, unique, true);
                                this.recursiveListConsultationMarkers(list, areas, unique, consultationPosition);
                            });
                        });
                    });
                }else{
                    this.recursiveListConsultationMarkers(list, areas, unique, location);
                }
            });
        }else{
            if(!this.locationFound){
                Toast.makeText(this.makeTranslation("no_consultations")).show();
            }else{
                localStorage.setItem(Config.storage.map_consultations, JSON.stringify(this.consultationList));
                this.locationFound = false;
            }
            if(location){
                this.mapSetCenter(location.latitude, location.longitude);
            }
            this.hideLoadingIndicator();
        }
    }

    /**
        Método que verifica de forma recursiva si las comunidades se encuentran relativamente cerca
        @method recursiveCheckConsultationCloser
        @param {any} areas Lista de áreas
        @param {number} position Posición del listado a recorrer
        @param {any} list Lista de Consultas
        @param {any} consultationPosition Ubicación de la consulta
        @param {any} fn Función callback
    */
    public recursiveCheckConsultationCloser(areas, position, list, consultationPosition, matches, toDelete, communitiesCount, fn){
        if(position < list.length){
            this.couchbaseService.getDocumentDB(list[position]).then(nextConsultation => {
                let nextPosition = nextConsultation["data"]["loc_con"] ? nextConsultation["data"]["loc_con"] : null;
                if(nextPosition){
                    for(var item in nextPosition){
                        nextPosition[item] = Number(nextPosition[item]);
                    }
                    if(this.locationService.getDistance(consultationPosition, nextPosition) <= 50.0){
                        matches++;
                        toDelete.push(list[position]);
                        //si hay cercanía, debe actualizarse la lista de comunidades
                        let secondPatient = nextConsultation["data"]["pac_id"];
                        this.couchbaseService.getDocumentDB(secondPatient).then(secondCom => {
                            this.getCommunity(secondCom, areas, ["are_geo_pac", "sub_are_pac", "id_com"], nameCommunity => {
                                let communityName = nameCommunity["nom_com"];
                                let coincidence = false;
                                for(var j = 0; j < communitiesCount.length; j++){
                                    if(communitiesCount[j].community == communityName){
                                        communitiesCount[j].count++;
                                        coincidence = true;
                                        break;
                                    }
                                }
                                if(!coincidence){
                                    //si no hubo coincidencia, se agrega a la lista
                                    communitiesCount.push({
                                        community: communityName,
                                        count: 1
                                    });
                                }
                                this.recursiveCheckConsultationCloser(areas, position+1, list, consultationPosition, matches, toDelete, communitiesCount, fn);
                            })
                        });
                    }else{
                        this.recursiveCheckConsultationCloser(areas, position+1, list, consultationPosition, matches, toDelete, communitiesCount, fn);
                    }    
                }else{
                    this.recursiveCheckConsultationCloser(areas, position+1, list, consultationPosition, matches, toDelete, communitiesCount, fn);
                }
            });
        }else{
            fn(communitiesCount, matches);
        }
    }

    /**
        Método que obtiene la comunidad solicitada
        @method getcommunity
        @param {any} documentBase Documento a comparar
        @param {any} areas Lista de áreas
        @param {any} properties Lista de propiedades, en orden de área, subárea y comunidad
        @param {any} fn Función callback
    */
    public getCommunity(documentBase, areas, properties, fn){
        let patientListSubAreas;
        for(let i = 0; i < areas["data"].length; i++){
            //se obtiene el listado de subáreas
            if(areas["data"][i]["id_are_geo"] == documentBase["data"][properties[0]]){
                patientListSubAreas = areas["data"][i]["lis_sub_are"];
                break;
            }
        }
        this.couchbaseService.getDocumentDB(patientListSubAreas).then(subareas => {
            let patientListCommunities;
            for(let i = 0; i < subareas["data"].length; i++){
                //se obtiene el listado de subáreas
                if(subareas["data"][i]["id_sub_are"] == documentBase["data"][properties[1]]){
                    patientListCommunities = subareas["data"][i]["lis_com"];
                    break;
                }
            }
            this.couchbaseService.getDocumentDB(patientListCommunities).then(communities=>{
                let community;
                for(let i = 0; i < communities["data"].length; i++){
                    //se obtiene la comunidad
                    if(communities["data"][i]["id_com"] == documentBase["data"][properties[2]]){
                        community = communities["data"][i];
                        break;
                    }
                }
                fn(community);
            })
        });
    }

    /** 
        Método que agrega un marcador en el mapa
        @method addMarker
        @param {any} coordinates Coordenadas del punto
        @param {string} title Texto del popup
        @param {string} marker Nombre o dirección del marcador
        @param {boolean} unique Indicador de elemnto único
        @param {boolean} needsTap Indicador de necesidad de tap
    */
    addMarker(coordinates, title, icon, unique, needsTap){
        let fn = needsTap ? () => {this.lastPopup = title;} : () => {}
        this.mapbox.addMarkers([{
            lng: coordinates[0],
            lat: coordinates[1],
            title: title,
            icon: icon,
            selected: unique,
            onTap: fn
        }]);
    }

    /**
        Convierte un GeoJSON de tipo Polygon en un array de puntos (lat, lng).
        @method coordinatesToPointList
        @param {number} id Identificador de la geometría
        @param {string} title Nombre de la geometría a transformar
        @param {string} color Hexadecimal de color para geometría
        @param {any} coordinates JSON con las coordenadas a transformar
        @param {string} icon Dirección al ícono que se usará
    */
    coordinatesToPointList(id, title, color, coordinates, icon){
        let globalPoints = [];
        let half_lat = 0;
        let half_lng = 0;
        let counter = 0;
        coordinates.forEach( multiPolygon => {
            multiPolygon.forEach( polygon => {
                polygon.forEach(point => {
                    half_lat += point[1];
                    half_lng += point[0];
                    counter++;
                    globalPoints.push({
                        lng: point[0],
                        lat: point[1]
                    });                  
                });
                if(globalPoints.length){
                    this.doAddPolygon(id, globalPoints, color);
                    if(counter){
                        this.addMarker([half_lng/counter, half_lat/counter], title, icon, false, false);
                        this.lastPosition.lat = half_lat/counter;
                        this.lastPosition.lng = half_lng/counter;
                    }
                    globalPoints = [];
                }
            });
        });
    }

    /**
        Dibuja un poligono dados los puntos.
        @method doAddPolygon
        @param {number} id Identificador de la geometría
        @param {any} points Arreglo de JSON de puntos
        @param {string} color Hexadecimal de color para geometría
    */
    public doAddPolygon(id, points, color){
        this.mapbox.addPolygon({
            id: id,
            fillColor: new Color(color),
            fillOpacity: 0.4,
            points: points
        })
        .then(result => console.log("Mapbox addPolygon done"))
        .catch((error: string) => console.log("mapbox addPolygon error: " + error));
    }

    /**
        Muestra un dialogo para selección de las zonas.
        @method selectZonas
    */
    selectZonas(){
        let options = [
            this.makeTranslation("areas"), 
            this.makeTranslation("subareas"), 
            this.makeTranslation("communities"),
            this.makeTranslation("sum_consultations"),
            this.makeTranslation("visitObservation")
        ];
            
        dialogs.action({
            message: this.makeTranslation("areas"),
            cancelButtonText: this.makeTranslation("cancel"),
            actions: options
        }).then(result => {
            let previous = this.layerSelected;
            this.layerSelected =  options.indexOf(result);
            this.showLoadingIndicator(this.makeTranslation("loading"));
        setTimeout( () => {
                    switch (this.layerSelected) {
                        case 0:
                            setTimeout( () => {
                                this.iterateOverAreas(1);
                            }, 200);
                            break;
                        case 1:
                            setTimeout( () => {
                                this.iterateOverAreas(2);
                            }, 200);
                            break;
                        case 2:
                            setTimeout( () => {
                                this.iterateOverAreas(3);
                            }, 200);
                            break;
                        case 3:
                            setTimeout( () => {
                                this.iterateOverConsultations();
                            }, 200);
                            break;
                        case 4:
                            setTimeout( () => {
                                this.iterateOverAlerts();
                            }, 200);
                            break;
                        default:
                            this.layerSelected = previous;
                            this.hideLoadingIndicator();
                    }
                },300);

        });     
    }

    /** 
        Método que centra el mapa en la ubicación del usuario
        @method centerMap
    */
    public centerMap(){
        this.locationService.enableLocationRequest();
        this.getPermission(()=>{
            if(!this.permission){
                Toast.makeText(this.makeTranslation("no_location_granted")).show();
            }else{
                this.mapbox.getUserLocation().then(
                    (userLocation) => {
                        this.mapSetCenter(userLocation.location.lat, userLocation.location.lng);
                    }
                )
            }
        })
    }

    /** 
        Método que verifica y solicita permisos
        @method getPermission
        @property {any} fn Función callback
    */
    private getPermission(fn){
        this.start = false;
        this.mapbox.hasFineLocationPermission().then(
            (granted) => {
                console.log("Has Location Permission? " + granted);
                this.permission = granted;
                if(!granted){
                    this.mapbox.requestFineLocationPermission().then(
                        () => {
                            console.log("Location permission requested");
                            this.permission = true;
                            fn();
                        }, () => {
                            fn();
                        }
                    ).catch((err) =>{
                        console.log("No permission ", err);
                    });
                }else{
                    fn();
                }
            }
        );
    }

    /**
        Método que permite compartir una ubicación
        @method sharePopup
    */
    public sharePopup(){
        if(this.lastPopup){
            SocialShare.shareText(this.lastPopup);
        }else{
            Toast.makeText(this.makeTranslation("nothing_to_share")).show();
        }
    }

    /**
        Método que cambia el formato de los puntos
        @method toggleFormat toggle
    */
    public toggleFormat(){
        if(!this.isBusy){
            let format = localStorage.getItem(Config.storage.format);
            let opposite = format == "degrees" ? "decimals" : "degrees"
            let msg = "format_"+opposite;
            let options = {
                title: this.makeTranslation(msg),
                cancelButtonText: this.makeTranslation("cancel"),
                okButtonText: this.makeTranslation("accept"),
                cancelable: false,
                message: this.makeTranslation("sure_continue")
            };
            confirm(options).then((result: boolean) => {
                if(result){
                    localStorage.setItem(Config.storage.format, opposite);
                    if(this.layerSelected == 2){
                        this.showLoadingIndicator(this.makeTranslation("loading"));
                        setTimeout( () => {
                            this.iterateOverAreas(3);
                        }, 200);
                    }
                    if(this.layerSelected == 3){
                        this.showLoadingIndicator(this.makeTranslation("loading"));
                        setTimeout( () => {
                            this.iterateOverConsultations();
                        }, 200);
                    }
                    if(this.layerSelected == 4){
                        this.showLoadingIndicator(this.makeTranslation("loading"));
                        setTimeout( () => {
                            this.iterateOverAlerts();
                        }, 200);
                    }
                }
            });
        }
    }

    /** 
        Método que transforma una coordenada decimal en formato de grados, minutos y segundos, si aplica
        @method transformDecimal
        @param {string} coordinate Coordenada geográfica
        @param {boolean} isLat Indica si el elemento es latitud o longitud
        @param {number} selectedFormat Indica si desea un formato en particular
        @return {string} Coordenada en grados, minutos y segundos
    */
    public transformDecimal(coordinate, isLat, selectedFormat = 0) {
        if(localStorage.getItem(Config.storage.format) == "degrees" || selectedFormat == 0 || selectedFormat == 2){
            coordinate = "" + coordinate;
            let decimalSplit = coordinate.split(".");
            let integerPart = Number(decimalSplit[0]);
            let position = isLat ? (integerPart > 0 ? "N" : "S" ) : (integerPart > 0 ? "E" : "W" );
            let degrees = Math.abs(Number(decimalSplit[0]));
            let decimalPart = Number("0."+decimalSplit[1]);
            var minfloat = decimalPart*60;
            var minutes = Math.floor(minfloat);
            var seconds = (minfloat-minutes)*60;
            minutes = seconds >= 60.0 ? minutes + 1 : minutes;
            degrees = minutes >= 60 ? degrees + 1: degrees;
            minutes = minutes % 60;
            seconds = Math.round((seconds % 60) * 100)/100;
            coordinate = "" + degrees + " " + minutes + " " + seconds + position;
        }
        return coordinate;
    }

    /**
        Inicializa el mapa
        @method doShow
        @property {number} height Margen superior por altura del ActionBar
        @property {boolean} isNew Indicador de carga de mapa por navegación o resume
    */
    public doShow(height, isNew = false): void {
        this.mapbox.show({
            accessToken: Config.mapBoxTokenKey,
            style: MapStyle.SATELLITE_STREETS,
            center: {
                lat: this.pauseValues && this.pauseValues.lat ? this.pauseValues.lat : 10.4,
                lng: this.pauseValues && this.pauseValues.lng ? this.pauseValues.lng : -66.8
            },
            margins: {
                top: isAndroid ? height : height*1.45
            },
            zoomLevel: this.pauseValues && this.pauseValues.zoomLevel ? this.pauseValues.zoomLevel : 4,
            showUserLocation: this.permission,
            hideAttribution: true,
            hideLogo: true,
            hideCompass: false,
            disableRotation: false,
            disableScroll: false,
            disableZoom: false,
            disableTilt: false,
        }).then(
            showResult => {
                this.locationService.enableLocationRequest().then(()=>{
                    if(isAndroid && this.pauseValues){
                        setTimeout( () => {
                            this.mapbox.animateCamera({
                                target: {
                                    lat: this.pauseValues && this.pauseValues.lat ? this.pauseValues.lat : 10.4,
                                    lng: this.pauseValues && this.pauseValues.lng ? this.pauseValues.lng : -66.8
                                },
                                zoomLevel: this.pauseValues && this.pauseValues.zoomLevel ? this.pauseValues.zoomLevel : 4,
                                altitude: 2000, // iOS (meters from the ground)
                                bearing: this.pauseValues && this.pauseValues.bearing ? this.pauseValues.bearing : 0, // Where the camera is pointing, 0-360 (degrees)
                                tilt: this.pauseValues && this.pauseValues.tilt ? this.pauseValues.tilt : 0,
                                duration: 1000 // default 10000 (milliseconds)
                            })
                            this.mapStarted = true;
                        }, 200);
                    }else if(isNew){
                        setTimeout( () => {
                            this.centerMap();
                            this.mapStarted = true;
                        }, 200);
                    }
                    console.log(`Mapbox show done for ${showResult.ios ? "iOS" : "Android"}, native object received: ${showResult.ios ? showResult.ios : showResult.android}`);
                    this.mapbox.setOnMapClickListener(point => console.log(`>> Map clicked: ${JSON.stringify(point)}`));  
                    this.mapbox.setOnMapLongClickListener(point => console.log(`>> Map longpressed: ${JSON.stringify(point)}`));
                    this.mapbox.setOnFlingListener(() => {}).catch(err => console.log(err));
                })
            },
            (error: string) => console.log("mapbox show error: " + error)
        );
    }

    /**
        Método que forma parte del evento que se dispara al presionar el back físico de Android.
        Cancela el back físico y retorna
        @method backMapBefore
        @param {any} args Argumento del evento
    */
    public backMapBefore(args){
        args.cancel = true;
        this.backMap();
    }

    /**
        Método que ejecuta el back desde mapa
        @method backMap
    */
    public backMap(){
        this.getMapValues(() => {
            if(this.mapbox){
                this.mapbox.destroy();
            }
            this.back(this.isBusy);
        });
    }    

    /**
        Método que obtiene los valores del mapa
        @method getMapValues
        @param {any} fn Función callback
    */
    public getMapValues(fn){
        this.pauseValues = {};
        this.mapbox.getZoomLevel().then(
            (zoomLevel) => {
                this.pauseValues["zoomLevel"] = zoomLevel;
                this.mapbox.getCenter().then(
                    (result) => {
                        this.pauseValues["lat"] = result.lat;
                        this.pauseValues["lng"] = result.lng;
                        this.mapbox.getTilt().then((tilt => {
                            this.pauseValues["tilt"] = tilt;
                            localStorage.setItem(Config.storage.map_config, JSON.stringify(this.pauseValues));
                            fn();
                        }))
                    },
                    (error) => {
                        console.log("mapbox getCenter error: " + error);
                        fn();
                    }
                )
            }, (error) => {
                console.log("mapbox getZoomLevel error: " + error);
                fn();
            }
        )
    }
}
