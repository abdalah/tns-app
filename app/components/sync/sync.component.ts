import { TranslateService } from 'ng2-translate';
import * as Toast from 'nativescript-toast';
import { RouterExtensions } from 'nativescript-angular/router';
import { ListViewEventData, RadListView } from 'nativescript-ui-listview';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { CouchbaseService } from '~/services/couchbase.service';
import { Config } from '~/config/config';
import { BaseComponent } from "../base/base.component";
import * as localStorage from 'nativescript-localstorage';
import * as dialogs from "tns-core-modules/ui/dialogs";

@Component({
    selector: 'sync-view',
    moduleId: module.id,
    templateUrl: './sync.component.html',
    styleUrls: ['./sync.component.css']
})

/**
    Clase que posee las funciones asociadas a la vista de sincronización
    @class SyncComponent
    @author Abdalah Masrie <abdalah.masrie@sigis.com.ve>
    @copyright SIGIS Soluciones Integrales GIS, C.A
    created on 2018/05/30
*/
export class SyncComponent extends BaseComponent implements OnInit {
    /**
        Lista de elementos a sincronizar
        @property items
        @type {any}
    */
    private items: any = [];
    /**
        Cantidad de pacientes agregados localmente
        @property valuePatients
        @type {number}
    */
   private valuePatients: number = 0;
    /**
        Cantidad de consultas agregadas localmente
        @property valueConsultations
        @type {number}
    */
   private valueConsultations: number = 0;
    /**
        Cantidad de observaciones agregadas localmente
        @property valueNotes
        @type {number}
    */
   private valueNotes: number = 0;

    /**
        Método constructor
        @method constructor
    */
    constructor( 
        translate: TranslateService,
        router: Router,
        routerExtensions: RouterExtensions
    ) {
        super(routerExtensions, translate, router, null, null);
    }

    /**
        Método que realiza las inicializaciones pertinentes al entrar en el componente
        @method ngOnInit
    */
    ngOnInit(): void {
        this.showLoadingIndicator(this.makeTranslation("loading"));
        let couchbase = new CouchbaseService();
        couchbase.getDocumentDB(Config.id.local_patients).then(document => {
            this.items = document["data"];
            couchbase.getDocumentDB(Config.id.local_observations).then(notes => {
                this.valueNotes = notes["data"].length;
                this.valueConsultations = document["consultations"].length;
                this.valuePatients = document["patients"].length;
                this.hideLoadingIndicator();
            });
        });
    }

    /**
        Método que se invoca al seleccionar un elemento de la lista
        @method onSelection
        @param {ListViewEventData} args Instancia del objeto seleccionado
    */
    private onSelection(args: ListViewEventData){
        const listView = args.object as RadListView;
        const item = listView.getSelectedItems()[0];
        listView.deselectItemAt(args.index);
        this.navigate('/patientOptions/'+ item['_id']);
    }

    /**
        Método que permite subir los cambios al servidor de couchbase
        @method sync
    */
    private sync() {
        if(this.isBusy){
            return;
        }
        let couchbase = new CouchbaseService();
        this.showLoadingIndicator(this.makeTranslation("syncing"));
        this.syncPull(couchbase).then( _ => {
            const user = JSON.parse(localStorage.getItem(Config.storage.user));
            couchbase.getDocumentDB(user['_id']).then(userDocument=>{
                if (!userDocument["data"]['est_usu']){
                    Toast.makeText(this.makeTranslation("user_inactive")).show();
                    this.hideLoadingIndicator();
                    return;
                }
                couchbase.updateDocumentDB(Config.id.local_patients, {"data": [], "patients": [], "consultations": []}).then(()=>{
                    couchbase.updateDocumentDB(Config.id.local_observations, {"data": []}).then(()=>{
                        this.syncPush(couchbase).then( _ => {
                            this.valueConsultations = 0;
                            this.valueNotes = 0;
                            this.valuePatients = 0;
                            this.hideLoadingIndicator();
                            dialogs.alert({
                                title: this.makeTranslation("sync_title"),
                                message: this.makeTranslation("sync_success"),
                                okButtonText: this.makeTranslation("accept"),
                                cancelable: false
                            }).then(() => {
                                localStorage.removeItem(Config.storage.map_alerts);
                                localStorage.removeItem(Config.storage.map_consultations);
                                this.back(this.isBusy);
                            });
                        }, _ => {
                            this.hideLoadingIndicator();
                        });
                    });
                });
            });
        }, _ => {
            this.hideLoadingIndicator();
        });
    };
}
