import { Component, ViewChild, AfterViewInit, ChangeDetectorRef } from "@angular/core";
import { Page } from "ui/page";
import { RadSideDrawerComponent, SideDrawerType } from "nativescript-ui-sidedrawer/angular";
import { RadSideDrawer } from 'nativescript-ui-sidedrawer';
import { Router } from "@angular/router";
import * as localStorage from 'nativescript-localstorage';
import { CouchbaseService } from "../../services/couchbase.service";
import { BaseComponent } from "../base/base.component";
import { Config } from '~/config/config';
import { TranslateService } from 'ng2-translate';
import { prompt, PromptResult, inputType, PromptOptions } from "tns-core-modules/ui/dialogs";
import * as Toast from 'nativescript-toast';
let md5 = require("md5");

var application = require('application');

@Component({
    moduleId: module.id,
    selector: "drawer-component",
    templateUrl: 'drawer.component.html',
    styleUrls: ['drawer.component.css']
})

/**
    Clase que posee las funciones asociadas al menú lateral
    @class DrawerComponent
    @author Abdalah Masrie <abdalah.masrie@sigis.com.ve>
    @copyright SIGIS Soluciones Integrales GIS, C.A
    created on 2018/05/30
*/
export class DrawerComponent extends BaseComponent implements AfterViewInit {
    /**
        Nombre de usuario
        @property userName
        @type {string}
    */
    private userName: string;

    /**
        Email de usuario
        @property userEmail
        @type {string}
    */
    private userEmail: string;

    /**
        Referencia al componente visual de menú lateral
        @property drawerComponent
        @type {RadSideDrawerComponent}
    */
    @ViewChild(RadSideDrawerComponent) public drawerComponent: RadSideDrawerComponent;

    /**
        Referencia al menú lateral
        @property drawer
        @type {RadSideDrawer}
    */
    private drawer: RadSideDrawer;

    /**
        Método constructor
        @method constructor
        @param {Page} page Referencia a la página
        @param {Router} routes Objeto que permite navegar entre vistas
        @param {TranslateService} translate Objeto que permite traducir elementos
        @param {ChangeDetectorRef} changeDetector Objeto que verifica cambios en la vista del menú lateral
    */
    public constructor(
        private page: Page, 
        routes: Router,
        translate: TranslateService,
        changeDetector: ChangeDetectorRef
    ) {
        super(null, translate, routes, null, changeDetector);
        this.blockBack();

		//configuraciones previas al manejo del back button de android
		this.page.on(Page.loadedEvent, event => {
			if (application.android) {
				application.android.on(application.AndroidApplication.activityBackPressedEvent, this.backEvent);
			}
		})

		this.page.on(Page.unloadedEvent, event => {
			if (application.android) {
				application.android.off(application.AndroidApplication.activityBackPressedEvent, this.backEvent);
			}
		})

        this.page.on(Page.navigatingToEvent, (() => {
            this.initComponent();
        }));	
    }

    /**
        Método que se ejecuta al entrar al componente, independientemente de si es por primera vez o por efecto de back
        @method initComponent
    */
    public initComponent(){
        let json = JSON.parse(localStorage.getItem(Config.storage.user));
        this.userName = json.data.nom_usu + " " + json.data.ape_usu;
        this.userEmail = json.data.ema_usu;
        let couchbaseService = new CouchbaseService();
        couchbaseService.getDocumentDB(this.userEmail).then(user=>{
            let pictureId = user["data"]["img_usu"]; 
            this.loadPictureSrc(pictureId, true);
        });
    }
	
    /**
        Método que maneja el comportamiento del botón back. Al ser presionado, cierra la aplicación
        @method backEvent
        @param {any} args Argumentos del evento
    */
	private backEvent(args) {
        //Esta función maneja el back de android. args.cancel = true impide el uso en esta vista
        if (application.android) {
            application.android.foregroundActivity.finish();
        }
	}		

    /**
        Método que se ejecuta después de iniciar la vista
        @method ngAfterViewInit
    */
    public ngAfterViewInit() {
        this.drawer = this.drawerComponent.sideDrawer;
        this.changeDetector.detectChanges();
    }
	
    /**
        Método que se encarga de mostrar u ocultar el menú lateral
        @method toggleDrawer
    */
	public toggleDrawer(){
		this.drawer.toggleDrawerState();
	}

    /**
        Método que se encarga de mostrar el menú lateral
        @method openDrawer
    */
    public openDrawer() {
        this.drawer.showDrawer();
    }

    /**
        Método que se encarga de ocultar el menú lateral
        @method onCloseDrawerTap
    */
    public onCloseDrawerTap() {
       this.drawer.closeDrawer();
    }

    /**
        Método que maneja el enrutamiento al seleccionar una opción en el menú lateral
        @method onNavItemTap
        @param {string} navItemRoute Nombre de la ruta a ser redirigido
        @param {string} listRoute Nombre de la ruta a ser redirigido para lista
        @param {boolean} newDoc Indicador de necesidad de nuevo documento para el enlace
    */
    public onNavItemTap(navItemRoute: string, listRoute: string = null, newDoc: boolean = false): void {
        let str = navItemRoute;
        this.onCloseDrawerTap();
        //si tiene opción de lista
        if(listRoute || newDoc){
            let cb = new CouchbaseService();
            //se debe verificar si existe un elemento
            cb.getDocumentDB(Config.id.local_observations).then(value=>{
                let data = value["data"];
                if(data.length && !newDoc){
                    str = listRoute;
                    if(navItemRoute){
                        this.navigate(str);
                    }
                }else{
                    cb.createDocumentDB({}).then(id =>{
                        str += id;
                        if(navItemRoute){
                            this.navigate(str);
                        }
                    });
                }
            });
        }else if(navItemRoute){
            this.navigate(str);
        }
    }
	
    /**
        Método que borra el registro de sesión y redirige a la vista de inicio de sesión
        @method logout
    */
	public logout(){
        localStorage.removeItem(Config.storage.user);
		this.navigate("/login");
    }
    
    /**
        Método que cierra el drawer y llama a la función que verifica huellas
        @method closeAndCheck
    */
    public closeAndCheck(){
        this.toggleDrawer();
        this.checkPlugin();
    }

    /**
        Método que verifica que el usuario conozca su contraseña antes de redirigir
        @method verifyPassword
    */
    public verifyPassword(){
        let json = JSON.parse(localStorage.getItem(Config.storage.user));
        let options = {
            title: this.makeTranslation("validate_user"),
            okButtonText: this.makeTranslation("verify"),
            cancelButtonText: this.makeTranslation("cancel"),
            cancelable: false,
            inputType: inputType.password,
            message: this.makeTranslation("must_give_password")
        };
        prompt(options).then((result: PromptResult) => {
            if(result.result){
                if(md5(json.data.ema_usu+result.text) != json.data.pas_usu){
                    Toast.makeText(this.makeTranslation("wrong_password")).show();
                    this.verifyPassword();
                }else{
                    this.onNavItemTap('/profile/');
                }
            }
        });

    }
    
}