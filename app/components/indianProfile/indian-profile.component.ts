import { Component, OnInit, ViewChild, ChangeDetectorRef } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { Page } from "tns-core-modules/ui/page";
import { RouterExtensions } from "nativescript-angular/router";
import { Indian } from "../../models/indian.model"
import { RadDataFormComponent } from "nativescript-ui-dataform/angular";
import { TranslateService } from 'ng2-translate';
import { CouchbaseService } from "../../services/couchbase.service";
import { Config } from "../../config/config";
import { isAndroid, isIOS } from "platform";
import * as moment from 'moment';
import { BaseComponent } from "../base/base.component";
import * as Toast from 'nativescript-toast';
var application = require('application');

@Component({
    selector: "indian-profile",
    moduleId: module.id,
    styleUrls: ["indian-profile.component.css"],
    templateUrl: "./indian-profile.component.html"
})

/**
    Clase que posee las funciones asociadas al formulario de perfil de indígena
    @class IndianProfileComponent
    @author Abdalah Masrie <abdalah.masrie@sigis.com.ve>
    @copyright SIGIS Soluciones Integrales GIS, C.A
    created on 2018/07/03
*/
export class IndianProfileComponent extends BaseComponent implements OnInit{
    /**
        Instancia del servicio de couchbase
        @property couchbaseService
        @type {CouchbaseService}
    */
    private couchbaseService: CouchbaseService = new CouchbaseService();
    /**
        Representación del formulario
        @property profileForm
        @type {RadDataFormComponent}
    */
    @ViewChild('profileForm') profileForm: RadDataFormComponent;
    /**
        Listado de géneros
        @property genders
        @type {any}
    */
    private genders: any;
    /**
        JSON de elementos para picker de áreas
        @property procedence1
        @type {any}
    */
    private procedence1: any;
    /**
        JSON de elementos para picker de subáreas
        @property procedence2
        @type {any}
    */
    private procedence2: any;
    /**
        JSON de elementos para picker de comunidades
        @property procedence4
        @type {any}
    */
    private procedence4: any;
    /**
        Identificador de indígena
        @property _indian
        @type {Indian}
    */
    private _indian: Indian;
    /**
        Lista de áreas geográficas
        @property fstProcedence
        @type {any}
    */
    private fstProcedence: any;
    /**
        Lista de subáreas geográficas
        @property sndProcedence
        @type {any}
    */
    private sndProcedence: any;
    /**
        Lista de comunidades
        @property fthProcedence
        @type {any}
    */
    private fthProcedence: any;
    /**
        Género seleccionado
        @property selectedGender
        @type {string}
    */
    private selectedGender: string;
    /**
        Identificador de área seleccionada
        @property selectedArea
        @type {string}
    */
    private selectedArea: string;
    /**
        Identificador de subárea seleccionada
        @property selectedSubArea
        @type {string}
    */
    private selectedSubArea: string;
    /**
        Identificador de comunidad seleccionada
        @property selectedCommunity
        @type {string}
    */
    private selectedCommunity: string;
    /** 
        Identificadores de huellas encontradas en orden de pulgares a índices, de derecha a izquierda
        @property fingerFound
        @type {any}
    */
    private fingerFound: any = [];
    /** 
        Lista de todos los identificadores de huellas nuevas, sin orden ni tamaño específico
        @property fingerList
        @type {any}
    */
    private fingerList: any = [];

    /**
        Método constructor
        @method constructor
        @param {Router} router Objeto que permite navegar entre vistas
        @param {Page} page Referencia a la página
        @param {ActivatedRoute} activatedRoute Objeto que permite obtener los parámetros de query
        @param {RouterExtensions} routerExtensions Objeto que permite navegar por la pila de vistas
        @param {TranslateService} translate Objeto que permite traducir elementos
        @param {ChangeDetectorRef} changeDetector Objeto que verifica cambios en la vista 
    */
    public constructor(
        router: Router,
        private page: Page,
        activatedRoute: ActivatedRoute,
        routerExtensions: RouterExtensions,
        translate: TranslateService,
        changeDetector: ChangeDetectorRef
    ) {
        super(routerExtensions, translate, router, activatedRoute, changeDetector);
    }

    /**
        Función que carga la información previamente almacenada en base de datos sobre el indígena identificado
        @method ngOnInit
    */
    ngOnInit() {
        this.page.on(Page.loadedEvent, event => {
            if (application.android) {
                setTimeout(() => {
                    application.android.on(application.AndroidApplication.activityBackPressedEvent, (this.backPatientBefore).bind(this));
                },500)
            }
        });

        this.page.on(Page.unloadedEvent, event => {
            if (application.android) {
                application.android.removeEventListener(application.AndroidApplication.activityBackPressedEvent);
            }
        });

        this.showLoadingIndicator(this.makeTranslation("loading"));
        //género
        let male = this.makeTranslation("male");
        let female = this.makeTranslation("female");
        this.genders = [
            {key: "0", label: this.makeTranslation("select")},
            {key: "M", label: male},
            {key: "F", label: female}
        ];
        this.selectedGender = "0";
        //áreas geográficas
        this.couchbaseService.getDocumentDB(Config.id.areas).then(fstProcedence => {
            this.fstProcedence = fstProcedence;
            this.procedence1 = this.setListPicker(this.fstProcedence.data, "id_are_geo", "nom_are_geo", "ord_geo");
            const id = this.activatedRoute.snapshot.params["id"];
            //cargar los valores previamente almacenados, si existen
            this.couchbaseService.getDocumentDB(id).then(obj => {
                let fingers = obj["data"] && obj["data"]["pac_hue"] ? obj["data"]["pac_hue"] : [null, null, null, null];
                for(var i = 0; i < fingers.length; i++){
                    let nameFinger = this.getFingerName(i);
                    let json = {
                        "finger": fingers[i],
                        "hasFinger": fingers[i] ? true : false,
                        "nameFinger": nameFinger,
                        "position": i
                    };
                    this.fingerFound.push(json);
                }
                this.changeDetector.detectChanges();
                //transición: si no tiene fecha, crea como nuevo, de lo contrario usa la fecha registrada
                if(obj["data"] && obj["data"]["fec_mod"]){
                    this._indian = new Indian(id, obj["data"]["fec_cre"]);
                }else{
                    this._indian = new Indian(id);
                }
                if(obj["data"] && obj["data"]["nom_pac"]){
                    //crear un modelo con el id y la fecha
                    this.getIndian().setName(obj["data"]["nom_pac"]);
                    this.selectedGender = obj["data"]["gen_pac"];
                    this.getIndian().setGender(this.selectedGender);
                    this.getIndian().setHeight(obj["data"]["est_pac"]);
                    this.getIndian().setYear((new Date()).getFullYear() - obj["data"]["fec_nac"]);
                    this.getIndian().setIsBrazilian(obj["data"]["pai_ori"] == "br" ? true : false);
                    this.getIndian().setIsVenezuelan(obj["data"]["pai_ori"] == "ve" ? true : false);
                    this.getIndian().setConsultations(obj["data"]["con_pac"]);
                    this.selectedArea = obj["data"]["are_geo_pac"];
                    this.getIndian().setFirstProcedence(this.selectedArea);
                    this.getChild(this.fstProcedence, this.selectedArea, "id_are_geo", "lis_sub_are", (sndProcedence)=>{
                        this.sndProcedence = sndProcedence;
                        this.procedence2 = this.setListPicker(this.sndProcedence.data, "id_sub_are", "nom_sub_are", "ord_sub_are");
                        this.selectedSubArea = obj["data"]["sub_are_pac"];
                        this.getIndian().setSecondProcedence(this.selectedSubArea);
                        this.getChild(this.sndProcedence, this.selectedSubArea, "id_sub_are", "lis_com", (fthProcedence)=>{
                            this.fthProcedence = fthProcedence;
                            this.procedence4 = this.setListPicker(this.fthProcedence.data, "id_com", "nom_com", "ord_com");
                            this.selectedCommunity = obj["data"]["id_com"];
                            this.getIndian().setCommunityList(this.selectedCommunity);
                            let pictureId = obj["data"] && obj["data"]["img_pac"] ? obj["data"]["img_pac"] : null;
                            this.loadPictureSrc(pictureId);
                            this.updateFormDocument();
                            this.changeDetector.detectChanges();
                            this.hideLoadingIndicator();
                        });
                    });
                }else{
                    //si el usuario es nuevo, se carga el primer área de la visita en curso
                    let found = false;
                    this.couchbaseService.getDocumentDB(Config.id.visits).then(visits => {
                        for(var i = 0; i < visits["data"].length; i++){
                            let startDate = moment(visits["data"][i].fec_ini_vis).utcOffset(0,true).local();
                            let endDate = moment(visits["data"][i].fec_fin_vis).utcOffset(0,true).local();
                            let close = visits["data"][i].vis_act;
                            let today = moment();
                            if(today.isAfter(startDate) && today.isBefore(endDate) && !close){
                                this.selectedArea = visits["data"][i].are_geo_vis[0];
                                this.getIndian().setFirstProcedence(this.selectedArea);
                                found = true;
                                break;
                            }
                        }
                        if(found){
                            this.getChild(this.fstProcedence, this.selectedArea, "id_are_geo", "lis_sub_are", (sndProcedence)=>{
                                this.sndProcedence = sndProcedence;
                                this.procedence2 = this.setListPicker(this.sndProcedence.data, "id_sub_are", "nom_sub_are", "ord_sub_are");
                            });
                        }
                        let pictureId = obj["data"] && obj["data"]["img_pac"] ? obj["data"]["img_pac"] : null;
                        this.loadPictureSrc(pictureId);
                        this.hideLoadingIndicator();
                    });
                }
            });
        });
    }

    /**
        Función que retorna los datos obtenidos del indígena
        @method getIndian
        @return {Indian}
    */
    private getIndian(): Indian {
        return this._indian;
    }

    /**
        Método que permite actualizar de formulario
        @method updateDocument
    */
    public updateFormDocument(){
        let newElem = this.getIndian().clone();
        this._indian = newElem;
    }
    /**
        Método disparado una vez se haya validado un campo en el formulario
        @method onPropertyValidated
        @param {any} args Argumentos de validación
    */
    private onPropertyValidated(args){
        let property = args.propertyName;
        let valueCandidate = args.entityProperty.valueCandidate;
        let isValid = args.entityProperty.isValid;
        console.log("propiedad afectada ", property);
        if(isValid){
            if(property == "name"){
                this.getIndian().setName(valueCandidate);
            }else if(property == "gender"){
                this.selectedGender = valueCandidate;
                if(isAndroid){
                    this.selectedGender = this.getListId(this.genders, valueCandidate, "label", "key");
                }
            }else if(property == "isVenezuelan"){
                this.getIndian().setIsVenezuelan(valueCandidate);
                this.profileForm.dataForm.notifyValidated("isBrazilian", true)
            }else if(property == "isBrazilian"){
                this.getIndian().setIsBrazilian(valueCandidate);
            }else if(property == "yearBorn"){
                this.getIndian().setYear(+valueCandidate);
            }else if(property == "gender"){
                this.getIndian().setGender(valueCandidate);
            }else if(property == "height"){
                this.getIndian().setHeight(+valueCandidate);
            }else if(property == "firstProcedence" && this.selectedArea != valueCandidate && this.fstProcedence){
                this.showLoadingIndicator(this.makeTranslation("loading"));
                this.selectedArea = valueCandidate;
                if(isAndroid){
                  this.selectedArea = this.getListId(this.procedence1.items, valueCandidate, "nom_are_geo", "id_are_geo");
                }
                this.selectedSubArea = "0";
                this.selectedCommunity = "0";
                this.getIndian().setCommunityList(this.selectedCommunity);
                this.getIndian().setSecondProcedence(this.selectedSubArea);
                this.getChild(this.fstProcedence, this.selectedArea, "id_are_geo", "lis_sub_are", (sndProcedence)=>{
                    this.sndProcedence = sndProcedence;
                    this.procedence2 = [];
                    if(this.sndProcedence && this.sndProcedence.data){
                        this.procedence2 = this.setListPicker(this.sndProcedence.data, "id_sub_are", "nom_sub_are", "ord_sub_are");
                    }
                    this.getIndian().setFirstProcedence(this.selectedArea);
                    this.hideLoadingIndicator();
                });
            }else if(property == "secondProcedence" && this.selectedSubArea != valueCandidate && this.sndProcedence){
                this.showLoadingIndicator(this.makeTranslation("loading"));
                this.selectedSubArea = valueCandidate;
                if(isAndroid){
                    this.selectedSubArea = this.getListId(this.procedence2.items, valueCandidate, "nom_sub_are", "id_sub_are");
                }
                this.selectedCommunity = this.selectedSubArea ? "0" : null;
                this.getIndian().setCommunityList(this.selectedCommunity);
                this.getChild(this.sndProcedence, this.selectedSubArea, "id_sub_are", "lis_com", (fthProcedence)=>{
                    this.fthProcedence = fthProcedence;
                    this.procedence4 = [];
                    if(this.fthProcedence && this.fthProcedence.data){
                        this.procedence4 = this.setListPicker(this.fthProcedence.data, "id_com", "nom_com", "ord_com");
                    }
                    this.getIndian().setSecondProcedence(this.selectedSubArea);
                    this.hideLoadingIndicator();
                });
            }else if(property == "communityList" && this.selectedCommunity && this.selectedCommunity != valueCandidate && this.fthProcedence){
                this.selectedCommunity = valueCandidate;
                if(isAndroid){
                  this.selectedCommunity = this.getListId(this.procedence4.items, valueCandidate, "nom_com", "id_com");
                }
                this.getIndian().setCommunityList(this.selectedCommunity);
            }
        }
        //console.log("indian value ", this.getIndian());
    }

    /**
        Método que almacena el modelo en base de datos y regresa a la ventana anterior
        @method saveModel
    */
    private saveModel(args){
        if(!this.isBusy){
            this.getLastFingers((lastFingers)=>{
                let gotFingers = false;
                for(let i = 0; i < lastFingers.length; i++){
                    if(lastFingers[i]){
                        gotFingers = true;
                        break;
                    }
                }
                if(!gotFingers){
                    Toast.makeText(this.makeTranslation("must_add_fingerprint")).show();
                }else{
                    this.showLoadingIndicator(this.makeTranslation("saving"));
                    //realizar validaciones
                    if(this.validateForm()){
                        let patient = this.activatedRoute.snapshot.params["id"];
                        this.couchbaseService.getDocumentDB(patient).then(patientDoc => {
                            let oldFingers = patientDoc && patientDoc["data"] && patientDoc["data"]["pac_hue"] ? patientDoc["data"]["pac_hue"] : [null, null, null, null];
                            this.alterPictureDocument(patientDoc, "img_pac", "pacientes", (imgId) => {
                                let json = {
                                    "data": {
                                        "nom_pac": this.getIndian().getName(),
                                        "id_com": this.getIndian().getCommunityList(),
                                        "gen_pac": this.getIndian().getGender()[0],
                                        "est_pac": this.getIndian().getHeight(),
                                        "fec_nac": (new Date()).getFullYear() - this.getIndian().getYear(),
                                        "pac_hue": lastFingers,
                                        "are_geo_pac": this.getIndian().getFirstProcedence(),
                                        "sub_are_pac": this.getIndian().getSecondProcedence(),
                                        "pai_ori": (this.getIndian().getIsBrazilian().toString() == "true" ? "br" : (this.getIndian().getIsVenezuelan().toString() == "true" ? "ve" : null)),
                                        "vac_pac": [],
                                        "con_pac": this.getIndian().getConsultations(),
                                        "img_pac": imgId,
                                        "fec_cre": this.getIndian().getDateCreated(),
                                        "fec_mod": this.getIndian().getDateModified()
                                    },
                                    "type": "pacientes"
                                };
                                //modificación de datos de paciente
                                this.couchbaseService.updateDocumentDB(patient, json).then(()=>{
                                    this.updateFingerDocuments(oldFingers, lastFingers, ()=>{
                                        //console.log("couch ", this.couchbaseService.getDocument(patient));
                    
                                        //se actualiza la lista temporal
                                        this.couchbaseService.getDocumentDB(Config.id.local_patients).then(listPatientData => {
                                            let found = false;
                                            for(var i = 0; i < listPatientData["patients"].length; i++){
                                                if(listPatientData["patients"][i] == patient){
                                                    found = true;
                                                    break;
                                                }
                                            }
                                            if(!found){
                                                listPatientData["patients"].push(patient);
                                            }
                                            this.couchbaseService.updateDocumentDB(Config.id.local_patients, listPatientData).then(()=>{
                                                this.hideLoadingIndicator();
                                                //regresar a la vista anterior
                                                this.back(this.isBusy);
                                            });
                                        });
                                    });
                                });
                            })                            
                        });
                    }else{
                        this.hideLoadingIndicator();
                    }
                }
            });
        }
    }

    /**
        Función que se encarga de realizar las validaciones básicas sobre el formulario
        @method validateForm
        @return {boolean} Verdadero si el formulario es válido, falso en caso de error
    */
    private validateForm(){
        let validated = true;
        let select = this.makeTranslation("select");
        const name = this.profileForm.dataForm.getPropertyByName("name");
        if(!name.valueCandidate || name.valueCandidate.trim().length == 0){
            validated = this.setError("empty_field", "name", name, this.profileForm);
        }else if(name.valueCandidate.length > 50){
            validated = this.setError("length_exceeded", "name", name, this.profileForm);
        }
        const isBr = this.profileForm.dataForm.getPropertyByName("isBrazilian");
        const isVe = this.profileForm.dataForm.getPropertyByName("isVenezuelan");
        if(isBr.valueCandidate == isVe.valueCandidate){
            validated = this.setError("multiple_nationalities", "isBrazilian", isBr, this.profileForm);
        }
        const year = this.profileForm.dataForm.getPropertyByName("yearBorn");
        if(!year.valueCandidate || isNaN(year.valueCandidate) || year.valueCandidate < 0 ||
            year.valueCandidate > 120){
            validated = this.setError("field_out_range", "yearBorn", year, this.profileForm);
        }
        const height = this.profileForm.dataForm.getPropertyByName("height");
        if(!height.valueCandidate || isNaN(height.valueCandidate) || height.valueCandidate < 1 || height.valueCandidate > 300){
            validated = this.setError("field_out_range", "height", height, this.profileForm);
        }
        const communityList = this.profileForm.dataForm.getPropertyByName("communityList");
        if((communityList.valueCandidate == select || this.selectedCommunity == "0")){
            validated = this.setError("must_select_option", "communityList", communityList, this.profileForm);
        }
        const fstProcedenceList = this.profileForm.dataForm.getPropertyByName("firstProcedence");
        if(fstProcedenceList.valueCandidate == select || !this.selectedArea || this.selectedArea == "0"){
            validated = this.setError("must_select_option", "firstProcedence", fstProcedenceList, this.profileForm);
        }
        const sndProcedenceList = this.profileForm.dataForm.getPropertyByName("secondProcedence");
        if(sndProcedenceList.valueCandidate == select || !this.selectedSubArea || this.selectedSubArea == "0"){
            validated = this.setError("must_select_option", "secondProcedence", sndProcedenceList, this.profileForm);
        }
        const genderList = this.profileForm.dataForm.getPropertyByName("gender");
        if(genderList.valueCandidate == select || !this.selectedGender || this.selectedGender == "0"){
            validated = this.setError("must_select_option", "gender", genderList, this.profileForm);
        }
        return validated;
    }

    /**
        Función que obtiene el id correspondiente a un nombre de una lista
        @method getListId
        @param {any} list Lista de elementos a recorrer
        @param {string} value Valor a comparar
        @param {string} label Propiedad a buscar
        @param {string} label Propiedad de identificador
    */
    private getListId(list, value, label, key){
        let keyId = -1;
        if(isNaN(value)){
            for(var i = 0; i < list.length; i++){
                if(list[i][label] == value){
                    keyId = list[i][key];
                    break;
                }
            }
        }else{
            return value;
        }
        return keyId;
    }

    /** 
        Método que almacena la nueva huella para un dedo establecido
        @method savePatientFinger
        @param {number} position Indicador del dedo que está almacenando
    */
    public savePatientFinger(position: number){
        const patientId = this.activatedRoute.snapshot.params["id"];
        //solicita la nueva huella
        this.checkPlugin(false, (this.fingerFound[position] || "1"), (fingerNumber, fingerImage, fingerPlugin) => {
            //crea el documento correspondiente en couchbase
            this.couchbaseService.createDocumentDB({
                "data":{
                    "huella": fingerImage,
                    "mano": fingerNumber,
                    "pac_id": patientId
                },
                "type": "huellas"
            }).then(lastFinger => {
                //refresca la vista
                let json = {
                    "finger": lastFinger,
                    "hasFinger": true,
                    "nameFinger": this.getFingerName(fingerNumber),
                    "position": fingerNumber
                };
                this.fingerFound[fingerNumber] = json;
                this.fingerList.push({id: lastFinger, fingerNumber: fingerNumber});
                this.hideLoadingIndicator();
                this.changeDetector.detectChanges();
            });
        }, position);
    }

    /**
        Método que forma parte del evento que se dispara al presionar el back físico de Android.
        Cancela el back físico y valida envía la alerta de eliminación en caso de paciente sin huellas
        antes de continuar.
        @method backPatientBefore
        @param {any} args Argumento del evento
    */
    public backPatientBefore(args){
        args.cancel = true;
        this.patientBack();
    }

    /**
        Método que elimina cualquier huella creada que no se decidió guardar antes de regresar
        @method patientBack
    */
    public patientBack(){
        this.recursiveDelete(this.fingerList.map((elem)=>{
            return elem.id;
        }), ()=>{
            this.back();
        });
    }

    /**
        Función que obtiene las últimas huellas registradas
        @method getLastFingers
        @param {any} fn Callback con el listado de identificadores de huellas
    */
    public getLastFingers(fn){
        let patient = this.activatedRoute.snapshot.params["id"];
        this.couchbaseService.getDocumentDB(patient).then(patientDoc => {
            //se obtiene el listado de huellas almacenadas hasta el momento
            let fingers = patientDoc && patientDoc["data"] && patientDoc["data"]["pac_hue"] ? patientDoc["data"]["pac_hue"] : [null, null, null, null];
            let changes = [null, null, null, null];
            let tempFingerList = this.fingerList.slice(0);
            while(tempFingerList.length > 0){
                let fingerData = tempFingerList.pop();
                if(!changes[fingerData.fingerNumber]){
                    //si changes está vacío, significa que el elemento es nuevo y se guardará
                    changes[fingerData.fingerNumber] = true;
                    fingers[fingerData.fingerNumber] = fingerData.id;
                }
            }
            fn(fingers);
            
        });
    }

    /**
        Método que actualiza los documentos de huellas, eliminando los innecesarios
        @method updateFingerDocuments
        @param {any} oldFingers Listado de huellas anteriores a la actualización
        @param {any} fingers Listado de huellas actualizadas
        @param {any} fn Función callback
    */
    public updateFingerDocuments(oldFingers, fingers, fn){
        let listFingersDeleting = [];
        //se obtiene el listado de id por huella
        this.recursiveGetPatientFingers(0, [], (listIdFingers) => {
            while(this.fingerList.length > 0){
                let fingerData = this.fingerList.pop();
                if(fingers[fingerData.fingerNumber] == fingerData.id){
                    //se debe agregar también al listado de huellas
                    listIdFingers[fingerData.fingerNumber].push(fingerData.id);
                }else{
                    //se eliminará
                    listFingersDeleting.push(fingerData.id);
                }
            }
            this.recursiveUpdateFingers(0, oldFingers, fingers, listIdFingers, ()=>{
                this.recursiveDelete(listFingersDeleting, fn);
            })
        })
    }

    /**
        Método que obtiene cada una de las huellas del paciente
        @method recursiveGetPatientFingers
        @param {number} position Posiicón del arreglo que recorre
        @param {any} listIdFingers Lista de Huellas 
        @param {any} fn Función callback
    */
    public recursiveGetPatientFingers(position, listIdFingers, fn){
        if(position < Config.id.fingerprints.length){
            this.couchbaseService.getDocumentDB(Config.id.fingerprints[position]).then(elem => {
                listIdFingers.push(elem["data"]);
                this.recursiveGetPatientFingers(position + 1, listIdFingers, fn);
            })
        }else{
            fn(listIdFingers);
        }
    }

    /**
        Método que actualiza recursivamente la lsita de huellas a eliminar
        @method recursiveUpdateFingers
        @param {number} position Posiicón del arreglo que recorre
        @param {any} oldfingers Huellas anteriores
        @param {any} fingers Listado de huellas
        @param {any} listIdFingers Lista de Id de huellas vigentes
        @param {any} fn Función callback
    */
    public recursiveUpdateFingers(position, oldFingers, fingers, listIdFingers, fn){
        if(position < oldFingers.length){
            if(oldFingers[position] && oldFingers[position] != fingers[position]){
                //se elimina la huella antigua que haya sido modificada
                listIdFingers.push(oldFingers[position]);
                //se debe borrar también de su posición del listado de huellas
                listIdFingers[position].splice(listIdFingers[position].indexOf(oldFingers[position]), 1);
            }
            //actualiza los listados
            this.couchbaseService.updateDocumentDB(Config.id.fingerprints[position], {"data": listIdFingers[position]}).then(()=>{
                this.recursiveUpdateFingers(position+1, oldFingers, fingers, listIdFingers, fn);
            });
        }else{
            fn();
        }
    }

}
