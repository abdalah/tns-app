import { Component } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { TranslateService } from 'ng2-translate';
import { BaseComponent } from "../base/base.component";
const json = require('../../i18n/en.json');

@Component({
    selector: "faq",
    moduleId: module.id,
    templateUrl: "./faq.component.html",
    styleUrls: ['faq.component.css']
})

/**
    Clase que posee las funciones asociadas al componente de ayuda
    @class FAQComponent
    @author Abdalah Masrie <abdalah.masrie@sigis.com.ve>
    @copyright SIGIS Soluciones Integrales GIS, C.A
    created on 2018/07/30
*/
export class FAQComponent extends BaseComponent{
    /**
        Lista de preguntas y respuestas
        @property faq
        @type {any}
    */
    private faqs: any = [];

    /**
        Método constructor
        @method constructor
        @param {RouterExtensions} routerExtensions Objeto que permite navegar por la pila de vistas
        @param {TranslateService} translate Objeto que permite traducir elementos
    */
    public constructor(routerExtensions: RouterExtensions,translate: TranslateService) {
        super(routerExtensions, translate, null, null, null);
        let check = true;
        let counter = 0;
        let questionPreffix = "question";
        let answerPreffix = "answer";
        while(check){
            let question = questionPreffix+counter;
            let answer = answerPreffix+counter;
            counter++;
            if(json[question]){
                let list = {
                    "question": this.makeTranslation(question),
                    "answer": this.makeTranslation(answer)    
                };
                this.faqs.push(list);
            }
            check = json[question] ? true : false;
        }
    }

}
