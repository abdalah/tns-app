import { Component, OnInit, ViewChild, ChangeDetectorRef } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { ActivatedRoute } from "@angular/router";
import { TranslateService } from 'ng2-translate';
import { RadDataFormComponent } from "nativescript-ui-dataform/angular";
import { BaseComponent } from "../base/base.component";
import { CouchbaseService } from "../../services/couchbase.service";
import { Config } from "../../config/config";
import { Observation } from "../../models/observation.model"
import * as localStorage from 'nativescript-localstorage';
import { isAndroid } from "platform";

@Component({
    selector: "visit-observation",
    moduleId: module.id,
    templateUrl: "./visitObservation.component.html"
})

/**
    Clase que posee las funciones asociadas al componente de observación de visita
    @class VisitObservationComponent
    @author Abdalah Masrie <abdalah.masrie@sigis.com.ve>
    @copyright SIGIS Soluciones Integrales GIS, C.A
    created on 2018/09/17
*/
export class VisitObservationComponent extends BaseComponent implements OnInit{
    /**
        Representación del formulario
        @property observationForm
        @type {RadDataFormComponent}
    */
    @ViewChild('observationForm') observationForm: RadDataFormComponent;
    /**
        Identificador de observación
        @property _observation
        @type {Observation}
    */
    private _observation: Observation;
    /**
        Indicador de nueva observación
        @property isNew
        @type {boolean}
    */
    private isNew: boolean;
    /**
        Id de observación
        @property idObservation
        @type {string}
    */
    private idObservation: string;
    /**
        Id del usuario
        @property userEmail
        @type {string}
    */
    private userEmail: string;
    /**
        Tipos de alertas disponibles
        @property typeAlerts
        @type {any}
    */
    private typeAlerts: any;
    /**
        Tipos de alertas seleccionado
        @property selectedAlert
        @type {number}
    */
    private selectedAlert: number;

    /**
        Método constructor
        @method constructor
        @param {RouterExtensions} routerExtensions Objeto que permite navegar por la pila de vistas
        @param {ActivatedRoute} activatedRoute Objeto que permite obtener los parámetros de query
        @param {TranslateService} translate Objeto que permite traducir elementos
        @param {ChangeDetectorRef} changeDetector Objeto que verifica cambios en la vista
    */
    public constructor(
        routerExtensions: RouterExtensions,
        activatedRoute: ActivatedRoute,
        translate: TranslateService,
        changeDetector: ChangeDetectorRef
    ) {
        super(routerExtensions, translate, null, activatedRoute, changeDetector);
    }

    /**
        Función que carga la información previamente almacenada en base de datos sobre la observación seleccionada, si existe
        @method ngOnInit
    */
    ngOnInit(){
        this.showLoadingIndicator(this.makeTranslation("loading"));
        this.typeAlerts = [
            { key: 0, label: this.makeTranslation("select") },
            { key: 1, label: this.makeTranslation("obs_log") },
            { key: 2, label: this.makeTranslation("obs_epi") },
            { key: 3, label: this.makeTranslation("obs_pob") }
        ];
        let cb = new CouchbaseService();
        this.idObservation = this.activatedRoute.snapshot.params["id"];
        cb.getDocumentDB(this.idObservation).then(observation=>{
            this.isNew = observation["data"] && observation["data"]["fec_cre"] ? false : true;
            let json = JSON.parse(localStorage.getItem(Config.storage.user));
            this.userEmail = json.data.ema_usu;
            if(observation["type"] == "observaciones"){
                this._observation = new Observation(this.userEmail, observation["data"]["fec_cre"]);
                this.getObservation().setLocation(observation["data"]["obs_loc"]);
                this.getObservation().setObservations(observation["data"]["obs_msg"]);
                this.selectedAlert = observation["data"]["obs_typ"];
                this.getObservation().setType(this.selectedAlert);
            }else{
                this._observation = new Observation(this.userEmail);
            }
            let pictureId = observation["data"] && observation["data"]["img_obs"] ? observation["data"]["img_obs"] : null;
            this.loadPictureSrc(pictureId);
            if(!this.getObservation().getLocation()){
                this.getLocation( location => {
                    this.getObservation().setLocation(location);
                });
            }else{
                this.hideLoadingIndicator();
            }
        });
    }

    /**
        Función que retorna los datos obtenidos del modelo de observación
        @method getObservation
        @return {Observation}
    */
    private getObservation(): Observation{
        return this._observation;
    }

    /**
        Método que almacena el modelo en base de datos y regresa a la ventana anterior
        @method saveModel
    */
    private saveModel(args){
        //validar el formulario
        if(!this.isBusy && this.validateForm()){
            this.showLoadingIndicator(this.makeTranslation("saving"));
            if(!this.getObservation().getLocation()){
                this.checkLocation(json => {
                    if(json.res){
                        this.getObservation().setLocation(json.val);
                    }
                    this.addValues(args);
                });
            }else{
                this.addValues(args);
            }            
        }
    }   

    /**
        Método disparado una vez se haya validado un campo en el formulario
        @method onPropertyValidated
        @param {any} args Argumentos de validación
    */
    private onPropertyValidated(args){
        let property = args.propertyName;
        let valueCandidate = args.entityProperty.valueCandidate;
        if(property == "type"){
            this.selectedAlert = valueCandidate;
            if(isAndroid){
                for(var i = 0; i < this.typeAlerts.length; i++){
                    if(this.typeAlerts[i].label == valueCandidate){
                        this.selectedAlert = this.typeAlerts[i].key;
                    }
                }
            }
            if(isNaN(this.selectedAlert)){
                this.selectedAlert = -1;
            }
            this.getObservation().setType(this.selectedAlert);
        }

    }

    /**
        Función que valida los campos en el formulario
        @method validateForm
        @return {boolean}
    */
    private validateForm(){
        let validated = true;
        const observations = this.observationForm.dataForm.getPropertyByName("observations");
        if((!observations.valueCandidate || observations.valueCandidate.trim().length == 0) && !this.getObservation().getObservations()){
            validated = this.setError("empty_field", "observations", observations, this.observationForm);
        }else if(observations.valueCandidate.length > 250){
            validated = this.setError("length_exceeded", "observations", observations, this.observationForm);
        }
        const type = this.observationForm.dataForm.getPropertyByName("type");
        if(!this.selectedAlert){
            validated = this.setError("must_select_option", "type", type, this.observationForm);
        }
        return validated;
    }    

    /**
        Método que agrega los valores restantes al modelo, lo guarda y regresa a la vista anterior
        @method addValues
        @param {any} args Argumentos de evento
    */
    private addValues(args){
        let cb = new CouchbaseService();
        let observations = this.observationForm.dataForm.getPropertyByName("observations");
        if(observations.valueCandidate && observations.valueCandidate.trim().length > 0){
            this.getObservation().setObservations(observations.valueCandidate);
        }
        cb.getDocumentDB(this.idObservation).then(observationData => {
            this.alterPictureDocument(observationData, "img_obs", "observaciones", (imgId) =>{
                let json = {
                    "data": {
                        "fec_cre": this.getObservation().getDateCreated(),
                        "fec_mod": this.getObservation().getDateModified(),
                        "obs_loc": this.getObservation().getLocation(),
                        "obs_msg": this.getObservation().getObservations(),
                        "obs_typ": this.getObservation().getType(),
                        "doc_id": this.userEmail,
                        "img_obs": imgId
                    },
                    "type": "observaciones"
                };
                cb.updateDocumentDB(this.idObservation, json).then(()=>{
                    //se actualiza la lista temporal
                    cb.getDocumentDB(Config.id.local_observations).then(listObservations =>{
                        let found = false;
                        let tempObservation = json.data;
                        tempObservation["_id"] = this.idObservation;
                        for(var i = 0; i < listObservations["data"].length; i++){
                            if(listObservations["data"][i]["_id"] == this.idObservation){
                                found = true;
                                listObservations["data"][i] = tempObservation;
                                break;
                            }
                        }
                        if(!found){
                            listObservations["data"].push(tempObservation);
                        }
                        cb.updateDocumentDB(Config.id.local_observations, listObservations).then(()=>{
                            localStorage.removeItem(Config.storage.map_alerts);
                            this.hideLoadingIndicator();
                            this.backEvent(args);
                        });
                    });
                });
            })
        });
    }

    /**
        Método que maneja el comportamiento del botón back
        @method backEvent
        @param {any} args Argumentos del evento
    */
    private backEvent(args) {
        if(!this.isBusy){
            let cb = new CouchbaseService();
            cb.getDocumentDB(this.idObservation).then(observation=>{
                if(!observation["type"]){
                    cb.deleteDocumentDB(this.idObservation).then(()=>{
                        this.back();
                    }).catch(err => {
                        console.log(err);
                        this.back();
                    });
                }else{
                    this.back();
                }
            });
        }
    }
}