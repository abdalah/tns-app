import { Component } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { Page } from "tns-core-modules/ui/page";
import { RouterExtensions } from "nativescript-angular/router";
import { TranslateService } from 'ng2-translate';
import { CouchbaseService } from "../../services/couchbase.service";
import { ListViewEventData, RadListView } from "nativescript-ui-listview";
import { BaseComponent } from "../base/base.component";
import { Config } from "../../config/config";
import * as moment from 'moment';

@Component({
    selector: "visitObservationList",
    moduleId: module.id,
    styleUrls: ["visitObservationList.component.css"],
    templateUrl: "./visitObservationList.component.html"
})

/**
    Clase que posee las funciones asociadas a la lista de observaciones locales
    @class VisitObservationListComponent
    @author Abdalah Masrie <abdalah.masrie@sigis.com.ve>
    @copyright SIGIS Soluciones Integrales GIS, C.A
    created on 2018/09/19
*/
export class VisitObservationListComponent extends BaseComponent{
    /**
        Listado de observaciones
        @property _observations
        @type {any}
    */
    private _observations: any = [];

    /**
        Método constructor
        @method constructor
        @param {Router} routes Objeto que permite navegar entre vistas
        @param {Page} page Referencia a la página
        @param {ActivatedRoute} activatedRoute Objeto que permite obtener los parámetros de query
        @param {RouterExtensions} routerExtensions Objeto que permite navegar por la pila de vistas
        @param {TranslateService} translate Objeto que permite traducir elementos
    */
    public constructor(
        router: Router,
        private page: Page,
        activatedRoute: ActivatedRoute,
        routerExtensions: RouterExtensions,
        translate: TranslateService
    ) {
        super(routerExtensions, translate, router, activatedRoute, null);

        this.page.on(Page.navigatingToEvent, (() => {
            this.initComponent();
        }));
    }

    /**
        Método que se ejecuta al entrar al componente, independientemente de si es por primera vez o por efecto de back
        @method initComponent
    */
    private initComponent(){
        this.showLoadingIndicator(this.makeTranslation("loading"));
        this._observations = [];
        let cb = new CouchbaseService();
        cb.getDocumentDB(Config.id.local_observations).then(obs=>{
            this._observations = obs["data"];
            for(let i = 0; i < this._observations.length; i++){
                this._observations[i].fec_cre = moment(this._observations[i].
                    fec_cre).utcOffset(0,true).local().format("DD/MM/YYYY HH:mm");
            }
            this.hideLoadingIndicator();
        });
    }

    /**
        Función que retorna el listado de observaciones
        @method getObservations
        @return {any}
    */
    public getObservations(): any {
        return this._observations;
    }

    /**
        Método que es disparado por un evento de selección de elemento. Redirecciona al formulario de la observación seleccionada
        @method onItemSelected
        @property {ListViewEventData} args Contenido asociado al elemento
    */
    public onItemSelected(args: ListViewEventData){
        const listView = args.object as RadListView;
        const selectedItems = listView.getSelectedItems() as Array<any>;
        listView.deselectItemAt(args.index);
        this.navigate("/visitObservation/"+selectedItems[0]._id);
    }
}
