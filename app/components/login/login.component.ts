import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { Page } from "tns-core-modules/ui/page";
import { CouchbaseService } from "../../services/couchbase.service";
import * as localStorage from 'nativescript-localstorage';
import * as Toast from 'nativescript-toast';
import {TranslateService} from 'ng2-translate';
import { Config } from "../../config/config";
var application = require('application');
var md5 = require("md5");
import { BaseComponent } from "../base/base.component";
import {screen} from "platform"
import { Mapbox } from "nativescript-mapbox";
import { resumeEvent, on as applicationOn, suspendEvent } from "application";

@Component({
    selector: "gr-login",
    styleUrls: ["components/login/login.component.css"],
    templateUrl: "components/login/login.component.html"
})

/**
    Clase que posee las funciones asociadas al componente de login
    @class LoginComponent
    @author Abdalah Masrie <abdalah.masrie@sigis.com.ve>
    @copyright SIGIS Soluciones Integrales GIS, C.A
    created on 2018/05/30
*/
export class LoginComponent extends BaseComponent implements OnInit{
    /**
        Instancia del servicio de Couchbase
        @property couchbaseService
        @type {CouchbaseService}
    */
    private couchbaseService: CouchbaseService = new CouchbaseService();
    /**
        Propiedad que oculta la vista mientras se verifica si ha iniciado sesión
        @property isLoading
        @type {boolean}
    */
    public isLoading: boolean = true;
    /**
        Propiedad que se relaciona con el campo de correo electrónico en la vista
        @property userEmail
        @type {string}
    */
    public userEmail: string;
    /**
        Propiedad que se relaciona con el campo de contraseña en la vista
        @property userPassword
        @type {string}
    */
    public userPassword: string;
    /**
        Propiedad que se indica la cuánto hay que mover el Layout
        @property translateY
        @type {number}
    */
    public translateY: number;
    /**
        Indicador de aplicaicón iniciada por primera vez
        @property appStart
        @type {boolean}
    */
    public appStart: boolean;

    /**
        Método constructor
        @method constructor
        @param {Router} router Objeto que permite navegar entre vistas
        @param {Page} page Referencia a la página
        @param {TranslateService} translate Instancia del servicio de traducción
        @param {ChangeDetectorRef} changeDetector Objeto que verifica cambios en la vista del menú lateral
    */
    public constructor(
        router: Router,
        private page: Page,
        translate: TranslateService,
        changeDetector: ChangeDetectorRef
    ) {
        super(null, translate, router, null, changeDetector);
        this.showLoadingIndicator(this.makeTranslation("loading"));
        this.page.on(Page.loadedEvent, event => {
      		if (application.android) {
      			application.android.on(application.AndroidApplication.activityBackPressedEvent, this.backEvent);
      		}
      	});
      	this.page.on(Page.unloadedEvent, event => {
      		if (application.android) {
      			application.android.off(application.AndroidApplication.activityBackPressedEvent, this.backEvent);
      		}
        });
        applicationOn(suspendEvent, (_) => {
            let amazon = localStorage.getItem(Config.storage.amazon);
            amazon = amazon ? JSON.parse(amazon) : null
            let ve_gen = localStorage.getItem(Config.storage.ve_gen);
            ve_gen = ve_gen ? JSON.parse(ve_gen) : null;
            let yanomami_br = localStorage.getItem(Config.storage.yanomami_br);
            yanomami_br = yanomami_br ? JSON.parse(yanomami_br) : null;
            let bolivar = localStorage.getItem(Config.storage.bolivar);
            bolivar = bolivar ? JSON.parse(bolivar) : null;
            if((amazon && amazon.status == "0") || (ve_gen && ve_gen.status == "0") || (yanomami_br && yanomami_br.status == "0") || (bolivar && bolivar.status == "0")){
                Toast.makeText(this.makeTranslation("downloads_paused")).show();
            }
        });
        applicationOn(resumeEvent, (_) => {
            let amazon = localStorage.getItem(Config.storage.amazon);
            amazon = amazon ? JSON.parse(amazon) : null
            let ve_gen = localStorage.getItem(Config.storage.ve_gen);
            ve_gen = ve_gen ? JSON.parse(ve_gen) : null;
            let yanomami_br = localStorage.getItem(Config.storage.yanomami_br);
            yanomami_br = yanomami_br ? JSON.parse(yanomami_br) : null;
            let bolivar = localStorage.getItem(Config.storage.bolivar);
            bolivar = bolivar ? JSON.parse(bolivar) : null;
            let mapDownloading = false;
            if(amazon && amazon.status == "0"){
                mapDownloading = true;
                this.downloadMap(amazon.idNotification, amazon.name, amazon.style, amazon.minZoom, amazon.maxZoom, amazon.bounds, amazon.message, false);
            }
            if(ve_gen && ve_gen.status == "0"){
                mapDownloading = true;
                this.downloadMap(ve_gen.idNotification, ve_gen.name, ve_gen.style, ve_gen.minZoom, ve_gen.maxZoom, ve_gen.bounds, ve_gen.message, false);
            }
            if(bolivar && bolivar.status == "0"){
                mapDownloading = true;
                this.downloadMap(bolivar.idNotification, bolivar.name, bolivar.style, bolivar.minZoom, bolivar.maxZoom, bolivar.bounds, bolivar.message, false);
            }
            if(yanomami_br && yanomami_br.status == "0"){
                mapDownloading = true;
                this.downloadMap(yanomami_br.idNotification, yanomami_br.name, yanomami_br.style, yanomami_br.minZoom, yanomami_br.maxZoom, yanomami_br.bounds, yanomami_br.message, false);
            }
            if(mapDownloading){
                Toast.makeText(this.makeTranslation("downloads_resumed")).show();
            }
        });
        if(localStorage.getItem(Config.storage.user)){
            this.hideLoadingIndicator();
            this.navigate("/drawer");
        }else{
            this.hideLoadingIndicator();
            this.isLoading = false;
        }
        this.page.actionBarHidden = true;
    }

    /**
        Método que se ejecuta seguido del constructor. Realiza sincronización automática al iniciar la aplicación
        @method ngOnInit
    */
    ngOnInit(){
        this.mapbox = new Mapbox();
    	this.translateY = screen.mainScreen.heightDIPs > 900 ? -20 : 50;
        let value = this.couchbaseService.getDocument(Config.id.doctor_finger);
        this.appStart = value ? true : false;
        setTimeout( () => {
            if(!this.appStart){
                this.sync();
            }
        }, 100);
    }  
    
    /**
        Método que maneja el comportamiento del botón back. Al ser presionado, cierra la aplicación
        @method backEvent
        @param {any} args Argumentos del evento
    */
    private backEvent(args) {
        if (application.android) {
  		    application.android.foregroundActivity.finish();
        }
    }

    /**
        Método que se ejecuta al presionar el botón de iniciar sesión. Verifica que exista un usuario con el correo y contraseña indicados para guardar una sesión y dirigirse a la pantalla de inicio
        @method submit
    */
    public submit() {
        //this.navigate("/map");

        if(this.userPassword && this.userEmail){
            if(this.validateEmail(this.userEmail)){
                this.couchbaseService.getDocumentDB(this.userEmail).then(user => {
                    console.log("user ", user);
                    console.log(md5(this.userEmail+this.userPassword))
                    if(user && user["data"] && user["data"]["ema_usu"] == this.userEmail){
                        if(user["data"]["pas_usu"] == md5(this.userEmail+this.userPassword)){
                            if(user["data"]["validated"]){
                                if(user["data"]["est_usu"]){
                                    localStorage.setItem(Config.storage.user, JSON.stringify(user));
                                    this.navigate("/drawer");
                                }else{
                                    Toast.makeText(this.makeTranslation("user_inactive")).show();
                                }    
                            }else{
                                Toast.makeText(this.makeTranslation("user_not_validated")).show();
                            }    
                        }else{
                            Toast.makeText(this.makeTranslation("user_no_pass")).show();
                        }
                    } else{
                        Toast.makeText(this.makeTranslation("user_not_found")).show();
                    }
    
                }).catch(err => {
                    console.log(err);
                });
            }else{
                Toast.makeText(this.makeTranslation("invalid_email")).show();

            }
        }else{
            Toast.makeText(this.makeTranslation("empty_fields")).show();
        }
    }

    /**
        Método que realiza pull de Sync Gateway para descargar los datos al dispositivo y poder acceder
        @method sync
    */
    public sync(){
        if(!this.isBusy){
            this.showLoadingIndicator(this.makeTranslation("syncing"));
            this.syncPull(this.couchbaseService).then( _ => {
                console.log("connected");
                this.hideLoadingIndicator();
                if(!this.appStart){
                    Toast.makeText(this.makeTranslation("download_first_map"), "long").show();
                    this.downloadMaps();
                    this.appStart = true;
                }
            }, _ => {
                console.log("no connection");
                this.hideLoadingIndicator();
            });
        }
    }

    /**
        Función que verifica si un texto dado es un correo electrónico
        @method validateEmail
        @param {string} email Dirección de correo electrónico
        @return {boolean} Indicador de correo válido
    */
    public validateEmail(email: string): boolean {
        let emailRegEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/i;
        return emailRegEx.test(email);
    }

    /** 
        Método que se encarga de iniciar sesión a través del lector de huellas
        @method fingerLogin
    */
    public fingerLogin(){
        if(!this.isBusy){
            this.couchbaseService.getDocumentDB(Config.id.doctor_finger).then(fingerprintDoc =>{
                if(!fingerprintDoc){
                    Toast.makeText(this.makeTranslation("must_sync")).show();
                }else{
                    this.checkPlugin(true, null, (fingerNumber, fingerImage, fingerPlugin) => {
                        //Al encontrar una huella, se compara con el resto
                        console.log("fingerprintDoc ", fingerprintDoc)
                        this.recursiveFingerComparison(0, fingerprintDoc, fingerPlugin, fingerImage);
                    })    
                }
            }).catch(err => {
                console.log(err);
            });
        }
    }

    /**
        Método que evalua recursivamente las huellas
        @method recursiveFingerComparison
        @param {number} number Posición de la lista a recorrer
        @param {any} fingerprintDoc Documento de la huella obtenida
        @param {Hf4000pFingerprint} fingerPlugin Instancia del plugin que procesa huellas
        @param {any} fingerImage Base64 de la huella
    */
    public recursiveFingerComparison(number, fingerprintDoc, fingerPlugin, fingerImage){
        let usertIdFound = null;
        if(number < fingerprintDoc["data"].length){
            this.couchbaseService.getDocumentDB(fingerprintDoc["data"][number]).then(finger => {
                let comparison = fingerPlugin.fingerCompare(finger["data"]["huella"], fingerImage);
                console.log("compare ", comparison)
                if(comparison >= 50){
                    usertIdFound = finger["data"]["doc_id"];
                    this.checkUserFound(usertIdFound);
                }else{
                    this.recursiveFingerComparison(number+1, fingerprintDoc, fingerPlugin, fingerImage);
                }
            }).catch(err => {
                console.log(err);
            });    
        }else{
            this.checkUserFound(usertIdFound);
        }
    }

    /**
        Método que verifica si se obtuvo un usuario para poder continuar    
        @method checkUserFound
        @param {string} usertIdFound Documento de huella obtenido
     */
    public checkUserFound(usertIdFound){
        if(usertIdFound){
            this.couchbaseService.getDocumentDB(usertIdFound).then(user => {
                if(user["data"]["est_usu"]){
                    localStorage.setItem(Config.storage.user, JSON.stringify(user));
                    this.hideLoadingIndicator();
                    this.changeDetector.detectChanges();
                    this.navigate("/drawer");
                }else{
                    Toast.makeText(this.makeTranslation("user_inactive")).show();
                }    
            }).catch(err => {
                console.log(err);
            });
        }else{
            Toast.makeText(this.makeTranslation("user_not_found")).show();
        }
        this.hideLoadingIndicator();
        this.changeDetector.detectChanges();
    }
    
    /** 
        Método que inicia la animación de la pantalla de login
        @method startBackgroundAnimation
        @param {any} background Representación del Layout donde se ejecutará al animación
    */
    startBackgroundAnimation(background) {
        background.animate({
            scale: { x: 1.1, y: 1.1 },
            duration: 1000 * 2
        });
    }

}
