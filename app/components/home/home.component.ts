import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { TranslateService } from 'ng2-translate';
import { CouchbaseService } from "../../services/couchbase.service";
import { Config } from "../../config/config";
import { Page } from "ui/page";
import * as moment from 'moment';
import { BaseComponent } from "../base/base.component";

@Component({
    selector: "home-component",
    moduleId: module.id,
    templateUrl: "./home.component.html",
    styleUrls: ['home.component.css']
})

/**
    Clase que posee las funciones asociadas al componente de la vista de inicio
    @class HomeComponent
    @author Abdalah Masrie <abdalah.masrie@sigis.com.ve>
    @copyright SIGIS Soluciones Integrales GIS, C.A
    created on 2018/05/30
*/

export class HomeComponent extends BaseComponent implements OnInit {
    /**
        Mensaje de visita
        @property visitMessage
        @type {string}
    */
    private visitMessage: string;
    /**
        Método constructor
        @method constructor
        @param {Page} page Referencia a la página
        @param {Router} routes Objeto que permite navegar entre vistas
        @param {TranslateService} translate Objeto que permite traducir elementos
        @param {ChangeDetectorRef} changeDetector Detector de cambios
    */
    constructor(private page: Page, routes: Router, translate: TranslateService, changeDetector:ChangeDetectorRef) {
        super(null, translate, routes, null, changeDetector);
        this.blockBack();
        this.page.on(Page.navigatingToEvent, (() => {
            localStorage.setItem("finger", "");
            localStorage.setItem("match", "");
        }));	        
    }

    /**
        Método que se ejecuta seguido del constructor
        @method ngOnInit
    */
    ngOnInit(): void {
        let cb = new CouchbaseService();
        let visit = Config.id.visits;
        let visits = cb.getDocument(visit)//.then(visits => {
            let visitOnGoing = false;
            let today = moment();
            let setStartDate = null;
            if(visits && visits["data"]){
                for(var i = 0; i < visits["data"].length; i++){
                    let startDate = moment(visits["data"][i].fec_ini_vis).utcOffset(0,true).local();
                    let close = visits["data"][i].vis_act;
                    if(today.isAfter(startDate) && !close){
                        if(!setStartDate || setStartDate.isBefore(startDate)){
                            setStartDate = startDate;
                            visitOnGoing = true;
                            let txtMsg = this.makeTranslation("visit_in_progress");
                            this.visitMessage = txtMsg + moment(visits["data"][i].fec_ini_vis).utcOffset(0,true).local().format("DD/MM/YYYY HH:mm");
                        }
                    }
                }
            }
            if(!visitOnGoing){
                this.visitMessage = this.makeTranslation("no_visit");
            }
//        })

    }        
}
