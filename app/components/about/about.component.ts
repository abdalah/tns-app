import { Component } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { TranslateService } from 'ng2-translate';
import { BaseComponent } from "../base/base.component";
import * as appversion from "nativescript-appversion";

@Component({
    selector: "about",
    moduleId: module.id,
    templateUrl: "./about.component.html",
    styleUrls: ['about.component.css']
})

/**
    Clase que posee las funciones asociadas al componente de información acerca de la aplicación
    @class AboutComponent
    @author Abdalah Masrie <abdalah.masrie@sigis.com.ve>
    @copyright SIGIS Soluciones Integrales GIS, C.A
    created on 2018/07/30
*/
export class AboutComponent extends BaseComponent{
    /**
        Información de la aplicación
        @property info
        @type {string}
    */
    private info: string;
    /**
        Versión de la aplicación
        @property version
        @type {string}
    */
   private version: string;

    /**
        Método constructor
        @method constructor
        @param {RouterExtensions} routerExtensions Objeto que permite navegar por la pila de vistas
        @param {TranslateService} translate Objeto que permite traducir elementos
    */
    public constructor(
        routerExtensions: RouterExtensions,
        translate: TranslateService
    ) {
        super(routerExtensions, translate, null, null, null);
        this.info = this.makeTranslation("about_app");
        
        appversion.getVersionName().then((version: string) => {
            this.version = this.makeTranslation("current_version") + version;
        });

    }

}