import { Component, OnInit, ViewChild, ChangeDetectorRef } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { Page } from "tns-core-modules/ui/page";
import { RouterExtensions } from "nativescript-angular/router";
import { Doctor } from "../../models/doctor.model"
import { RadDataFormComponent } from "nativescript-ui-dataform/angular";
import { TranslateService } from 'ng2-translate';
import { CouchbaseService } from "../../services/couchbase.service";
import { Config } from "../../config/config";
import { BaseComponent } from "../base/base.component";
import * as localStorage from 'nativescript-localstorage';
var application = require('application');

@Component({
    selector: "doctor-profile",
    moduleId: module.id,
    styleUrls: ["doctor-profile.component.css"],
    templateUrl: "./doctor-profile.component.html"
})

/**
    Clase que posee las funciones asociadas al formulario de perfil del médico
    @class DoctorProfileComponent
    @author Abdalah Masrie <abdalah.masrie@sigis.com.ve>
    @copyright SIGIS Soluciones Integrales GIS, C.A
    created on 2018/10/25
*/
export class DoctorProfileComponent extends BaseComponent implements OnInit{
    /**
        Instancia de médico
        @property _doctor
        @type {Doctor}
    */
    private _doctor: Doctor;
    /**
        Instancia del servicio de couchbase
        @property couchbaseService
        @type {CouchbaseService}
    */
    private couchbaseService: CouchbaseService = new CouchbaseService();
    /**
        Representación del formulario
        @property docProfileForm
        @type {RadDataFormComponent}
    */
    @ViewChild('docProfileForm') docProfileForm: RadDataFormComponent;
    /** 
        Indicador de huella de médico encontrada
        @property fingerFound
        @type {boolean}
    */
    private fingerFound: boolean;
    /** 
        Última huella obtenida (texto)
        @property fingerSaved
        @type {string}
    */
    private fingerSaved: string;
    /** 
        Última huella obtenida (identificador)
        @property lastFinger
        @type {string}
    */
    private lastFinger: string;
    /** 
        Lista de todos los identificadores de huellas nuevas, sin orden ni tamaño específico
        @property fingerList
        @type {any}
    */
    private fingerList: any = [];


    /**
        Método constructor
        @method constructor
        @param {Router} router Objeto que permite navegar entre vistas
        @param {Page} page Referencia a la página
        @param {ActivatedRoute} activatedRoute Objeto que permite obtener los parámetros de query
        @param {RouterExtensions} routerExtensions Objeto que permite navegar por la pila de vistas
        @param {TranslateService} translate Objeto que permite traducir elementos
        @param {ChangeDetectorRef} changeDetector Objeto que verifica cambios en la vista del menú lateral
    */
    public constructor(
        router: Router,
        private page: Page,
        activatedRoute: ActivatedRoute,
        routerExtensions: RouterExtensions,
        translate: TranslateService,
        changeDetector: ChangeDetectorRef
    ) {
        super(routerExtensions, translate, router, activatedRoute, changeDetector);
    }

    /**
        Función que retorna los datos obtenidos del médico
        @method getDoctor
        @return {Doctor}
    */
    private getDoctor(): Doctor {
        return this._doctor;
    }

    /**
        Función que carga la información previamente almacenada en base de datos sobre el indígena identificado
        @method ngOnInit
    */
    ngOnInit() {
        this.page.on(Page.loadedEvent, event => {
            if (application.android) {
                application.android.on(application.AndroidApplication.activityBackPressedEvent, (this.doctorBack).bind(this));
            }
        });

        this.page.on(Page.unloadedEvent, event => {
            if (application.android) {
                application.android.removeEventListener(application.AndroidApplication.activityBackPressedEvent);
            }
        });
        let json = JSON.parse(localStorage.getItem(Config.storage.user));
        this.showLoadingIndicator(this.makeTranslation("loading"));
        this._doctor = new Doctor(json);
        //obtener la huella del médico
        this.couchbaseService.getDocumentDB(json._id).then(doctorDocument=>{
            this.fingerFound = doctorDocument["data"] && doctorDocument["data"]["hue_usu"] && doctorDocument["data"]["hue_usu"].length > 0 ? true : false;
            let pictureId = doctorDocument["data"]["img_usu"]; 
            this.loadPictureSrc(pictureId, true);
            if(this.fingerFound){
                this.lastFinger = doctorDocument["data"]["hue_usu"][0];
                this.couchbaseService.getDocumentDB(doctorDocument["data"]["hue_usu"][0]).then(fingerDoc=>{
                    this.fingerSaved = this.getFingerName(fingerDoc["data"]["mano"]);
                    this.changeDetector.detectChanges();
                    this.hideLoadingIndicator();
                });
            }else{
                this.hideLoadingIndicator();
            }
        });
    }

    /**
        Método disparado una vez se haya validado un campo en el formulario
        @method onPropertyValidated
        @param {any} args Argumentos de validación
    */
    private onPropertyValidated(args){
        let property = args.propertyName;
        let valueCandidate = args.entityProperty.valueCandidate;
        let isValid = args.entityProperty.isValid;
        if(isValid){
            if(property == "firstName"){
                this.getDoctor().setFirstName(valueCandidate);
            }else if(property == "lastName"){
                this.getDoctor().setLastName(valueCandidate);
            }
        }
    }

    /**
        Método que almacena el modelo en base de datos y regresa a la ventana anterior
        @method saveModel
    */
    private saveModel(args){
        if(!this.isBusy){
            this.showLoadingIndicator(this.makeTranslation("saving"));
            //realizar validaciones
            if(this.validateForm()){
                if(this.imageFile){
                    this.savePictureFile(this.imageFile);
                }
                let localDoctor = JSON.parse(localStorage.getItem(Config.storage.user));
                this.couchbaseService.getDocumentDB(localDoctor["_id"]).then(doctorDocument=>{
                    this.alterPictureDocument(doctorDocument, "img_usu", "usuarios", (imgId)=>{
                        let json = {
                            "_id": localDoctor["_id"],
                            "type": "usuarios",
                            "data": {
                              "ape_usu": this.getDoctor().getLastName(),
                              "ema_usu": localDoctor["_id"],
                              "est_usu": doctorDocument["data"]["est_usu"],
                              "fec_cre": doctorDocument["data"]["fec_cre"],
                              "fec_mod": this.getDoctor().getDateModified(),
                              "hue_usu": this.lastFinger ? [this.lastFinger] : [],
                              "nom_usu": this.getDoctor().getFirstName(),
                              "pas_usu": doctorDocument["data"]["pas_usu"],
                              "validated": true,
                              "img_usu": imgId
                            }
                        };
                        //modificación de datos de paciente
                        this.couchbaseService.updateDocumentDB(localDoctor["_id"], json).then(()=>{
                            localStorage.setItem(Config.storage.user, JSON.stringify(json));
                            this.updateFingerDocument(doctorDocument["data"]["hue_usu"][0], this.lastFinger, ()=>{
                                this.hideLoadingIndicator();
                                //regresar a la vista anterior
                                this.back(this.isBusy);
                            });
                        });
                    });
                });
            }else{
                this.hideLoadingIndicator();
            }
        }
    }

    /**
        Función que se encarga de realizar las validaciones básicas sobre el formulario
        @method validateForm
        @return {boolean} Verdadero si el formulario es válido, falso en caso de error
    */
    private validateForm(){
        let validated = true;
        const firstName = this.docProfileForm .dataForm.getPropertyByName("firstName");
        if(!firstName.valueCandidate || firstName.valueCandidate.trim().length == 0){
            validated = this.setError("empty_field", "firstName", firstName, this.docProfileForm);
        }else if(firstName.valueCandidate.length > 100){
            validated = this.setError("length_exceeded", "firstName", firstName, this.docProfileForm);
        }
        const lastName = this.docProfileForm .dataForm.getPropertyByName("lastName");
        if(!lastName.valueCandidate || lastName.valueCandidate.trim().length == 0){
            validated = this.setError("empty_field", "lastName", lastName, this.docProfileForm);
        }else if(lastName.valueCandidate.length > 100){
            validated = this.setError("length_exceeded", "lastName", lastName, this.docProfileForm);
        }
        return validated;
    }

    /**
        Método que obtiene una nueva huella y la asocia al médico
        @method getNewFinger 
    */
    private getNewFinger(){
        let json = JSON.parse(localStorage.getItem(Config.storage.user));
        let docId = json._id;
        this.couchbaseService.getDocumentDB(docId).then(doctorDoc=>{
            let pastFinger = doctorDoc["data"]["hue_usu"][0] || "1";
            //solicita la nueva huella
            this.checkPlugin(true, pastFinger, (fingerNumber, fingerImage, fingerPlugin) => {
                //crea el documento correspondiente en couchbase
                this.couchbaseService.createDocumentDB({
                    "data":{
                        "huella": fingerImage,
                        "mano": fingerNumber,
                        "doc_id": docId
                    },
                    "type": "huella_doctor"
                }).then(docId => {
                    this.lastFinger = docId.toString();
                    this.fingerList.push(this.lastFinger);
                    //refresca la vista
                    this.fingerFound = true;
                    this.fingerSaved = this.getFingerName(fingerNumber);
                    this.hideLoadingIndicator();
                    this.changeDetector.detectChanges();
                });
            });
    
        });
    }

    /**
        Método que elimina cualquier huella creada que no se decidió guardar antes de regresar
        @method doctorBack
    */
    public doctorBack(){
        this.recursiveDelete(this.fingerList, ()=>{
            this.back();
        });
    }    

    /**
        Método que actualiza el documento de huellas, eliminando los innecesarios
        @method updateFingerDocument
        @param {any} oldFinger Huella anterior a la actualización
        @param {any} finger Huella actualizada
        @param {any} fn Función callback
    */
    public updateFingerDocument(oldFinger, finger, fn){
        let listFingersDeleting = [];
        //obtiene la lista de huellas de médicos
        this.couchbaseService.getDocumentDB(Config.id.doctor_finger).then((list)=>{
            let listIdFingers = list["data"];
            //recorre el listado de huellas creadas y se queda con la última
            while(this.fingerList.length > 0){
                let fingerData = this.fingerList.pop();
                if(finger == fingerData){
                    //se debe agregar también al listado de huellas
                    listIdFingers.push(fingerData);
                }else{
                    //agrega la huella a la lista de eliminados
                    listFingersDeleting.push(fingerData);
                }
            }
            //si la huella actual es distinta de la antigua y existe, se agrega a la lsita de eliminados
            if(finger && oldFinger && finger != oldFinger){
                listFingersDeleting.push(oldFinger);
                listIdFingers.splice(listIdFingers.indexOf(oldFinger), 1);
            }
            this.couchbaseService.updateDocumentDB(Config.id.doctor_finger, {"data": listIdFingers}).then(()=>{
                this.recursiveDelete(listFingersDeleting, fn);
            });
        });
    }

}
