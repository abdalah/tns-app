import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { Page } from "tns-core-modules/ui/page";
import { RouterExtensions } from "nativescript-angular/router";
import { RadDataFormComponent } from "nativescript-ui-dataform/angular";
import { TranslateService } from 'ng2-translate';
import { CouchbaseService } from "../../services/couchbase.service";
import { Consultation } from "../../models/consultation.model"
import * as localStorage from 'nativescript-localstorage';
import { Config } from "../../config/config";
import { BaseComponent } from "../base/base.component";
import { Image } from "tns-core-modules/ui/image";
import * as moment from 'moment';
import { isAndroid } from "platform";

@Component({
    selector: "consultation",
    moduleId: module.id,
    templateUrl: "./consultation.component.html"
})

/**
    Clase que posee las funciones asociadas al formulario de consulta del indígena
    @class ConsultationComponent
    @author Abdalah Masrie <abdalah.masrie@sigis.com.ve>
    @copyright SIGIS Soluciones Integrales GIS, C.A
    created on 2018/07/20
*/
export class ConsultationComponent extends BaseComponent implements OnInit{
    /**
        Modelo de consulta
        @property _consultation
        @type {Consultation}
    */
    private _consultation: Consultation;
    /**
        Datos del perfil del indígena
        @property profile
        @type {any}
    */
    private profile: any;
    /**
    	Indicador de sexo femenino
    	@property isFemale
    	@type {boolean}
    */
    private isFemale: boolean;
    /**
    	Indicador de nueva consulta
    	@property isNew
    	@type {boolean}
    */
    private isNew: boolean;
    /**
        Indicador de consulta agregada
        @property addedConsultation
        @type {boolean}
    */
    private addedConsultation: boolean = false;
    /** 
        Lista de medicamentos para la enfermedad
        @property medicineList
        @type {any}
    */
    private medicineList: any = [];
    /**
        Identificador de la patología
        @property patologyId
        @type {string}
    */
    private patologyId: string;
    /**
        Representación del formulario
        @property medicalForm
        @type {RadDataFormComponent}
    */
    @ViewChild('medicalForm') medicalForm: RadDataFormComponent;

    /**
        Método constructor
        @method constructor
        @param {Router} router Objeto que permite navegar entre vistas
        @param {Page} page Referencia a la página
        @param {ActivatedRoute} activatedRoute Objeto que permite obtener los parámetros de query
        @param {RouterExtensions} routerExtensions Objeto que permite navegar por la pila de vistas
        @param {TranslateService} translate Objeto que permite traducir elementos
        @param {ChangeDetectorRef} changeDetector Objeto que verifica cambios en la vista del menú lateral
    */
    public constructor(
        router: Router,
        private page: Page,
        activatedRoute: ActivatedRoute,
        routerExtensions: RouterExtensions,
        translate: TranslateService,
        changeDetector: ChangeDetectorRef
    ) {
        super(routerExtensions, translate, router, activatedRoute, changeDetector);
    }

    /**
        Método que se ejecuta después de ejecutar el constructor
        @method ngOnInit
    */
    ngOnInit() {
        this.showLoadingIndicator(this.makeTranslation("loading"));
        let cb = new CouchbaseService();
        this.patologyId = Config.id.oncocercosis;
        //se obtiene la lista de medicamentos para la patología
        cb.getDocumentDB(this.patologyId).then(value =>{
            let list = value["data"];
            this.iterateMedicine(0, cb, list, [], (newList)=>{
                this.medicineList = this.setListPicker(newList, "id", "name", null, false);
                //Obtener el perfil del usuario en base al id
                let id = this.activatedRoute.snapshot.params["id"];
                let consultationId = this.activatedRoute.snapshot.params["consultation"];
                cb.getDocumentDB(consultationId).then((consultation)=>{
                    let pictureId = consultation["data"] && consultation["data"]["img_con"] ? consultation["data"]["img_con"] : null; 
                    this.isNew = consultation["data"] && consultation["data"]["fec_cre"] ? false : true;
                    cb.getDocumentDB(id).then((elem)=>{
                        this.profile = elem;
                        //Con esto, se debe setear si es mujer y la dosis de ivermectina
                        this.isFemale = this.profile["data"] && this.profile["data"]["gen_pac"] == "F";
                        let json = JSON.parse(localStorage.getItem(Config.storage.user));
                        let doctor = json.data._id;
                        if(this.isNew){
                            //si es nuevo, crea un nuevo registro
                            this._consultation = new Consultation(id, doctor, localStorage.getItem(Config.storage.finger), localStorage.getItem(Config.storage.match));
                            this._consultation.setMedicine(Config.id.ivermectina);
                        }else{
                            //si no es nuevo debe cargar los datos anteriores
                            this._consultation = new Consultation(id, doctor, consultation["data"]["finger"], consultation["data"]["match"], consultation["data"]["fec_cre"]);
                            this._consultation.setLactancy(consultation["data"]["lac_con"] && this.isFemale);
                            this._consultation.setLocation(consultation["data"]["loc_con"]);
                            this._consultation.setReluctance(consultation["data"]["ren_con"]);
                            this._consultation.setSick(consultation["data"]["enf_con"]);
                            this._consultation.setIsPregnant(consultation["data"]["emb_con"] && this.isFemale);
                            this._consultation.setObservations(consultation["data"]["obs_con"]);
                            this._consultation.setMedicine(consultation["data"]["med_con"]);
                        }
                        //manejo de dosis
                        this.getNewDose(cb, ()=>{
                            let position = localStorage.getItem(Config.storage.position);
                            position = position ? JSON.parse(position) : null;
                            let time = (new Date()).getTime();
                            //si hay una posición vigente, almacenar
                            if(position && time - position.time < 900000){
                                this.getConsultation().setLocation(position.location);
                            }
                            //si no hay posición o el tiempo superó 15 minutos, buscar otra
                            if(!this.getConsultation().getLocation()){
                                setTimeout(()=>{
                                    this.getLocation( location => {
                                        if(location){
                                            this.getConsultation().setLocation(location);
                                            let json = {
                                                "time": time,
                                                "location": location
                                            };
                                            localStorage.setItem(Config.storage.position, JSON.stringify(json));
                                        }
                                        this.loadPictureSrc(pictureId);
                                    });
                                },500);
                            }else{
                                this.hideLoadingIndicator();
                                this.loadPictureSrc(pictureId);
                            }
                        })
                    });
                });
            })
        })
    }

    /**
        Método que se encarga de iterar sobre la lista de medicamentos
        @method iterateMedicine
        @param {number} position Posición del arreglo que recorre
        @param {any} cb Instancia de Couchbase
        @param {any} list Lista de medicamentos
        @param {any} newList Lista con los medicamentos para la patología indicada
        @param {any} fn Función callback
    */
    public iterateMedicine(position, cb, list, newList, fn){
        if(position < list["lis_med"].length){
            cb.getDocumentDB(list["lis_med"][position]).then((doc)=>{
                if(doc["data"].act_med){
                    let json = {
                        "id": doc._id,
                        "name": doc["data"].nom_med + " " + doc["data"].pre_med
                    }
                    newList.push(json);    
                }
                this.iterateMedicine(position+1, cb, list, newList, fn);
            })
        }else{
            fn(newList);
        }
    }

    /**
        Método disparado una vez se haya validado un campo en el formulario
        @method onPropertyValidated
        @param {any} args Argumentos de validación
    */
    private onPropertyValidated(args){
        let cb = new CouchbaseService();
        let property = args.propertyName;
        let valueCandidate = args.entityProperty.valueCandidate;
        let isValid = args.entityProperty.isValid;
        if(isValid){
            if(property != "observations"){
                if(property == "isPregnant"){
                    this.getConsultation().setIsPregnant(valueCandidate.toString() == "true");
                }else if(property == "fstWeekLactancy"){
                    this.getConsultation().setLactancy(valueCandidate.toString() == "true");
                }else if(property == "treatmentReluctant"){
                    this.getConsultation().setReluctance(valueCandidate.toString() == "true");
                }else if(property == "seriouslySick"){
                    this.getConsultation().setSick(valueCandidate.toString() == "true");
                }else if(property == "medicine"){
                    this.showLoadingIndicator(this.makeTranslation("loading"));
                    let selectedMedicine = valueCandidate;
                    if(isAndroid){
                        for(var i = 0; i < this.medicineList.items.length; i++){
                            if(this.medicineList.items[i][this.medicineList.label] == valueCandidate){
                                selectedMedicine = this.medicineList.items[i][this.medicineList.key];
                                break;
                            }
                        }
                    }
                    this.getConsultation().setMedicine(selectedMedicine);
                    this.getNewDose(cb, ()=>{
                        this.hideLoadingIndicator();
                    })
                }
                if(property == "seriouslySick" || property == "treatmentReluctant" || property == "fstWeekLactancy" || property == "isPregnant"){
                    this.showLoadingIndicator(this.makeTranslation("loading"));
                    if(valueCandidate.toString() == "true"){
                        this.getConsultation().setDose("0");
                        this.hideLoadingIndicator();
                    }else{
                        this.getNewDose(cb, ()=>{
                            this.hideLoadingIndicator();
                        })
                    }                    
                }
                this.updateFormDocument();
            }
        }
    }

    /**
        Método que verifica las condiciones para obtener la nueva dosis tras haber un cambio
        @method getNewDose
        @param {any} cb Instancia de couchbase
        @param {any} fn Función callback
    */
    public getNewDose(cb, fn){
        let patientId = this.activatedRoute.snapshot.params["id"];
        cb.getDocumentDB(patientId).then(doc=>{
            let patientDoc = doc["data"];
            let patientHeight = patientDoc["est_pac"];
            let patientGender = patientDoc["gen_pac"];
            let consultation = this.getConsultation();
            let patientPregnant = consultation.getIsPregnant().toString() == "true";
            if(!(consultation.getLactancy().toString() == "true" ||
                consultation.getReluctance().toString() == "true" ||
                consultation.getSick().toString() == "true" || 
                consultation.getMedicine() == "0" || 
                this.getConsultation().getMedicine() == "0" ||
                this.getConsultation().getMedicine() == "-1")){
                    let medicineId = consultation.getMedicine();
                    cb.getDocumentDB(medicineId).then(medicineDoc=>{
                        let doseList = medicineDoc["data"]["dosis"];
                        this.iterateDose(0, patientGender, patientHeight, patientPregnant, doseList, cb, (newDose)=>{
                            this.getConsultation().setDose(newDose);
                            this.updateFormDocument();
                            fn();
                        });
                    });
            }else{
                this.getConsultation().setDose("0");
                this.updateFormDocument();
                fn();
            }
        });
    }

    /**
        Método que itera sobre las dosis
        @method iterateDose
        @param {number} position Posición del listado a recorrer
        @param {boolean} patientGender Género del paciente
        @param {boolean} patientHeight Estatura del paciente
        @param {boolean} patientPregnant Indicador de paciente embarazada
        @param {any} doseList Lista de dosis
        @param {any} cb Función Callback
        @param {any} fn Función callback
    */
    public iterateDose(position, patientGender, patientHeight, patientPregnant, doseList, cb, fn){
        if(position < doseList.length){
            cb.getDocumentDB(doseList[position]).then(dose=>{
                let doseDoc = dose["data"];
                let doseMinHeight = doseDoc["est_min"];
                let doseMaxHeight = doseDoc["est_max"];
                let doseGender = doseDoc["dos_sex"];
                let dosePregnant = doseDoc["dos_emb"];
                if((!doseGender || doseGender == patientGender) && 
                    patientHeight >= doseMinHeight && 
                    (dosePregnant || !patientPregnant) &&
                    (!doseMaxHeight || patientHeight <= doseMaxHeight)){
                        fn(doseDoc["can_dosis"].toString());
                }else{
                    this.iterateDose(position+1, patientGender, patientHeight, patientPregnant, doseList, cb, fn);
                }
            });
        }else{
            fn("0");
        }
    }

    /**
        Método que permite actualizar de formulario
        @method updateDocument
    */
    public updateFormDocument(){
        let newElem = this.getConsultation().clone();
        this._consultation = newElem;
    }

    /**
        Función que retorna los datos obtenidos de la consulta
        @method getConsultation
        @return {Consultation}
    */
    private getConsultation(): Consultation {
        return this._consultation;
    }

    /**
        Método que almacena el modelo en base de datos y regresa a la ventana anterior
        @method saveModel
        @param {any} args Argumentos de evento
    */
    private saveModel(args){
        //validar el formulario
        if(!this.isBusy && this.validateForm()){
            this.showLoadingIndicator(this.makeTranslation("saving"));
            if(this.isNew){
                //caso de creación
                if(!this.addedConsultation){
                    let cb = new CouchbaseService();
                    let patient = this.activatedRoute.snapshot.params["id"];
                    let consultationId = this.activatedRoute.snapshot.params["consultation"];
                    cb.getDocumentDB(patient).then(jsonPatient=>{
                        jsonPatient["data"].con_pac.push(consultationId);
                        cb.updateDocumentDB(patient, jsonPatient).then(()=>{
                            this.addedConsultation = true;
                            this.processLocationConsultation(args);
                        });
                    });
                }else{
                    this.processLocationConsultation(args);
                }
            }else{
                this.processLocationConsultation(args);
            }
        }
    }

    /**
        Método que procesa ubicación
        @method processLocationConsultation
        @param {any} args Argumentos de evento
    */
    public processLocationConsultation(args){
        //si no hay ubicación todavía, se debe mostrar una alerta correspondiente 
        if(!this.getConsultation().getLocation()){
            this.checkLocation(json => {
                if(json.res){
                    this.getConsultation().setLocation(json.val);
                }
                this.addValues(args);
            });
        }else{
            this.addValues(args);
        }
    }

    /**
        Función que valida los campos en el formulario
        @method validateForm
        @return {boolean}
    */
    private validateForm(){
        let validated = true;
        let select = this.makeTranslation("select");
        const selectedMedicine = this.medicalForm.dataForm.getPropertyByName("medicine");
        if(this.getConsultation().getMedicine() == "0"){
            validated = this.setError("must_select_option", "medicine", selectedMedicine, this.medicalForm);
        }
        return validated;
    }   
    
    /**
        Método que agrega los valores restantes al modelo, lo guarda y regresa a la vista anterior
        @method addValues
        @param {any} args Argumentos de evento
    */
    private addValues(args){
        let cb = new CouchbaseService();
        let isPregnant = this.medicalForm.dataForm.getPropertyByName("isPregnant");
        let fstWeekLactancy = this.medicalForm.dataForm.getPropertyByName("fstWeekLactancy");
        let medicineDose = this.medicalForm.dataForm.getPropertyByName("dose");
        let reluctance = this.medicalForm.dataForm.getPropertyByName("treatmentReluctant");
        let sickness = this.medicalForm.dataForm.getPropertyByName("seriouslySick");
        let observations = this.medicalForm.dataForm.getPropertyByName("observations");
        let observationsValue = observations.valueCandidate ? observations.valueCandidate : this.getConsultation().getObservations();

        this.getConsultation().setLactancy(fstWeekLactancy.valueCandidate);
        this.getConsultation().setReluctance(reluctance.valueCandidate);
        this.getConsultation().setSick(sickness.valueCandidate);
        this.getConsultation().setIsPregnant(isPregnant.valueCandidate);
        this.getConsultation().setDose(medicineDose.valueCandidate);
        this.getConsultation().setObservations(observationsValue);

        let consultationId = this.activatedRoute.snapshot.params["consultation"];
        let patientId = this.activatedRoute.snapshot.params["id"];
        let store = JSON.parse(localStorage.getItem(Config.storage.user));
        cb.getDocumentDB(consultationId).then(consultationData=>{
            this.alterPictureDocument(consultationData, "img_con", "consultas", (imgId)=>{
                let json = {
                    "data": {
                        "pac_id": patientId,
                        "fec_cre": this.getConsultation().getDateCreated(),
                        "fec_mod": this.getConsultation().getDateModified(),
                        "loc_con": this.getConsultation().getLocation(),
                        "emb_con": this.getConsultation().getIsPregnant().toString() == "true",
                        "lac_con": this.getConsultation().getLactancy().toString() == "true",
                        "obs_con": this.getConsultation().getObservations() || "",
                        "dos_con": this.getConsultation().getDose(),
                        "ren_con": this.getConsultation().getReluctance().toString() == "true",
                        "enf_con": this.getConsultation().getSick().toString() == "true",
                        "med_con": this.getConsultation().getMedicine(),
                        "img_con": imgId,
                        "id_pat": this.patologyId,
                        "finger": this.getConsultation().getFinger(),
                        "match": this.getConsultation().getMatch(),
                        "doc_id": store.data.ema_usu
                    },
                    "type": "consultas"
                };
                cb.updateDocumentDB(consultationId, json).then(()=>{
                    //se actualiza la lista temporal
                    cb.getDocumentDB(Config.id.local_patients).then(listPatientData=>{
                        cb.getDocumentDB(patientId).then(info=>{
                            let patientInfo = info["data"];
                            let patientData = {
                                "_id": patientId,
                                "gen_pac": patientInfo.gen_pac,
                                "nom_pac": patientInfo.nom_pac,
                                "fec_cre": moment(this.getConsultation().getDateCreated()).utcOffset(0,true).local().format("DD/MM/YYYY HH:mm")
                            };
                            let found = false;
                            for(var i = 0; i < listPatientData["data"].length; i++){
                                if(listPatientData["data"][i]["_id"] == patientId){
                                    found = true;
                                    listPatientData["data"][i] = patientData;
                                    break;
                                }
                            }
                            let consultationFound = false;
                            for(var i = 0; i < listPatientData["consultations"].length; i++){
                                if(listPatientData["consultations"][i] == consultationId){
                                    consultationFound = true;
                                    break;
                                }
                            }
                            if(!consultationFound){
                                listPatientData["consultations"].push(consultationId);
                            }
                            if(!found){
                                listPatientData["data"].push(patientData);
                            }
                            cb.updateDocumentDB(Config.id.local_patients, listPatientData).then(()=>{
                                localStorage.removeItem(Config.storage.map_consultations);
                                this.hideLoadingIndicator();
                                this.backEvent(args);    
                            });
                        });
                    });
                });
            })
        });
    }

    /**
        Método que maneja el comportamiento del botón back
        @method backEvent
        @param {any} args Argumentos del evento
    */
    private backEvent(args) {
        if(!this.isBusy){
            let cb = new CouchbaseService();
            let consultationId = this.activatedRoute.snapshot.params["consultation"];
            cb.getDocumentDB(consultationId).then(consultation=>{
                if(!consultation["type"]){
                    cb.deleteDocumentDB(consultationId).then(()=>{
                        this.back();
                    }).catch(err => {
                        console.log(err);
                        this.back();
                    });
                }else{
                    this.back();
                }
            });
        }
    }
}
