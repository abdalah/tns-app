import { Component, ChangeDetectorRef } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { Page } from "tns-core-modules/ui/page";
import { RouterExtensions } from "nativescript-angular/router";
import { PlatformLocation } from "@angular/common";
import { TranslateService } from 'ng2-translate';
import { CouchbaseService } from "../../services/couchbase.service";
import * as moment from 'moment';
var application = require('application');
import { BaseComponent } from "../base/base.component";
import { Config } from "../../config/config";
import { confirm } from "tns-core-modules/ui/dialogs";

@Component({
    selector: "patient-options",
    moduleId: module.id,
    templateUrl: "./patient-options.component.html",
    styleUrls: ['patient-options.component.css']
})

/**
    Clase que posee las funciones asociadas al componente de opciones del paciente
    @class PatientOptionsComponent
    @author Abdalah Masrie <abdalah.masrie@sigis.com.ve>
    @copyright SIGIS Soluciones Integrales GIS, C.A
    created on 2018/07/02
*/
export class PatientOptionsComponent extends BaseComponent{
    /**
        Identificador de Perfil encontrado
        @property foundProfile
        @type {boolean}
    */
    private foundProfile: boolean;
    /**
        Identificador de consultas encontradas
        @property foundConsultation
        @type {boolean}
    */
    private foundConsultation: boolean;
    /**
        Objeto con datos de perfil
        @property profile
        @type {any}
    */
    private profile: any = {};
    /**
        Objeto con datos de última consulta
        @property lastConsultation
        @type {any}
    */
    private lastConsultation: any = {};
    /**
        Método constructor
        @method constructor
        @param {Router} router Objeto que permite navegar entre vistas
        @param {Page} page Referencia a la página
        @param {ActivatedRoute} activatedRoute Objeto que permite obtener los parámetros de query
        @param {RouterExtensions} routerExtensions Objeto que permite navegar por la pila de vistas
        @param {PlatformLocation} location Ubicación del usuario en la aplicación
        @param {TranslateService} translate Objeto que permite traducir elementos
        @param {ChangeDetectorRef} changeDetector Objeto que verifica cambios en la vista del menú lateral
    */
    public constructor(
        router: Router,
        private page: Page,
        activatedRoute: ActivatedRoute,
        routerExtensions: RouterExtensions,
        private location : PlatformLocation,
        translate: TranslateService,
        changeDetector: ChangeDetectorRef
    ) {
        super(routerExtensions, translate, router, activatedRoute, changeDetector);
        this.page.on(Page.navigatingToEvent, (() => {
            this.initComponent();
        }));
    }

    /**
        Método que se ejecuta al entrar al componente, independientemente de si es por primera vez o por efecto de back
        @method initComponent
    */
    private initComponent(){
        const id = this.activatedRoute.snapshot.params["id"];
        this.page.on(Page.loadedEvent, event => {
            if (application.android) {
                setTimeout(()=> {
                    application.android.on(application.AndroidApplication.activityBackPressedEvent, (this.backPatientOptBefore).bind(this));
                }, 500)
            }
        });

        this.page.on(Page.unloadedEvent, event => {
            if (application.android) {
                application.android.removeEventListener(application.AndroidApplication.activityBackPressedEvent);
            }
        });
        this.showLoadingIndicator(this.makeTranslation("loading"));
        this.lastConsultation = {};
        let cb = new CouchbaseService();
        cb.getDocumentDB(id).then(document => {
            if(document && document["type"] == "pacientes"){
                cb.getDocumentDB(Config.id.areas).then(areaList => {
                    this.getChild(areaList, document["data"]["are_geo_pac"], "id_are_geo", "lis_sub_are", (subareaList)=>{
                        this.getChild(subareaList, document["data"]["sub_are_pac"], "id_sub_are", "lis_com", (communityList)=>{
                            let groupName;
                            for(var i = 0; i < communityList["data"].length; i++){
                                if(communityList["data"][i]["id_com"] == document["data"]["id_com"]){
                                    groupName = communityList["data"][i]["nom_com"];
                                    break;
                                }
                            }
                            this.foundProfile = document["data"]["nom_pac"] ? true : false;
                            this.profile.name = this.makeTranslation("name") + ": " + document["data"]["nom_pac"];
                            this.profile.height = this.makeTranslation("height") + ": " + document["data"]["est_pac"];
                            this.profile.year_born = this.makeTranslation("year_born") + ": " + ( (new Date()).getFullYear() - document["data"]["fec_nac"]);
                            this.profile.gender = this.makeTranslation("gender") + ": " + this.makeTranslation(document["data"]["gen_pac"] == "M" ? "male": "female");
                            this.profile.community = this.makeTranslation("community") + ": " + groupName;
                
                            let listConsultations = document["data"]["con_pac"];
                            this.foundConsultation = listConsultations.length > 0 ? true : false;
                            if(this.foundConsultation){
                                let last = listConsultations[listConsultations.length - 1];
                                cb.getDocumentDB(last).then(lastConsultation => {
                                    cb.getDocumentDB(lastConsultation["data"].doc_id).then(doctor => {
                                        this.lastConsultation.dateCreated = this.makeTranslation("last_evaluation") + moment(lastConsultation["data"].fec_cre).utcOffset(0,true).local().format("DD/MM/YYYY HH:mm");
                                        this.lastConsultation.doctor = this.makeTranslation("by_doctor") + doctor["data"].nom_usu + " " + doctor["data"].ape_usu;
                                        this.changeDetector.detectChanges();
                                        this.hideLoadingIndicator();
                                    });
                                });
                            }else{
                                this.changeDetector.detectChanges();
                                this.hideLoadingIndicator();
                            }
                        });
                    });
                });
            }else{
                this.hideLoadingIndicator();
            }
        });
    }

    /**
        Método que realiza la navegación a una nueva vista, tomando sólo el id del paciente
        @method navigateTo
        @param {string} str Dirección a la que se quiere navegar
    */
    public navigateTo(str: string){
        application.android.removeEventListener(application.AndroidApplication.activityBackPressedEvent);
        const id = this.activatedRoute.snapshot.params["id"];
        this.navigate(str+id);
    }

    /**
        Método que identifica la vista a la que debe navegar según la cantidad de consultas registradas
        @method checkConsultation
    */
    public checkConsultation(){
        if(this.foundConsultation){
            this.navigateTo('/consultationList/');
        }else{
            this.newConsultation('/consultation/');
        }
    }

    /**
        Método que forma parte del evento que se dispara al presionar el back físico de Android.
        Cancela el back físico y valida envía la alerta de eliminación en caso de paciente sin huellas
        antes de continuar.
        @method backPatientOptBefore
        @param {any} args Argumento del evento
    */
    public backPatientOptBefore(args){
        args.cancel = true;
        this.backPatient();
    }

    /**
        Método que envía una alerta antes de regresar en caso de que no se posean huellas
        @backPatient
    */
    public backPatient(){
        const id = this.activatedRoute.snapshot.params["id"];
        let cb = new CouchbaseService();
        cb.getDocumentDB(id).then(doc => {
            if(!doc || !doc["data"]){
                let options = {
                    title: this.makeTranslation("fingercheck"),
                    cancelButtonText: this.makeTranslation("cancel"),
                    okButtonText: this.makeTranslation("accept"),
                    cancelable: false,
                    message: this.makeTranslation("no_finger_back")
                };
                confirm(options).then((result: boolean) => {
                    if(result){
                        cb.deleteDocumentDB(id).then(()=>{
                            this.back();
                        }).catch(err => {
                            console.log(err);
                            this.back();
                        });
                    }
                });
            }else{
                this.back();
            }
        });
    }

}
