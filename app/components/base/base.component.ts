import { Component, ChangeDetectorRef } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { Router, ActivatedRoute } from "@angular/router";
import { TranslateService } from 'ng2-translate';
import { CouchbaseService } from "../../services/couchbase.service";
import { Config } from '~/config/config';
import * as Toast from 'nativescript-toast';
import { Hf4000pFingerprint } from 'nativescript-hf4000p-fingerprint';
import { confirm, action } from "tns-core-modules/ui/dialogs";
import { Mapbox, MapStyle, DownloadProgress } from "nativescript-mapbox";
import { LocationService } from "../../services/location.service"
import { CameraService } from "../../services/camera.service";
import { NotificationService } from "../../services/notification.service";
import { LoadingIndicator } from 'nativescript-loading-indicator';
import { ImageSource, fromBase64, fromAsset, fromResource, fromFile } from "image-source";
import {Folder, path, knownFolders} from "file-system";
import * as application from "application";
import { isIOS, isAndroid } from "platform";
import * as utils from "utils/utils";
import * as frame from "ui/frame";
import * as moment from 'moment';
import * as underscore from 'underscore';
require("nativescript-nodeify");
import { Buffer } from 'buffer';

@Component({
    selector: "base",
    template: ""
})
/**
	Clase que implementa funcionalidades genéricas
	@class BaseComponent
	@author Abdalah Masrie <abdalah.masrie@sigis.com.ve>
	@copyright SIGIS Soluciones Integrales GIS, C.A
	created on 2018/09/11
*/

export class BaseComponent {
    /**
        Propiedad que muestra el loader mientras se verifica si se posee una sesión activa
        @property isBusy
        @type {boolean}
    */
    public isBusy: boolean = false;
    /**
        Objeto que permite traducir elementos
        @property {TranslateService} translate
    */
    translate: TranslateService;
    /**
        Objeto que permite navegar por la pila de vistas
        @property {RouterExtensions} routerExtensions
    */
    routerExtensions: RouterExtensions;
    /**
        Objeto que permite obtener los parámetros de query
        @property {ActivatedRoute} activatedRoute
    */
    activatedRoute: ActivatedRoute;
    /**
        Objeto que permite navegar entre vistas
        @property {Router} router
    */
    router: Router;
    /**
        Objeto que verifica cambios en la vista del menú lateral
        @property {ChangeDetectorRef} changeDetector
    */
    changeDetector: ChangeDetectorRef;
    /**
        Instancia del servicio de localización
        @property locationService
        @type {LocationService}
    */
    locationService: LocationService = new LocationService();
    /** 
        Indicador de botón back bloqueado
        @property blockedBack
        @type {boolean}
    */
    blockedBack: boolean = false;
    /**
        Instancia del servicio de cámara
        @property cameraService
        @type {CameraService}
    */
    private cameraService: CameraService;
    /**
        Instancia del servicio de notificaciones
        @property notificationService
        @type {NotificationService}
    */
    private notificationService: NotificationService;
    /**
        Instancia de LoadingIndicator
        @property loadingIndicator
        @type {LoadingIndicator}
    */
    loadingIndicator: LoadingIndicator;
    /**
        Referencia a la imagen en base64
        @property picture
        @type {any}
    */
    picture: any;    
    /**
        Fuente de la imagen a mostrar
        @property imageSrc
        @type {any}
    */
    imageSrc: any;
    /** 
        Instancia de MapBox
        @property mapbox
        @type {MapBox}
    */
    mapbox: Mapbox;
    /** 
        Porcentaje asociado a descarga
        @property percentage
        @type {number}
    */
    percentage: number = -1;
    /**
        Último identificador de imagen
        @param lastPicId
        @type {string}
    */
    lastPicId: string = null;
    /**
        Imagen obtenida de la cámara para almacenar en archivo (perfil de usuario)
        @param imageFile
        @type {any}
    */
    imageFile: any;

    /**
		Método constructor
		@method constructor
        @param {RouterExtensions} routerExtensions Objeto que permite navegar por la pila de vistas
        @param {TranslateService} translate Objeto que permite traducir elementos
        @param {Router} router Objeto que permite navegar entre vistas
        @param {ActivatedRoute} activatedRoute Objeto que permite obtener los parámetros de query
        @param {ChangeDetectorRef} changeDetector Objeto que verifica cambios en la vista del menú lateral
	*/
	public constructor(
		routerExtensions: RouterExtensions,
        translate: TranslateService,
        router: Router,
        activatedRoute: ActivatedRoute,
        changeDetector: ChangeDetectorRef
	){
        this.translate = translate;
        this.routerExtensions = routerExtensions;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.changeDetector = changeDetector;
        this.cameraService = new CameraService();
        this.notificationService = new NotificationService();
        this.loadingIndicator = new LoadingIndicator();
    }

    /** 
        Método que oculta el loadingIndicator
        @method hideLoadingIndicator
    */
    public hideLoadingIndicator(){
        this.loadingIndicator.hide();
        this.isBusy = false;
    }

    /**
        Método que muestra el LoadingIndicator
        @method showLoadingIndicator
        @param {string} msg Mensaje a mostrar mientras se realiza la carga 
        @param {boolean} cancel Indicador de loader cancelable
    */
    public showLoadingIndicator(msg, cancel=false){
        this.isBusy = !cancel;
        this.loadingIndicator.show({ 
            message : msg,
            android: {
                cancelable: cancel,
                indeterminate: true
            }
        });
    }

    /**
        Muestra un listado de mapas disponibles para su descarga y 
        ejecuta la descarga si el usuario realiza la selección.
        @method downloadMaps
    */
    downloadMaps(){
        let amazon: any = localStorage.getItem("amazon");
        amazon = amazon ? JSON.parse(amazon) : null
        let ve_gen: any = localStorage.getItem("ve_gen");
        ve_gen = ve_gen ? JSON.parse(ve_gen) : null;
        let yanomami_br: any = localStorage.getItem("yanomami_br");
        yanomami_br = yanomami_br ? JSON.parse(yanomami_br) : null;
        let bolivar: any = localStorage.getItem("bolivar");
        bolivar = bolivar ? JSON.parse(bolivar) : null;

        var options = [
            amazon && amazon.status == "1" ? (this.makeTranslation("amazon") + " *") : this.makeTranslation("amazon"), 
            bolivar && bolivar.status == "1" ? (this.makeTranslation("bolivar") + " *") : this.makeTranslation("bolivar"), 
            yanomami_br && yanomami_br.status == "1" ? (this.makeTranslation("yanomami_br") + " *") : this.makeTranslation("yanomami_br"), 
            ve_gen && ve_gen.status == "1" ? (this.makeTranslation("ve_gen") + " *") : this.makeTranslation("ve_gen")
        ];
            
        action({
            message: this.makeTranslation("map_download"),
            cancelButtonText: this.makeTranslation("cancel"),
            actions: options
        }).then(result => {
            var index =  options.indexOf(result);
            let msg = this.makeTranslation("downloading")+" "+result+" ";
            if(index!=-1){
                switch (index) {
                    case 0:
                        this.downloadMap(index, "amazon", MapStyle.SATELLITE_STREETS, 8, 10, {
                            north: 4.719,
                            east: -68.577,
                            south: 0.615,
                            west: -61.710
                        },msg);
                        break;
                    case 1:
                        this.downloadMap(index, "bolivar", MapStyle.SATELLITE_STREETS, 8, 10, {
                            north: 7.901,
                            east: -67.033,
                            south: 3.815,
                            west: -60.167
                        },msg);
                        break;
                    case 2:
                        this.downloadMap(index, "yanomami_br", MapStyle.SATELLITE_STREETS, 8, 10, {
                            north: 4.215,
                            east: -65.484,
                            south: 0.110,
                            west: -58.618
                        }, msg);
                        break;
                    case 3:
                        this.downloadMap(index, "ve_gen", MapStyle.SATELLITE_STREETS, 1, 5, {
                            north: 15.496,
                            east: -74.355,
                            south: -0.747,
                            west: -46.890
                        }, msg);
                        break;
                }  
            }
        });     
    }

    /**
        Descarga los datos de  un mapa seleccionado
        @method downloadMap
        @param {number} idNotification Identificador de notificación
        @param {string} name Nombre del mapa a descargar
        @param {string} style Estilo del mapa a mostrar
        @param {number} minZoom Zoom mínimo permitido
        @param {number} maxZoom Zoom máximo permitido
        @param {any} bounds Estilo del mapa a mostrar        
        @param {string} message Mensaje de desacarga
        @param {boolean} isNew Indicador de descarga nueva
    */
    downloadMap(idNotification, name, style, minZoom, maxZoom, bounds, message, isNew = true){
        if(isNew){
            Toast.makeText(this.makeTranslation(message)).show();
        }
        let json = {
            idNotification: idNotification,
            name: name,
            style: style,
            minZoom: minZoom,
            maxZoom: maxZoom,
            bounds: bounds,
            message: message,
            status: "0"
        };
        localStorage.setItem(name, JSON.stringify(json));
        this.percentage = 0;
        this.mapbox.downloadOfflineRegion({
            accessToken: Config.mapBoxTokenKey,
            name: this.makeTranslation(name),
            style: style,
            minZoom: minZoom,
            maxZoom: maxZoom,
            bounds: bounds,
            onProgress: (progress: DownloadProgress) => {
                console.log(`Download progress: ${JSON.stringify(progress)}`);
                if(this.percentage < progress.percentage && this.percentage > -1){
                    this.percentage = progress.percentage;
                }
                if(progress.complete){
                    this.percentage = -1;
                }
            }
        }).then(
            () => {
                let json = {status: "1"};
                localStorage.setItem(name, JSON.stringify(json));
                let nameTrsl = this.makeTranslation(name);
                this.notificationService.createNotification(idNotification, this.makeTranslation("map_downloaded") + nameTrsl).then(()=>{
                    this.notificationService.addEvent(() => {
                        this.notificationService.cancelNotification(idNotification).then( () => {});
                    });
                }).catch(error => {
                    Toast.makeText(this.makeTranslation(error)).show();
                });
            },
            error => {
                localStorage.removeItem(name);
                Toast.makeText(this.makeTranslation("download_error")).show();
                console.log(error);
            }
        );
    }
    
    /**
        Método que oculta el teclado
        @method hideKeyboard
    */
    public hideKeyboard() {    
        var UIApplication;
        if (isIOS) {
		frame.topmost().nativeView.endEditing(true);
        }
        if (isAndroid) {
            const dialogFragment = application.android
                .foregroundActivity
                .getFragmentManager()
                .findFragmentByTag("dialog");
            if (dialogFragment) {
                utils.ad.dismissSoftInput(dialogFragment.getDialog().getCurrentFocus());
            } else {
                utils.ad.dismissSoftInput();
            }
        }
    }

    /**
        Función que realiza una traducción
        @method makeTranslation
        @param {string} str Nombre del elemento a traducir
    */
    public makeTranslation(str){
        let val;
        this.translate.get(str).subscribe((res: string) => {
            val = res;
        });
        return val;
    }

    /** 
        Método que bloquea la funcionalidad de back
        @method blockBack
    */
    public blockBack(){
        this.blockedBack = true;
    }

    /**
        Método que maneja el comportamiento del botón back
        @method back
        @param {boolean} busy Indicador de vista en espera (por defecto false)
    */
    public back(busy = false) {
        this.hideKeyboard();
        if(!busy && !this.blockedBack){
            this.routerExtensions.back();
        }
    }

    /**
        Función que setea un mensaje de error en el formulario
        @method setError
        @param {string} msg Mensaje a traducir
        @param {string} prop Nombre de propiedad seleccionada
        @param {any} arg Argumento del formulario
        @param {any} form Referencia al formulario donde se mostrará el error
        @return {boolean} Siempre retorna false
    */
    public setError(msg, prop, arg, form): boolean{
        arg.errorMessage = this.makeTranslation(msg);
        form.dataForm.notifyValidated(prop, false);
        return false;
    }

    /**
        Método que realiza la navegación a una nueva vista, tomando el id del paciente y agregando un nuevo documento
        @method newConsultation
        @param {string} str Dirección a la que se quiere navegar
    */
    public newConsultation(str: string){
        const id = this.activatedRoute.snapshot.params["id"];
        let cb = new CouchbaseService();
        cb.createDocumentDB({})
        .then((consultationId)=>{
            this.router.navigate([str+id+"/"+consultationId]);
        })
        .catch((err)=>{
            console.log(err);
        });
    }

    /**
        Método que se encarga de realizar la navegación a la ubicación indicada
        @method navigate
        @param {string} str Dirección a la que se redirecciona
    */
    public navigate(str: string){
        if(!this.isBusy){
            this.router.navigate([str]);
        }
    }

    /**
        Método que realiza el pull de couchbase
        @method syncPull
        @param {CouchbaseService} cb Instancia del servicio de couchbase
    */
    public syncPull(cb: CouchbaseService){
        return new Promise( (resolve, reject) => {
            if(cb.checkConnection()){
                let replicator = cb.startSyncPull(Config.couchbase, false);            
                this.checkReplicator(replicator, cb).then( _ => {
                    cb.stopSyncPull();
                    resolve();
                }).catch( _ => {
                    cb.stopSyncPull();
                    reject();
                });
            }else{
                Toast.makeText(this.makeTranslation("no_connection")).show();
                cb.stopSyncPull();
                reject();
            }
        });
    }

    /**
        Método que realiza el push de couchbase
        @method syncPush
        @param {CouchbaseService} cb Instancia del servicio de couchbase
    */
    public syncPush(cb: CouchbaseService){
        return new Promise( (resolve, reject) => {
            if(cb.checkConnection()){
                let replicator = cb.startSyncPush(Config.couchbase, false);
                this.checkReplicator(replicator, cb).then( _ => {
                    cb.stopSyncPush();
                    resolve();
                }).catch( _ => {
                    cb.stopSyncPush();
                    reject();
                });
            }else{
                Toast.makeText(this.makeTranslation("no_connection")).show();
                cb.stopSyncPull();
                reject();
            }
        });
    }

    /**
        Método que crea un nuevo registro y pasa su id como parte del link
        @method newRecord
        @param {string} link Ruta de redirección
    */
    public newRecord(link: string){
        let cb = new CouchbaseService();
        cb.createDocumentDB({})
        .then((id)=>{
            this.router.navigate([link+id]);
        })
        .catch((err)=>{
            console.log(err);
        });

    }

    /**
        Método que verifica si la acción (Pull o Push) sigue ejecutándose
        @method checkReplicator
        @param { Replicator } replicator Instancia que indica el estado de la acción
        @param {CouchbaseService} cb Instancia del servicio de couchbase
    */
    private checkReplicator(replicator: any, cb: CouchbaseService){
        return new Promise( (resolve, reject) => {
            setTimeout( _ => {
                if(cb.checkConnection()){
                    if (replicator.isRunning()){
                        console.log('Replicator is still Running!');
                        return this.checkReplicator(replicator, cb).then( _ => resolve());
                    }
                    console.log('Replicator finished!');
                    resolve();
                }else{
                    Toast.makeText(this.makeTranslation("device_disconnected")).show();
                    reject();
                }
            }, 1000);
        });
    }

    /**
        Método que genera una lista indicada para áreas
        @method setListPicker
        @param {any} list Lista de elementos a manipular
        @param {string} id Nombre del identificador a emplear
        @param {string} property Propiedad a incluir en el json
        @param {string} order Indicador de orden. Alfabético por defecto
        @param {boolean} defaultUndefined Campo que indica si quiere un valor "Seleccione"
    */
    public setListPicker(list, id, property, order = null, defaultUndefined=true){
        if(order){
            list.sort(function(a,b){
                return (a[order] < b[order]) ? -1 : (a[order] > b[order]) ? 1 : 0;
            })
        }else{
            list.sort(function(a,b){
                return (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
            })
        }
        if(defaultUndefined){
            let defaultValue = {};
            defaultValue[id] = "0";
            defaultValue[property] = this.makeTranslation("select");
            list.unshift(defaultValue);
        }
        return {
            "key": id,
            "label": property,
            "items": list
        };
    }

    /**
        Función que recorre una lista hasta obtener el id de un documento el cual busca
        @method getChild
        @param {any} list Lista del documento a recorrer
        @param {string} val Valor a comparar
        @param {string} name Clave a buscar
        @param {string} found Clave del elemento a obtener
        @param {any} fn Función callback con el documento obtenido, si aplica
    */
    public getChild(list, val, name, found, fn){
        let result = null;
        let cb = new CouchbaseService();
        if(val != 0 || val != "0"){
            for(var i = 0; i < list.data.length; i++){
                if(list.data[i][name] == val){
                    result = list.data[i][found];
                    break;
                }
            }
            cb.getDocumentDB(result).then(value => {
                fn(value);
            });
        }else{
            fn(null);
        }
    }

    /**
        Función que elimina la imagen anterior y crea una nueva
        @method alterPictureDocument
        @param {any} parent Objeto que posee el documento
        @param {string} propertyName Nombre de la propiedad en el documento
        @param {string} typeAssoc Nombre del documento asociado
        @param {any} fn Función callback
        @return {any} Identificador de la imagen o null
    */
    public alterPictureDocument(parent, propertyName, typeAssoc, fn){
        let cb = new CouchbaseService();
        let image = parent["data"] && parent["data"][propertyName] ? parent["data"][propertyName] : null;
        if(image){
            cb.getDocumentDB(image).then(previousImage => {
                this.alterPictureAfterImage(previousImage, typeAssoc, cb, propertyName, image, parent, fn);
            });
        }else{
            this.alterPictureAfterImage(null, typeAssoc, cb, propertyName, image, parent, fn);
        }
    }

    /**
        Método que continua la lógica de AlterPicture Document
        @method alterPictureAfterImage
        @param {any} previousImage Documento de la imagen anterior
        @param {string} typeAssoc Elemento asociado
        @param {any} cb Instancia de couchbase
        @param {string} propertyName Nombre de la propiedad en el documento
        @param {string} image Identificador de la imagen
        @param {any} parent Objeto que posee el documento
        @param {any} fn Función callback
    */
    public alterPictureAfterImage(previousImage, typeAssoc, cb, propertyName, image, parent, fn){
        if(this.picture && this.picture != 1 && (!previousImage || !underscore.isEqual(this.picture.data, previousImage.image.data))){
            let imageJson = {
                "image": this.picture,
                "association": typeAssoc,
                "type": "imagenes"
            };
            cb.createDocumentDB(imageJson).then((id)=>{
                if(parent["data"] && parent["data"][propertyName]){
                    cb.deleteDocumentDB(parent["data"][propertyName]).then(()=>{
                        fn(id);
                    }).catch((err)=>{
                        console.log(err);
                        fn(id);
                    });
                }else{
                    fn(id);
                }
            }).catch((err)=>{
                console.log(err);
                fn(image);
            });
        }else{
            fn(image);
        }
    }

    /**
        Método que carga la fuente de la foto
        @method loadPictureSrc
        @param {string} id Identificador de imagen en Couchbase
        @param {boolean} profile Indica si la imagen a cargar es para el perfil de usuario
    */
    public loadPictureSrc(id, profile = false){
        if(profile){
            this.loadPictureFile();
        }
        if(!this.imageSrc){
            this.lastPicId = id;

            this.imageSrc = "res://loading_image";
            this.changeDetector.detectChanges();
    
            let worker = new Worker('../../services/worker-picture.service');
            worker.postMessage({
                id: id
            });
            worker.onmessage = (msg) => {
                let request = msg.data;
                this.picture = request.documentImage ? request.documentImage["image"] : null;
                let base64 = this.picture ? (new Buffer(this.picture)).toString("base64"): null;
                this.imageSrc = this.picture ? fromBase64(base64) : "res://camera_icon";
                if(profile && this.picture){
                    let imgSrc = new ImageSource();
                    imgSrc.loadFromBase64(base64);
                    this.savePictureFile(imgSrc);
                }
                this.changeDetector.detectChanges();
                worker.terminate();
            };
            worker.onerror = function(err) {
                console.log(err.message);
            }    
        }
    }
   
    /**
        Método que procesa la foto recibida tras llamar al servicio
        @method getPicture
    */
    public getPicture(){
        this.takePicture( (pic) => {
            fromAsset(pic).then( image => {
                this.imageFile = image;
                let base64 = image.toBase64String('png');
                this.picture = Buffer.from(base64, "base64"); 
                this.imageSrc = pic;
                this.changeDetector.detectChanges();
            });
        })
    }

    /**
        Método que se encarga de llamar al servicio para tomar una foto
        @method takePicture
        @param {any} cb Función callback
    */
    public takePicture(cb: any){
        this.cameraService.takePicture()
        .then( (picture) => {
            cb(picture);
        })
        .catch( error => {
            let errorMsg;
            if(error.length < 22){
                errorMsg = this.makeTranslation(error);
            }else{
                errorMsg = this.makeTranslation("camera_picture_error");
            }
            this.loadPictureSrc(this.lastPicId);
            Toast.makeText(errorMsg).show();
        })
    }

    /**
        Método que almacena una foto en un archivo
        @method savePictureFile
        @param {any} picture Imagen, en caso de que sea llamada desde un hilo
    */
    public savePictureFile(picture = null){
        const folderDest = knownFolders.currentApp();
        const pathDest = path.join(folderDest.path, "yanomapp_profile.png");
        const saved: boolean = picture ? picture.saveToFile(pathDest, "png") : (this.picture).saveToFile(pathDest, "png");
        if (saved) {
            console.log("Image saved successfully!");
        }
    }

    /**
        Método que carga una foto de un archivo
        @method loadPictureFile
    */
    public loadPictureFile(){
        try{
            const folder: Folder = <Folder> knownFolders.currentApp();
            const folderPath: string = path.join(folder.path, "yanomapp_profile.png");
            this.imageSrc = <ImageSource> fromFile(folderPath);
            this.picture = 1;
            this.changeDetector.detectChanges();    
        }catch(error){
            //ene ste caso, no hay imagen a cargar (el directorio no ha sido creado)
            console.log("error: ", error);
        }
    }

    /**
        Método que obtiene de manera recursiva un listado de documentos
        @method recursiveGet
        @param {any} list Arreglo de Id de documentos a obtener
        @param {any} fn Función callback
    */
    public recursiveGet(list, fn){
        let cb = new CouchbaseService();
        if(list.length > 0){
            cb.getDocumentDB(list[0].pop()).then(()=>{
                this.recursiveGet(list, fn);
            })
            .catch((err)=>{
                console.log(err);
                this.recursiveGet(list, fn);
            });
        }else{
            fn();
        }
    }

    /**
        Método que elimina de manera recursiva un listado de identificadores
        @method recursiveDelete
        @param {any} list Arreglo de Id de documentos a eliminar
        @param {any} fn Función callback
    */
    public recursiveDelete(list, fn){
        let cb = new CouchbaseService();
        if(list.length > 0){
            cb.deleteDocumentDB(list[0].pop()).then(()=>{
                this.recursiveDelete(list, fn);
            })
            .catch((err)=>{
                console.log(err);
                this.recursiveDelete(list, fn);
            });
        }else{
            fn();
        }
    }

    /**
        Método que verifica si se logró obtener ubicación geográfica
        @method checkLocation
        @param {any} fn Función callback
    */
    public checkLocation(fn: any){
        let json = {
            title: this.makeTranslation("geolocation_service"),
            cancelButtonText: this.makeTranslation("no_position"),
            okButtonText: this.makeTranslation("try_again"),
            cancelable: false,
            message: this.makeTranslation("position_not_found")
        };
        confirm(json).then((result: boolean) => {
            if(result){
                this.locationService.getLocation()
                .then( (location) => {
                    fn({res: true, val: location});
                })
                .catch( (error) => {
                    console.log(error)
                    let errorMsg = this.makeTranslation("error_location");
                    Toast.makeText(errorMsg).show();
                    this.checkLocation(fn);
                })
            }else{
                fn({res: false, val: null});
            }
        });
    }      

    /**
        Método que se encarga de llamar al Activity del plugin para obtener una huella
        @method checkPlugin
        @param {boolean} isMedic Indicador de huella de médico
        @param {string} fingerId Identificador de huella actual, si existe
        @param {any} fn Función callback
        @param {number} position Número que representa el dedo usado
    */
    public checkPlugin(isMedic = false, fingerId = null, fn = null, position = 0): void{
        this.showLoadingIndicator(this.makeTranslation("processing"));
        var fingerPlugin = new Hf4000pFingerprint(); 
        fingerPlugin.getFinger( (fingerNumber, fingerImage) => {
            if(!fingerImage){
                //Si no se encontró una huella, mostrar toast con error
                Toast.makeText(this.makeTranslation("no_fingerprint")).show();
                this.hideLoadingIndicator();
                this.changeDetector.detectChanges();
            }else{
                //Al encontrar una huella, se procesa en couchbase
                this.processFingerPrint(fingerImage, fingerNumber, fingerPlugin, isMedic, fingerId, fn);
            }
        }, position);
    }

    /**
        Método que se encarga de procesar la huella obtenida
        @method processFingerPrint
        @param {any} fingerPrintStr Huella obtenida
        @param {number} numFinger Número de dedo
        @param {Hf4000pFingerprint} fingerPrintObject Instancia del plugin lector de huellas
        @param {boolean} isMedic Indicador de huella de médico
        @param {string} fingerId Identificador de huella actual, si existe
        @param {any} fn Función callback
    */
    private processFingerPrint(fingerPrintStr: any, numFinger: number, fingerPrintObject: Hf4000pFingerprint, isMedic: boolean, fingerId: string, fn: any){
        let cb = new CouchbaseService();
        //se obtiene la lista de id de huellas
        if(isMedic){
            cb.getDocumentDB(Config.id.doctor_finger)
            .then((fingerprintDoc)=>{
                this.startFingerWorker(fingerId, fingerprintDoc, fingerPrintStr, isMedic, numFinger, fingerPrintObject, cb, fn);
            })
            .catch((err)=>{
                console.log(err);
            })
        }else{
            cb.getDocumentDB(Config.id.fingerprints[numFinger])
            .then((fingerprintDoc)=>{
                this.startFingerWorker(fingerId, fingerprintDoc, fingerPrintStr, isMedic, numFinger, fingerPrintObject, cb, fn);
            })
            .catch((err)=>{
                console.log(err);
            })

        }
    }

    /**
        Método que incia el worker de Validación de huellas
        @method startFingerWorker
        @param {string} fingerId Id de la huella
        @param {any} fingerprintDoc Documento de huella
        @param {string} fingerPrintStr Huella a comparar
        @param {boolean} isMedic Indicador de huella de médico
        @param {number} numFinger Número de huella
        @param {Hf4000pFingerprint} fingerPrintObject Instancia del plugin lector de huellas
        @param {any} cb Instancia de couchbase
        @param {any} fn Función callback
    */
    public startFingerWorker(fingerId, fingerprintDoc, fingerPrintStr, isMedic, numFinger, fingerPrintObject, cb, fn){
        let patientIdFound;
        let found = false;
        //se recorre la lista y se verifica cada documento presente
        let worker = new Worker('../../services/worker-finger.service');
        worker.postMessage({
            fingerId: fingerId,
            fingerprintDoc: fingerprintDoc,
            fingerPrintStr: fingerPrintStr,
            found: found
        });
        worker.onmessage = (msg) => {
            let request = msg.data;
            found = request.found;
            patientIdFound = request.patientIdFound;
            let match = request.match;
            let finger = request.finger;
            let options = {
                title: this.makeTranslation("fingercheck"),
                cancelButtonText: this.makeTranslation("cancel"),
                okButtonText: this.makeTranslation("accept"),
                cancelable: false
            };
            if(isMedic && !fingerId){
                fn(numFinger, fingerPrintStr, fingerPrintObject);
            }else if((isMedic && fingerId || (!isMedic && fingerId))){
                if(found){
                    Toast.makeText(this.makeTranslation("fingerprint_exists")).show();
                    this.hideLoadingIndicator();
                }else{
                    fn(numFinger, fingerPrintStr, fingerPrintObject);
                }
            }else{
                if(found){
                    //si se encontró la huella, se informa y pasa a la siguiente vista
                    options["message"] = this.makeTranslation("got_finger");
                    localStorage.setItem("finger", finger);
                    localStorage.setItem("match", match);
                    this.openDialog(true, patientIdFound, options, fingerPrintObject, cb);
                }else{
                    //si no se encontró la huella, se informa si se desea agregar. De ser así, crea, guarda y pasa a la siguiente vista, caso contrario skip
                    options["message"] = this.makeTranslation("finger_not_found");
                    this.openDialog(false, {"image":fingerPrintStr, "number": numFinger}, options, fingerPrintObject, cb);
                }    
            }    
            worker.terminate();
        };
        worker.onerror = function(err) {
            console.log(err.message);
        }
    }

    /** 
        Método que obtiene el nombre de la huella almacenada
        @method getFingerName
        @param {number} num Número de huella
        @return {string} Nombre del dedo solicitado
    */
    public getFingerName(num: number){
        let fingerSaved;
        switch(num){
            case 1:
                fingerSaved = this.makeTranslation("left_thumb");
                break;
            case 2:
                fingerSaved = this.makeTranslation("right_index");
                break;
            case 3: 
                fingerSaved = this.makeTranslation("left_index");
                break;
            default:
                fingerSaved = this.makeTranslation("right_thumb");
        }
        return fingerSaved;
    }

    /**
        Método que procesa la alerta generada y la respuesta del cliente
        @method openDialog
        @param {boolean} found indicador de huella encontrada
        @param {any} finger Identificador de paciente o de huella
        @param {any} options Json con elemntos del dialog
        @param {Hf4000pFingerprint} fingerPrintObject Instancia del plugin detector de huellas
        @param {CouchbaseService} cb Instancia del servicio de couchbase
    */
    private openDialog(found: boolean, finger: any, options: any, fingerPrintObject: Hf4000pFingerprint, cb: CouchbaseService){
        setTimeout( _ => {
            let patientId;
            this.hideLoadingIndicator();
            this.changeDetector.detectChanges();
            if(found){
                //obtiene el id del paciente                    
                patientId = finger;
                this.navigate("/patientOptions/"+patientId);
            }else{
                confirm(options).then((result: boolean) => {
                    if(result){
                        this.showLoadingIndicator(this.makeTranslation("processing"));
                        this.changeDetector.detectChanges();
                        //crea el documento de paciente 
                        cb.createDocumentDB({}).then((patientId)=>{
                            //crea el documento de huellas por cada una
                            fingerPrintObject.registerFingers( readedFingers => {
                                if(readedFingers && readedFingers.length > 0){
                                    this.recursiveReadFingerPrints(0, readedFingers, cb, patientId, [], (idFinger) =>{
                                        //actualiza el documento de paciente con el id de la huella
                                        cb.updateDocumentDB(patientId, {
                                            "data": {
                                                "pac_hue": idFinger,
                                                "fec_cre": moment().utc().format("YYYY-MM-DD HH:mm:ss"),
                                                "fec_mod": moment().utc().format("YYYY-MM-DD HH:mm:ss")
                                            }
                                        }).then(()=>{
                                            this.endDialog(patientId);
                                        }).catch((err)=>{
                                            console.log(err);
                                            this.endDialog(patientId);
                                        });
                                    });
                                }else{
                                    this.hideLoadingIndicator();
                                    this.changeDetector.detectChanges();
                                    Toast.makeText(this.makeTranslation("no_fingerprint")).show();
                                }
                            });

                        }).catch((err)=>{
                            console.log(err);
                        });
                    }
                });
            }
        }, 800);
    }

    /**
        Método que finaliza el LoadingIndicator y regresa a la vista anterior
        @method endDialog
        @param {string} patientId Id del paciente
    */
    public endDialog(patientId){
        this.hideLoadingIndicator();
        this.changeDetector.detectChanges();
        this.navigate("/patientOptions/"+patientId);
    }

    /**
        Método que de manera recursiva obtiene las huellas leídas
        @method recursiveReadFingerPrints
        @param {number} position Posición del arreglo que se recorre
        @param {any} readedFingers Lista de huellas leídas
        @param {any} cb Instancia de couchbase
        @param {string} patientId Id del paciente
        @param {any} idFinger Lista de Id de las huellas a ser agregadas
        @param {any} fn Función callback
    */
    public recursiveReadFingerPrints(position, readedFingers, cb, patientId, idFinger, fn){
        if(position < readedFingers.length){
            if(readedFingers[position]){
                cb.createDocumentDB({
                    "data":{
                        "huella": readedFingers[position],
                        "mano": position,
                        "pac_id": patientId
                    },
                    "type": "huellas"
                }).then(fingerPrintId => {
                    idFinger.push(fingerPrintId);
                    //agrega a las listas de huellas los nuevos id
                    cb.getDocumentDB(Config.id.fingerprints[position]).then(fingerList=>{
                        fingerList.data.push(idFinger[position]);
                        cb.updateDocumentDB(Config.id.fingerprints[position], {
                            "data": fingerList.data
                        }).then(()=>{
                            this.recursiveReadFingerPrints(position+1, readedFingers, cb, patientId, idFinger, fn);
                        })
                        .catch(err=>{
                            console.log(err);
                        });
                    }).catch(err=>{
                        console.log(err);
                    })
                }).catch(err =>{
                    console.log(err);
                });
            }else{
                //se almacena null para mantener el mismo orden de huellas
                idFinger.push(null);
                this.recursiveReadFingerPrints(position+1, readedFingers, cb, patientId, idFinger, fn);
            }
        }else{
            fn(idFinger);
        }
    }

    /**
        Método que obtiene la ubicación geográfica
        @method getLocation
        @param {any} fn Función callback
    */
    public getLocation(fn: any){
        this.locationService.getLocation()
        .then( (location) => {
            this.hideLoadingIndicator();
            fn(location);
        })
        .catch( _ => {
            let errorMsg = this.makeTranslation("error_location");
            Toast.makeText(errorMsg).show();
            this.hideLoadingIndicator();
            fn(null);
        })
    }

}
