import { NgModule, ErrorHandler, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";
import { NativeScriptUISideDrawerModule } from "nativescript-ui-sidedrawer/angular";
import { NativeScriptHttpModule } from "nativescript-angular/http";
import { TranslateModule, TranslateLoader, TranslateStaticLoader } from "ng2-translate";
import { Http } from "@angular/http";
import { NativeScriptUIDataFormModule } from "nativescript-ui-dataform/angular";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { registerElement } from 'nativescript-angular/element-registry';
import { CardView } from 'nativescript-cardview';
import { AppRoutingModule } from "./app.routing";
import { AppComponent } from "./app.component";

import { CouchbaseService } from "./services/couchbase.service";
import { LocationService } from "./services/location.service";
import { CameraService } from "./services/camera.service";
import { ErrorService } from "./services/error.service";

import { BaseComponent } from "./components/base/base.component";
import { LoginComponent } from "./components/login/login.component";
import { DrawerComponent } from "./components/drawer/drawer.component";
import { PatientOptionsComponent } from "./components/patientOptions/patient-options.component";
import { IndianProfileComponent } from "./components/indianProfile/indian-profile.component";
import { DoctorProfileComponent } from "./components/doctorProfile/doctor-profile.component";
import { ConsultationComponent } from "./components/consultation/consultation.component";
import { ConsultationListComponent } from "./components/consultationList/consultation-list.component";
import { FAQComponent } from "./components/faq/faq.component";
import { AboutComponent } from "./components/about/about.component";
import { VisitObservationComponent } from "./components/visitObservation/visitObservation.component";
import { VisitObservationListComponent } from "./components/visitObservationList/visitObservationList.component";

import { HomeComponent } from "./components/home/home.component";
import { SyncComponent } from './components/sync/sync.component';
import { MapComponent } from './components/map/map.component';
registerElement('CardView', () => CardView);
registerElement("MapboxView", () => require("nativescript-mapbox").MapboxView);
registerElement("Fab", () => require("nativescript-floatingactionbutton").Fab);

export function translateLoaderFactory(http: Http) {
    return new TranslateStaticLoader(http, "/i18n", ".json");
};

@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        NativeScriptModule,
        NativeScriptUIListViewModule,
		NativeScriptUISideDrawerModule,
        AppRoutingModule,
		NativeScriptUIDataFormModule,
		NativeScriptFormsModule,
        NativeScriptHttpModule,
        TranslateModule.forRoot(
            [
                {
                    provide: TranslateLoader,
                    deps: [Http],
                    useFactory: (translateLoaderFactory)
                }
            ]
        )
    ],
    declarations: [
        AppComponent,
        BaseComponent,
        LoginComponent,
        PatientOptionsComponent,
        IndianProfileComponent,
        DoctorProfileComponent,
        ConsultationComponent,
        ConsultationListComponent,
        DrawerComponent,
        HomeComponent,
        FAQComponent,
        AboutComponent,
        VisitObservationComponent,
        VisitObservationListComponent,
        SyncComponent,
        MapComponent
    ],
    providers: [
        CouchbaseService,
        LocationService,
        CameraService,
        {
            provide: ErrorHandler,
            useClass: ErrorService,
        }
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ],
    entryComponents: [
    ],
})
/*
Pass your application module to the bootstrapModule function located in main.ts to start your app
*/
export class AppModule { }
