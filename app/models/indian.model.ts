import * as moment from 'moment';

/**
    Clase que maneja los datos asociados al perfil de un indígena
    @class Indian
    @author Abdalah Masrie <abdalah.masrie@sigis.com.ve>
    @copyright SIGIS Soluciones Integrales GIS, C.A
    created on 2018/07/03
*/
export class Indian {
    /**
        Identificador de indígena
        @property indianId
        @type {string}
    */
    private indianId: string;
    /**
        Nombre del indígena
        @property name
        @type {string}
    */
    private name: string;
    /**
        Año de nacimiento
        @property yearBorn
        @type {number}
    */
    private yearBorn: number;
    /**
        Identificador de comunidad nueva
        @property newCommunity
        @type {boolean}
    */
    private newCommunity: boolean;
    /**
        Identificador de comunidad si ya existe
        @property communityList
        @type {string}
    */
    private communityList: string;
     /**
        Nombre de comunidad nueva
        @property communityText
        @type {string}
    */
    private communityText: string;
   /**
        Identificador de género
        @property gender
        @type {string}
    */
    private gender: string;
    /**
        Estatura del indígena
        @property height
        @type {number}
    */
    private height: number;
    /**
        Identificador de primera división territorial de procedencia
        @property firstProcedence
        @type {string}
    */
    private firstProcedence: string;
    /**
        Identificador de segunda división territorial de procedencia
        @property secondProcedence
        @type {string}
    */
    private secondProcedence: string;
    /**
        Identificador de paciente venezolano
        @property isVenezuelan
        @type {boolean}
    */
    private isVenezuelan: boolean;
    /**
        Identificador de paciente brasilero
        @property isBrazilian
        @type {boolean}
    */
   private isBrazilian: boolean;
   /**
        Listado de consultas asociadas al paciente
        @property consultations
        @type {any}
    */
    private consultations: any;
    /**
        Fecha de creación de registro
        @property dateCreated
        @type {string}
    */
    private dateCreated: string;
    /**
         Fecha de modificación de registro
        @property dateModified
        @type {string}
    */
    private dateModified: string;

    /**
        Método constructor que crea un perfil de indígena vacío
        @method constructor
        @param {string} id Identificador de indígena
        @param {string} date Fecha de creación
    */
    public constructor(id: string, date?: string){
        let actualDate = moment().utc().format("YYYY-MM-DD HH:mm:ss");
        this.dateCreated = date ? date : actualDate;
        this.dateModified = actualDate;
        this.indianId = id;
        this.name = null;
        this.yearBorn = null;
        this.communityList = "0";
        this.communityText = null;
        this.gender = "0";
        this.height = null;
        this.firstProcedence = "0";
        this.secondProcedence = "0";
        this.newCommunity = false;
        this.isVenezuelan = true;
        this.isBrazilian = false;
        this.consultations = [];
    }
    /**
        Método que obtiene el identificador del indígena
        @method getId
        @return {string} Identificador del indígena
    */
    public getId(): string{
        return this.indianId;
    }
    /**
        Método que obtiene la fecha de creación
        @method getDateCreated
        @return {string} Fecha de creación
    */
    public getDateCreated(): string{
        return this.dateCreated;
    }
    /**
        Método que obtiene la fecha de modificación
        @method getDateModified
        @return {string} Fecha de modificación
    */
    public getDateModified(): string{
        return this.dateModified;
    }
    /**
        Método que obtiene el nombre del indígena
        @method getName
        @return {string} Nombre del indígena
    */
    public getName(): string{
        return this.name;
    }
    /**
        Método que modifica el nombre del indígena
        @method setName
        @param {string} name Nuevo nombre del indígena
    */
    public setName(name: string): void{
        this.name = name;
    }
    /**
        Método que obtiene el año de nacimiento
        @method getYear
        @return {number} Año de nacimiento
    */
    public getYear(): number{
        return this.yearBorn;
    }
    /**
        Método que modifica el año de nacimiento
        @method setYear
        @param {number} year Año de nacimiento
    */
    public setYear(year: number): void{
        this.yearBorn = year;
    }
    /**
        Método que obtiene la comunidad enlistada a la que pertenece el indígena
        @method getCommunityList
        @return {string} Comunidad enlistada a la que pertenece el indígena
    */
    public getCommunityList(): string{
        return this.communityList;
    }
    /**
        Método que modifica la comunidad enlistada a la que pertenece el indígena
        @method setCommunityList
        @param {string} community Nueva comunidad enlistada
    */
    public setCommunityList(community: string): void{
        this.communityList = community;
    }
    /**
        Método que obtiene la comunidad nueva a la que pertenece el indígena
        @method getCommunityText
        @return {string} Comunidad nueva a la que pertenece el indígena
    */
    public getCommunityText(): string{
        return this.communityText;
    }
    /**
        Método que modifica la comunidad nueva a la que pertenece el indígena
        @method setCommunityText
        @param {string} community Nueva comunidad (nombre)
    */
    public setCommunityText(community: string): void{
        this.communityText = community;
    }    
    /**
        Método que obtiene el género del indígena
        @method getGender
        @return {string} Género del indígena
    */
    public getGender(): string{
        return this.gender;
    }
    /**
        Método que modifica el género del indígena
        @method setGender
        @param {string} gender Género
    */
    public setGender(gender: string): void{
        this.gender = gender;
    }
    /**
        Método que obtiene la estatura del indígena
        @method getHeight
        @return {number} Estatura del indígena
    */
    public getHeight(): number{
        return this.height;
    }
    /**
        Método que modifica la estatura del indígena
        @method setHeight
        @param {number} height Nueva estatura
    */
    public setHeight(height: number): void{
        this.height = height;
    }
    /**
        Método que obtiene la primera división territorial de procedencia
        @method getFirstProcedence
        @return {string} Primera división territorial de procedencia
    */
    public getFirstProcedence(): string{
        return this.firstProcedence;
    }
    /**
        Método que modifica la primera división territorial
        @method setFirstProcedence
        @param {string} procedence Primera división territorial
    */
    public setFirstProcedence(procedence: string): void{
        this.firstProcedence = procedence;
    }
    /**
        Método que obtiene la segunda división territorial de procedencia
        @method getSecondProcedence
        @return {string} Segunda división territorial de procedencia
    */
    public getSecondProcedence(): string{
        return this.secondProcedence;
    }
    /**
        Método que modifica la segunda división territorial
        @method setSecondProcedence
        @param {string} procedence Segunda división territorial
    */
    public setSecondProcedence(procedence: string): void{
        this.secondProcedence = procedence;
    }
    /**
        Método que obtiene el indicador de nueva comunidad
        @method getNewCommunity
        @return {boolean} Indicador de nueva comunidad
    */
    public getNewCommunity(): boolean{
        return this.newCommunity;
    }
    /**
        Método que modifica el indicador de nueva comunidad
        @method setNewCommunity
        @param {boolean} newCommunity Nuevo indicador
    */
    public setNewCommunity(newCommunity: boolean): void{
        this.newCommunity = newCommunity;
    }
    /**
        Método que obtiene el indicador de nacionalidad venezolana
        @method getIsVenezuelan
        @return {boolean} Indicador de paciente venezolano
    */
    public getIsVenezuelan(): boolean{
        return this.isVenezuelan;
    }
    /**
        Método que modifica el indicador de nacionalidad venezolana
        @method setIsVenezuelan
        @param {boolean} ve Nueva nacionalidad
    */
    public setIsVenezuelan(ve: boolean): void{
        this.isVenezuelan = ve;
    }
    /**
        Método que obtiene el indicador de nacionalidad brasilera
        @method getIsBrazilian
        @return {boolean} Indicador de paciente brasilero
    */
    public getIsBrazilian(): boolean{
        return this.isBrazilian;
    }
    /**
        Método que modifica el indicador de nacionalidad brasilera
        @method setIsBrazilian
        @param {strinbooleang} br Nueva nacionalidad
    */
    public setIsBrazilian(br: boolean): void{
        this.isBrazilian = br;
    }
/**
        Método que obtiene el listado de consultas del paciente
        @method getConsultations
        @return {any} Lista de consultas
    */
    public getConsultations(): any{
        return this.consultations;
    }
    /**
        Método que modifica el listado de consultas
        @method setConsultations
        @param {any} consultations Lista de consultas
    */
    public setConsultations(consultations: any): void{
        this.consultations = consultations;
    }
    /** 
        Método que clona el objeto actual
        @method clone
        @return {any} Copia de la clase
    */
    public clone(){
        let newElem = new Indian(this.getId(), this.getDateCreated());
        newElem.setConsultations(this.getConsultations());
        newElem.setIsBrazilian(this.getIsBrazilian().toString() == "true");
        newElem.setSecondProcedence(this.getSecondProcedence());
        newElem.setIsVenezuelan(this.getIsVenezuelan().toString() == "true");
        newElem.setNewCommunity(this.getNewCommunity().toString() == "true");
        newElem.setFirstProcedence(this.getFirstProcedence());
        newElem.setHeight(this.getHeight());
        newElem.setGender(this.getGender());
        newElem.setCommunityText(this.getCommunityText());
        newElem.setCommunityList(this.getCommunityList());
        newElem.setYear(this.getYear());
        newElem.setName(this.getName());
        return newElem;
    }
}