import * as moment from 'moment';

/**
    Clase que maneja los datos asociados a una observación de visita
    @class Observation
    @author Abdalah Masrie <abdalah.masrie@sigis.com.ve>
    @copyright SIGIS Soluciones Integrales GIS, C.A
    created on 2018/09/18
*/
export class Observation {
    /**
        Identificador del médico
        @property doctorId
        @type {string}
    */
    private doctorId: string;
    /**
        Fecha de creación de registro
        @property dateCreated
        @type {string}
    */
    private dateCreated: string;
    /**
        Fecha de modificación de registro
        @property dateModified
        @type {string}
    */
    private dateModified: string;
    /**
        Localización detectada
        @property location
        @type {any}
    */
    private location: any;
    /**
        Observaciones generales
        @property observations
        @type {string}
    */
    private observations: string;
    /**
        Identificador de tipo de observación
        @property type
        @type {number}
    */
    private type: number;

    /**
        Método constructor que crea un perfil de indígena vacío
        @method constructor
        @param {string} id Identificador del médico
        @param {string} date Fecha de creación
    */
    public constructor(id: string, date?: string){
        let actualDate = moment().utc().format("YYYY-MM-DD HH:mm:ss");
        this.doctorId = id;
        this.dateCreated = date ? date : actualDate;
        this.dateModified = actualDate.toString();
        this.location = null;
        this.observations = null;
        this.type = -1;
    }
    /**
        Método que obtiene el identificador del médico
        @method getId
        @return {string} Identificador del médico
    */
    public getId(): string{
        return this.doctorId;
    }
    /**
        Método que obtiene la fecha de creación
        @method getDateCreated
        @return {string} Fecha de creación
    */
    public getDateCreated(): string{
        return this.dateCreated;
    }
    /**
        Método que obtiene la fecha de modificación
        @method getDateModified
        @return {string} Fecha de modificación
    */
    public getDateModified(): string{
        return this.dateModified;
    }
    /**
        Método que obtiene la lcoalización
        @method getLocation
        @return {any} Localización
    */
    public getLocation(): any{
        return this.location;
    }
    /**
        Método que modifica la localización
        @method setLocation
        @param {any} loc Nueva localización
    */
    public setLocation(loc: string): void{
        this.location = loc;
    }
    /**
        Método que obtiene las observaciones
        @method getObservations
        @return {string} Observaciones
    */
    public getObservations(): string{
        return this.observations;
    }
    /**
        Método que modifica las observaciones
        @method setObservations
        @param {string} observations Nuevas observaciones
    */
    public setObservations(observations: string): void{
        this.observations = observations;
    }
    /**
        Método que obtiene el identificador de tipo
        @method getType
        @return {number} Identificador de tipo
    */
    public getType(): number{
        return this.type;
    }
    /**
        Método que modifica el identificador de tipo
        @method setType
        @param {number} type Nuevo identificador de tipo
    */
    public setType(type: number): void{
        this.type = type;
    }

}