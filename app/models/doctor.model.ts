import * as moment from 'moment';

/**
    Clase que maneja los datos asociados al médico
    @class Doctor
    @author Abdalah Masrie <abdalah.masrie@sigis.com.ve>
    @copyright SIGIS Soluciones Integrales GIS, C.A
    created on 2018/10/25
*/

export class Doctor {
    /**
        Correo del médico
        @property email
        @type {string}
    */
    private email: string;
    /**
        Fecha de creación de registro
        @property dateCreated
        @type {string}
    */
    private dateCreated: string;
    /**
        Fecha de modificación de registro
        @property dateModified
        @type {string}
    */
    private dateModified: string;
    /**
        Nombres del médico
        @property firstName
        @type {string}
    */
    private firstName: string;
    /**
        Apellidos del médico
        @property lastName
        @type {string}
    */
    private lastName: string;

    /**
        Método constructor que crea un perfil de médico vacío
        @method constructor
        @param {any} user Datos del usuario
    */
    public constructor(user: any){
        let actualDate = moment().utc().format("YYYY-MM-DD HH:mm:ss");
        this.dateCreated = user.data["fec_cre"];
        this.dateModified = actualDate.toString();
        this.email = user["_id"];
        this.firstName = user.data["nom_usu"];
        this.lastName = user.data["ape_usu"];
        /*this.fingerPrints = [];
        let fingers = user.data["hue_usu"];
        for(var i = 0; i < fingers.length; i++){
            this.fingerPrints.push(fingers[i] ? true : false);
        }*/
    }
    /**
        Método que obtiene el correo electrónico del médico
        @method getEmail
        @return {string} Correo del médico
    */
    public getEmail(): string{
        return this.email;
    }
    /**
        Método que obtiene la fecha de creación
        @method getDateCreated
        @return {string} Fecha de creación
    */
    public getDateCreated(): string{
        return this.dateCreated;
    }
    /**
        Método que obtiene la fecha de modificación
        @method getDateModified
        @return {string} Fecha de modificación
    */
    public getDateModified(): string{
        return this.dateModified;
    }
    /**
        Método que obtiene el nombre del médico
        @method getFirstName
        @return {string} Nombre del médico
    */
    public getFirstName(): string{
        return this.firstName;
    }
    /**
        Método que modifica el nombre del médico
        @method setFirstName
        @param {string} firstName Nuevo nombre
    */
    public setFirstName(firstName: string): void{
        this.firstName = firstName;
    }
    /**
        Método que obtiene el apellido del médico
        @method getLastName
        @return {string} Apellido del médico
    */
    public getLastName(): string{
        return this.lastName;
    }
    /**
        Método que modifica el apellido del médico
        @method setLastName
        @param {string} lastName Nuevo apellido
    */
    public setLastName(lastName: string): void{
        this.lastName = lastName;
    }

}