import * as moment from 'moment';

/**
    Clase que maneja los datos asociados a una consulta de un indígena
    @class Consultation
    @author Abdalah Masrie <abdalah.masrie@sigis.com.ve>
    @copyright SIGIS Soluciones Integrales GIS, C.A
    created on 2018/07/20
*/
export class Consultation {
    /**
        Identificador de indígena
        @property indianId
        @type {string}
    */
    private indianId: string;
    /**
        Fecha de creación de registro
        @property dateCreated
        @type {string}
    */
    private dateCreated: string;
    /**
        Fecha de modificación de registro
        @property dateModified
        @type {string}
    */
    private dateModified: string;
    /**
        Localización detectada
        @property location
        @type {any}
    */
    private location: any;
    /**
        Indicador de embarazo
        @property isPregnant
        @type {boolean}
    */
    private isPregnant: boolean;
    /**
        Indicador de primera semana de lactancia
        @property fstWeekLactancy
        @type {boolean}
    */
    private fstWeekLactancy: boolean;
    /**
        Dosis a ser administrada
        @property dose
        @type {string}
    */
    private dose: string;
    /**
        Indicador de renuencia al tratamiento
        @property treatmentReluctant
        @type {boolean}
    */
    private treatmentReluctant: boolean;
    /**
        Indicador de enfermo grave
        @property seriouslySick
        @type {boolean}
    */
   private seriouslySick: boolean;
    /**
        Observaciones generales
        @property observations
        @type {string}
    */
    private observations: string;
    /**
        Identificador del médico
        @property doctor
        @type {string}
    */
    private doctor: string;
    /** 
        Identificador del medicamento empleado
        @property medicine
        @type {string}
    */
    private medicine: string;
    /** 
        Identificador de huella usada
        @property finger
        @type {string}
    */
    private finger: string;
    /** 
        Porcentaje de coincidencia
        @property match
        @type {string}
    */
    private match: string;

    /**
        Método constructor que crea un perfil de indígena vacío
        @method constructor
        @param {string} id Identificador de indígena
        @param {string} doctor Nombre del médico
        @param {string} finger Identificador de huella
        @param {string} match Porcentaje de coincidencia de huella
        @param {string} date Fecha de creación
    */
    public constructor(id: string, doctor: string, finger: string, match: string, date?: string){
        let actualDate = moment().utc().format("YYYY-MM-DD HH:mm:ss");
        this.indianId = id;
        this.dateCreated = date ? date : actualDate;
        this.dateModified = actualDate;
        this.finger = finger ? finger : null;
        this.match = match ? match : null;
        this.location = null;
        this.isPregnant = false;
        this.fstWeekLactancy = false;
        this.dose = null;
        this.treatmentReluctant = false;
        this.seriouslySick = false;
        this.medicine = "-1";
        this.observations = null;
        this.doctor = doctor;
    }
    /**
        Método que obtiene el identificador del indígena
        @method getId
        @return {string} Identificador del indígena
    */
    public getId(): string{
        return this.indianId;
    }
    /**
        Método que obtiene el identificador de la huella
        @method getFinger
        @return {string} Identificador de la huella
    */
    public getFinger(): string{
        return this.finger;
    }
    /**
        Método que obtiene el porcentaje de coincidencia de la huella
        @method getMatch
        @return {string} Porcentaje de coincidencia de la huella
    */
    public getMatch(): string{
        return this.match;
    }
    /**
        Método que obtiene la fecha de creación
        @method getDateCreated
        @return {string} Fecha de creación
    */
    public getDateCreated(): string{
        return this.dateCreated;
    }
    /**
        Método que obtiene la fecha de modificación
        @method getDateModified
        @return {string} Fecha de modificación
    */
    public getDateModified(): string{
        return this.dateModified;
    }
    /**
        Método que obtiene la lcoalización
        @method getLocation
        @return {any} Localización
    */
    public getLocation(): any{
        return this.location;
    }
    /**
        Método que modifica la localización
        @method setLocation
        @param {any} loc Nueva localización
    */
    public setLocation(loc: string): void{
        this.location = loc;
    }
    /**
        Método que obtiene el indicador de embarazo
        @method getIsPregnant
        @return {boolean} Indicador de embarazo
    */
    public getIsPregnant(): boolean{
        return this.isPregnant;
    }
    /**
        Método que modifica el indicador de embarazo
        @method setIsPregnant
        @param {boolean} pregnant Nuevo indicador
    */
    public setIsPregnant(pregnant: boolean): void{
        this.isPregnant = pregnant;
    }
    /**
        Método que obtiene el indicador de lactancia
        @method getLactancy
        @return {boolean} Indicador de lactancia
    */
    public getLactancy(): boolean{
        return this.fstWeekLactancy;
    }
    /**
        Método que modifica el indicador de lactancia
        @method setLactancy
        @param {boolean} lactancy Nuevo indicador
    */
    public setLactancy(lactancy: boolean): void{
        this.fstWeekLactancy = lactancy;
    }
    /**
        Método que obtiene la dosis
        @method getDose
        @return {string} Dosis
    */
    public getDose(): string{
        return this.dose;
    }
    /**
        Método que modifica la dosis
        @method setDosis
        @param {string} dose Nueva dosis
    */
    public setDose(dose: string): void{
        this.dose = dose;
    }
    /**
        Método que obtiene la indicador de renuencia
        @method getReluctance
        @return {boolean} Renuencia
    */
    public getReluctance(): boolean{
        return this.treatmentReluctant;
    }
    /**
        Método que modifica campo de renuencia
        @method setReluctance
        @param {boolean} rel Renuencia a tratamiento
    */
    public setReluctance(rel: boolean): void{
        this.treatmentReluctant = rel;
    }
    /**
        Método que obtiene la indicador de enfermo grave
        @method getSick
        @return {boolean} Enfermedad
    */
    public getSick(): boolean{
        return this.seriouslySick;
    }
    /**
        Método que modifica campo de enfermo grave
        @method setSick
        @param {boolean} enf Enfermo grave
    */
    public setSick(enf: boolean): void{
        this.seriouslySick = enf;
    }
    /**
        Método que obtiene la identificador de medicamento
        @method getMedicine
        @return {string} Medicamento
    */
    public getMedicine(): string{
        return this.medicine;
    }
    /**
        Método que modifica campo de medicamento
        @method setMedicine
        @param {string} medicine Nuevo identificador de medicamento
    */
    public setMedicine(medicine: string): void{
        this.medicine = medicine;
    }    
    /**
        Método que obtiene las observaciones
        @method getObservations
        @return {string} Observaciones
    */
    public getObservations(): string{
        return this.observations;
    }
    /**
        Método que modifica las observaciones
        @method setObservations
        @param {string} observations Nuevas observaciones
    */
    public setObservations(observations: string): void{
        this.observations = observations;
    }
    /**
        Método que obtiene el identificador del médico
        @method getDoctor
        @return {string} doctor
    */
    public getDoctor(): string{
        return this.doctor;
    }
    /** 
        Método que clona el objeto actual
        @method clone
        @return {any} Copia de la clase
    */
    public clone(){
        let newElem = new Consultation(this.getId(), this.getDoctor(), this.getFinger(), this.getMatch(), this.getDateCreated());
        newElem.setIsPregnant(this.getIsPregnant().toString() == "true");
        newElem.setLactancy(this.getLactancy().toString() == "true");
        newElem.setObservations(this.getObservations());
        newElem.setReluctance(this.getReluctance().toString() == "true");
        newElem.setSick(this.getSick().toString() == "true");
        newElem.setLocation(this.getLocation());
        newElem.setDose(this.getDose());
        newElem.setMedicine(this.getMedicine());
        return newElem;
    }
}