import { NgModule } from "@angular/core";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { Routes } from "@angular/router";

import { LoginComponent } from "./components/login/login.component";
import { DrawerComponent } from "./components/drawer/drawer.component";
import { PatientOptionsComponent } from "./components/patientOptions/patient-options.component";
import { IndianProfileComponent } from "./components/indianProfile/indian-profile.component";
import { DoctorProfileComponent } from "./components/doctorProfile/doctor-profile.component";
import { ConsultationComponent } from "./components/consultation/consultation.component";
import { ConsultationListComponent } from "./components/consultationList/consultation-list.component";
import { FAQComponent } from "./components/faq/faq.component";
import { AboutComponent } from "./components/about/about.component";
import { VisitObservationComponent } from "./components/visitObservation/visitObservation.component";
import { VisitObservationListComponent } from "./components/visitObservationList/visitObservationList.component";

import { SyncComponent } from "./components/sync/sync.component";
import { MapComponent } from "./components/map/map.component";

const routes: Routes = [
    { path: "", redirectTo: "/login", pathMatch: "full" },
    { path: "login", component: LoginComponent },
    { path: "drawer", component: DrawerComponent },
    { path: "patientOptions/:id", component: PatientOptionsComponent },
    { path: "profile", component: DoctorProfileComponent },
    { path: "indianProfile/:id", component: IndianProfileComponent },
    { path: "consultation/:id/:consultation", component: ConsultationComponent },
    { path: "consultationList/:id", component: ConsultationListComponent },
    { path: "faq", component: FAQComponent },
    { path: "about", component: AboutComponent },
    { path: "visitObservation/:id", component: VisitObservationComponent },
    { path: "visitObservationList", component: VisitObservationListComponent },
    { path: "sync", component: SyncComponent },
    { path: "map", component: MapComponent }
  ];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }