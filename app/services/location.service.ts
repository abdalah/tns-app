import { Injectable } from "@angular/core";
import * as geolocation from "nativescript-geolocation";
import { Accuracy } from "ui/enums"; // used to describe at what accuracy the location should be get

/**
	Clase que funcionalidades relacionadas al plugin de geolocalización
	@class LocationService
	@author Abdalah Masrie <abdalah.masrie@sigis.com.ve>
	@copyright SIGIS Soluciones Integrales GIS, C.A
	created on 2018/07/20
*/

@Injectable()
export class LocationService {
	/**
		Método constructor
		@method constructor
	*/
	public constructor(){

	}

	/** 
		Método que solicita la activación del sistema de ubicación
		@method enableLocationRequest
	*/
	public enableLocationRequest(){
		return geolocation.enableLocationRequest();
	}

	/** 
		Función que obtiene la distancia entre dos puntos
		@method getDistance
		@param {any} point1 Punto de inicio
		@param {any} point2 Punto de fin
		@return {number} Distancia en metros
	*/
	public getDistance(point1, point2){
		return geolocation.distance(point1, point2);
	}
		
	/**
		Método que verifica si se tiene permiso de GPS o lo solicita. Posteriormente, llama a la función que obtiene la ubicación
		@method getLocation
		@return {Promise}
	*/
	public getLocation(): Promise<any> {
		return new Promise(
			(resolve, reject) => {
				geolocation.isEnabled().then(available => {
					if (!available) {
						this.enableLocationRequest();
						this._getCurrentLocation()
							.then(resolve)
							.catch(reject);
					}else{
						this._getCurrentLocation()
							.then(resolve)
							.catch(reject);
					}
				})
			}
		);
	}

	/**
		Método privado que obtiene la ubicación geográfica
		@method _getCurrentLocation
		@return {Promise}
	*/
	private _getCurrentLocation(): Promise<any> {
		return new Promise(
			(resolve, reject) => {
				geolocation.isEnabled().then(available => {
					if(available){
						geolocation.getCurrentLocation({
							desiredAccuracy: Accuracy.any,
							timeout: 30000
						})
						.then(loc => {
							let location = {};
							location["altitude"] = loc.altitude;
							location["direction"] = loc.direction;
							location["horizontalAccuracy"] = loc.horizontalAccuracy;
							location["latitude"] = loc.latitude;
							location["longitude"] = loc.longitude;
							location["speed"] = loc.speed;
							location["verticalAccuracy"] = loc.verticalAccuracy;
							location["timestamp"] = loc.timestamp;
							for(var item in location){
								location[item] = location[item].toString();
							}
							resolve(location);
						})
						.catch(error => {
							reject(error);
						})
					}else{
						reject();
					}
				});
			}
		);
	}

}
