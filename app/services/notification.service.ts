import { Injectable } from "@angular/core";
import { LocalNotifications } from "nativescript-local-notifications";

/**
	Clase que maneja las funciones genéricas de manejo de notificaciones
	@class NotificationService
	@author Abdalah Masrie <abdalah.masrie@sigis.com.ve>
	@copyright SIGIS Soluciones Integrales GIS, C.A
	created on 2019/05/23
*/

@Injectable()
export class NotificationService {

	/**
		Método constructor que crea una base de datos local
		@method constructor
	*/
	public constructor() {}

    /**
        Método que crea una nueva notificación
        @method createNotification
        @param {number} id Identificador de notificación
        @param {string} title Título de la notificación
    */
    public createNotification(id, title){
        return new Promise(
			(resolve, reject) => {
                this._getPermission()
                .then( () => {
                    LocalNotifications.schedule([{
                        id: id,
                        title: title,
                        forceShowWhenInForeground: true
                    }]).then(
                        () => {
                            resolve();
                        },
                        (_) => {
                            reject("generic_error_message");
                        }
                    );
                })
                .catch(reject);
            }
        );
    }

    /**
        Método que obtiene los permisos para notificaciones
        @method getPermission
    */
    private _getPermission(){
        return new Promise(
			(resolve, reject) => {
                LocalNotifications.requestPermission().then(
                    (granted) => {
                        if(granted){
                            resolve();
                        }else{
                            reject("no_notification_permission");
                        }
                    }
                )
            }
        );
    }

    /**
        Método que agrega un evento sobre una notificación
        @method addEvent
        @param {any} cb Función callback 
    */
    public addEvent(cb){
        return new Promise(
			(resolve, _) => {
                LocalNotifications.addOnMessageReceivedCallback(
                    (notification) => {
                        cb(notification.id);
                    }
                ).then(
                    () => {
                        resolve();
                    }
                )
            }
        );
    }

    /**
        Método que finaliza una notificación
        @method cancelNotification
        @param {number} id Identificador de notificación
    */
    public cancelNotification(id){
        return new Promise(
			(resolve, reject) => {
                LocalNotifications.cancel(id).then(
                    (foundAndCanceled) => {
                        if(foundAndCanceled){
                            resolve();
                        }else{
                            reject();
                        }
                    }
                )        
            }
        );
    }
}