import { Injectable } from "@angular/core";
import * as camera from "nativescript-camera";
import {screen} from "platform"

/**
	Clase que posee funcionalidades relacionadas al plugin de cámara
	@class CameraService
	@author Abdalah Masrie <abdalah.masrie@sigis.com.ve>
	@copyright SIGIS Soluciones Integrales GIS, C.A
	created on 2019/02/25
*/

@Injectable()
export class CameraService {
	/**
		Método constructor
		@method constructor
	*/
    constructor(){};

    /**
        Método que permite tomar una fotografía
        @method takePicture
        @return {any} Representación de la imagen o error en caso contrario
    */
    public takePicture(){
        let options = { 
            width: 150, 
            height: 150, 
            keepAspectRatio: true, 
            saveToGallery: false 
        };
		return new Promise(
			(resolve, reject) => {
                if(camera.isAvailable()){
                    camera.requestPermissions().then( _ => {
                        camera.takePicture(options).then(picture => {
                            resolve(picture);
                        }, error => {
                            console.error(error);
                            reject("camera_picture_error");
                        });
                    }, _ => {
                        reject("camera_no_permission");
                    });    
                }else{
                    reject("camera_not_available");
                }
			}
		);
    }
}
