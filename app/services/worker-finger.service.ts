require("globals");
import { CouchbaseService } from "./couchbase.service";
import { Hf4000pFingerprint } from 'nativescript-hf4000p-fingerprint';
const context: Worker = self as any;

/**
    Hilo que se encarga de comparar las huellas registradas
    @author Abdalah Masrie <abdalah.masrie@sigis.com.ve>
    @copyright SIGIS Soluciones Integrales GIS, C.A
    created on 2019/02/15
*/
context.onmessage = (msg)=> {
    let cb = new CouchbaseService();
    let request = msg.data;
    let fingerprintDoc = request.fingerprintDoc;
    let fingerId = request.fingerId;
    let fingerPrintObject = new Hf4000pFingerprint();
    let fingerPrintStr = request.fingerPrintStr;
    let found = request.found;
    let patientIdFound;
    let comparison, finger;
    for(var i = 0 ; i < fingerprintDoc.data.length; i++){
        finger = cb.getDocument(fingerprintDoc.data[i]);
        //si se consigue la huella que coincide, termina el ciclo
        if(finger && (!fingerId || (fingerId && fingerId != finger._id))){
            comparison = fingerPrintObject.fingerCompare(finger.data.huella, fingerPrintStr);
            if(comparison >= 50){
                found = true;
                patientIdFound = finger.data.pac_id;
                break;
            }
        }
    }
    context.postMessage({
        found: found,
        patientIdFound: patientIdFound,
        finger: found ? finger._id : null,
        match: found ? comparison : null
    });
};

context.onerror = function(err){
    console.log(err);
}