import { Injectable } from "@angular/core";
import { Couchbase } from "nativescript-couchbase";
import * as connection from "tns-core-modules/connectivity";
import { isAndroid } from "platform";

/**
	Clase que maneja las funciones genéricas de comunicación a Couchbase por medio de Sync Gateway
	@class CouchbaseService
	@author Abdalah Masrie <abdalah.masrie@sigis.com.ve>
	@copyright SIGIS Soluciones Integrales GIS, C.A
	created on 2018/06/15
*/

@Injectable()
export class CouchbaseService {
	/**
		Objeto que representa a la base de datos
		@property database
		@type {any}
	*/
	private database: any;
	/**
		Objeto que manejará peticiones pull de Couchbase
		@property pull
		@type {any}
	*/    
	private pull: any;
	/**
		Objeto que manejará peticiones push de Couchbase
		@property push
		@type {any}
	*/    
	private push: any;

	/**
		Método constructor que crea una base de datos local
		@method constructor
	*/
	constructor() {
		console.log("enter constructor")
		this.init();
//		console.log("db exists ? ", CouchbaseService.database)
	}

	/**
		Método que inicia la base de datos
		@method init()
	*/
	public init(){
		if(!this.database) {
			console.log("enter init")
//			console.log("no db")
			this.database = new Couchbase("data");
//			console.log("new db ", CouchbaseService.database);
		}
	}

	/**
		Método que permite obtener la base de datos local generada
		@method getDatabase
		@return {any} Base de datos previamente generada
	*/
	public getDatabase() {
		return this.database;
	}

	/**
		Método que verifica cambios en la base de datos para actualizar localmente
		@method changeListener
	*/
	public changeListener(){
		console.log("listener called");
		this.database.addDatabaseChangeListener((changes) => {
			console.log("change detected ", changes);
		});
	}

	/**
		Método que inicia el proceso de obtención de datos de Couchbase
		@method startSyncPull
		@param {string} gateway Host, puerto y bucket a la que se conectará para obtener los datos
        @param {boolean} continuous Indica si se desea realizar sincronización continua
        @returns {Replicator} - Instancia que maneja estado el pull
	*/
	public startSyncPull(gateway: string, continuous: boolean) {
		this.pull = this.database.createPullReplication(gateway);
		this.pull.setContinuous(continuous);
        this.pull.start();
        return isAndroid ? this.pull.replicator : this.pull;
	}

	/**
		Método que inicia el proceso de traslado de datos a Couchbase
		@method startSyncPush
		@param {string} gateway Host, puerto y bucket a la que se conectará para pasar los datos
        @param {boolean} continuous Indica si se desea realizar sincronización continua
        @returns {Replicator} - Instancia que maneja estado el push
	*/
	public startSyncPush(gateway: string, continuous: boolean) {
		this.push = this.database.createPushReplication(gateway);
		this.push.setContinuous(continuous);
        this.push.start();
        return isAndroid ? this.push.replicator : this.push;
	}

	/**
		Método que finaliza el proceso de obtención de datos de Couchbase
		@method stopSyncPull
	*/
	public stopSyncPull() {
		this.pull.stop();
	}

	/**
		Método que finaliza el proceso de traslado de datos a Couchbase
		@method stopSyncPush
	*/
	public stopSyncPush() {
		this.push.stop();
	}

	/**
		Función que crea un documento nuevo
		@method createDocument
		@param {any} json Json con los valores a insertar
		@return {string} Identificador del documento
	*/
	public createDocument(json){
		return this.getDatabase().createDocument(json);
	}

	/**
		Función que obtiene un documento
		@method getDocument
		@param {string} id Identificador de documento a buscar
		@return {any} Objeto del documento
	*/
	public getDocument(id){
		return this.getDatabase().getDocument(id);
	}	

	/**
		Método que actualiza un documento
		@method updateDocument
		@param {string} id Identificador de documento a modificar
		@param {any} doc Documento con los valores a actualizar
	*/
	public updateDocument(id, doc){
		this.getDatabase().updateDocument(id, doc);
	}	

	/**
		Método que elimina un documento
		@method deleteDocument
		@param {string} id Identificador de documento a eliminar
	*/
	public deleteDocument(id){
		this.getDatabase().deleteDocument(id);
	}	

	/**
		Función que verifica si se ha establecido algún tipo de conectividad
		@method checkConnection
		@return {boolean} Indicador de conexión establecida (verdadero si está coenctado)
	*/
	public checkConnection(){
		let connectedTo = connection.getConnectionType();
		if(connectedTo == connection.connectionType.wifi || connectedTo == connection.connectionType.mobile){
			return true;
		}else{
			return false;
		}
	}

	/**
		Función asíncrona que crea un documento nuevo
		@method createDocumentDB
		@param {any} json Json con los valores a insertar
		@return {string} Identificador del documento
	*/
	public async createDocumentDB(json){
		return await new Promise(
			(resolve, reject) => {
				this._executeWorker(1, null, json)
				.then((value) => {
					resolve(value["data"]["returnValue"]);
				})
				.catch((err) => {
					reject(err);
				});
			}
		);
	}

	/**
		Función asíncrona que obtiene un documento
		@method getDocumentDB
		@param {string} id Identificador de documento a buscar
		@return {any} Objeto del documento
	*/
	public async getDocumentDB(id){
		return await new Promise(
			(resolve, reject) => {
				this._executeWorker(0, id, null)
				.then((value) => {
					resolve(value["data"]["returnValue"]);
				})
				.catch((err) => {
					reject(err);
				});
			}
		);
	}	

	/**
		Método asíncrona que actualiza un documento
		@method updateDocumentDB
		@param {string} id Identificador de documento a modificar
		@param {any} doc Documento con los valores a actualizar
	*/
	public async updateDocumentDB(id, doc){
		return await new Promise(
			(resolve, reject) => {
				this._executeWorker(2, id, doc)
				.then((_) => {
					resolve();
				})
				.catch((err) => {
					reject(err);
				});
			}
		);
	}	

	/**
		Método asíncrona que elimina un documento
		@method deleteDocumentDB
		@param {string} id Identificador de documento a eliminar
	*/
	public async deleteDocumentDB(id){
		return await new Promise(
			(resolve, reject) => {
				this._executeWorker(3, id, null)
				.then(() => {
					resolve();
				})
				.catch((err) => {
					reject(err);
				});
			}
		);
	}	

	/**
		Método privado que ejecuta el worker de operaciones
		@method _executeWorker
		@param {number} oepration Número de operación a ejecutar
		@param {string} id Identificador de documento, si aplica
		@param {any} data Contenido del documento, si aplica
		@param {any} cb Función callback
	*/
	private async _executeWorker(operation, id, data){
		return await new Promise(
			(resolve, reject) => {
				let worker = new Worker("./worker-database.service");
				worker.postMessage({
					operation: operation,
					id: id,
					json: data
				});
				worker.onerror = function(err) {
					console.log(err.message);
					reject(err.message);
				}
				worker.onmessage = (msg) => {
					resolve(msg);
					worker.terminate();
				};
			}
		)
	}

}