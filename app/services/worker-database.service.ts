require("globals");
const context: Worker = self as any;
import { CouchbaseService } from "./couchbase.service";

/**
    Hilo que se encarga de ejecutar operaciones básicas sobre base de datos
    @author Abdalah Masrie <abdalah.masrie@sigis.com.ve>
    @copyright SIGIS Soluciones Integrales GIS, C.A
    created on 2019/05/31
*/
context.onmessage = (msg)=> {
    try{
        let request = msg.data;
        let cbInstance = new CouchbaseService();
        let operation = request.operation;
        let json = request.json;
        let id = request.id;
        let returnValue = true;
        switch(operation){
            case 1:
                //inserción
                returnValue = cbInstance.createDocument(json);
                break;
            case 2:
                //actualización
                cbInstance.updateDocument(id, json);
                break;
            case 3:
                //eliminación
                cbInstance.deleteDocument(id);
                break;
            default:
                //búsqueda
                returnValue = cbInstance.getDocument(id);
        }
        context.postMessage({
            returnValue: returnValue
        });    
    }catch(err){
        console.log("found error: ", err)
        context.postMessage({
            returnValue: null
        });
    }
};

context.onerror = function(err){
    console.log(err);
}