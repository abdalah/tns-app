require("globals");
import { CouchbaseService } from "./couchbase.service";
const context: Worker = self as any;

/**
    Hilo que se encarga de cargar imágenes
    @author Abdalah Masrie <abdalah.masrie@sigis.com.ve>
    @copyright SIGIS Soluciones Integrales GIS, C.A
    created on 2019/05/06
*/
context.onmessage = (msg)=> {
    let cb = new CouchbaseService();
    let request = msg.data;
    let pictureId = request.id;
    let documentImage = pictureId ? cb.getDocument(pictureId) : null;
    context.postMessage({
        documentImage: documentImage
    });
};

context.onerror = function(err){
    console.log(err);
}