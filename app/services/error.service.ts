import { Injectable,ErrorHandler } from "@angular/core";
import * as Toast from 'nativescript-toast';
import { TranslateService } from 'ng2-translate';

/**
    Clase que maneja errores encontrados durante la ejecución del programa
    @class ErrorService
    @author Abdalah Masrie <abdalah.masrie@sigis.com.ve>
    @copyright SIGIS Soluciones Integrales GIS, C.A
    created on 2019/02/15
*/

@Injectable()
export class ErrorService implements ErrorHandler {

    /**
        Método constructor que crea una base de datos local
        @method constructor
    */  
    constructor(private translate: TranslateService){};
               
    /**
        Método que maneja el error encontrado
        @method handleError
        @param {any} error Objeto con el error encontrado
    */
    public handleError(error: any) {
        console.error("**ERROR** ",error);
        let msg;
        this.translate.get("generic_error_message").subscribe((res: string) => {
            msg = res;
        });
    
        Toast.makeText(msg).show();
    }  
}
