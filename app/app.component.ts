import { Component } from "@angular/core";
import * as Platform from "platform";
import { TranslateService } from 'ng2-translate';
import { CouchbaseService } from './services/couchbase.service';

@Component({
    selector: "ns-app",
    templateUrl: "app.component.html",
})

/**
	Clase que se ejecuta al iniciar la aplicación móvil. Posee configuraciones iniciales
	@class AppComponent
	@author Abdalah Masrie <abdalah.masrie@sigis.com.ve>
	@copyright SIGIS Soluciones Integrales GIS, C.A
	created on 2018/05/25
*/

export class AppComponent { 
	/**
		Método constructor
		@method constructor
		@param {TranslateService} translate Objeto que define la configuración de internacionalización.
		@param {CouchbaseService} couchbaseService Instancia de Couchbase
	*/
	constructor(private translate: TranslateService, private servi: CouchbaseService) {
		console.log("la mia db ", this.servi.getDatabase());
	    this.translate.setDefaultLang("en");
	    this.translate.use(Platform.device.language);
	}
}
