import { Injectable } from '@angular/core';

/**
    Clase singleton que contiene los datos fijos de configuración
    @class Config
    @author Abdalah Masrie <abdalah.masrie@sigis.com.ve>
    @copyright SIGIS Soluciones Integrales GIS, C.A
    created on 2018/06/15
*/

@Injectable()
export class Config {
    /**
        Configuraciones de conexión a Sync Gateway que conecta a Couchbase
        @property couchbase
        @type {string}
        @static
        @final
    */
	static couchbase = "xxxxxxxxxxxxxxxxxxxxxxxxx";

    /**
        Identificadores fijos para couchbase
        @property id
        @type {any}
        @static
        @final
    */
    static id = {
        "visits": "visitas",
        "areas": "areas_geograficas",
        "local_observations": "temp_observaciones",
        "local_patients": "temp_pacientes",
        "fingerprints": [
            "right_thumb_fingerprints",
            "left_thumb_fingerprints",
            "right_index_fingerprints",
            "left_index_fingerprints"
        ],
        "doctor_finger": "doctor_fingerprints",
        "oncocercosis": "0446ea81-cc85-4236-9633-1ff966a8aa64",
        "ivermectina": "88eb7292-c413-46fa-a722-7610a462ec1d"
    };

    /**
        Identificadores fijos para localStorage
        @property storage
        @type {any}
        @static
        @final
    */
    static storage = {
        "user": "user",
        "map_config": "map_config",
        "format": "format",
        "finger": "finger",
        "match": "match",
        "position": "position",
        "amazon": "amazon",
        "ve_gen": "ve_gen",
        "yanomami_br": "yanomami_br",
        "bolivar": "bolivar",
        "map_areas": "map_areas",
        "map_subareas": "map_subareas",
        "map_communities": "map_communities",
        "map_consultations": "map_consultations",
        "map_alerts": "map_alerts"
    };

    /** 
        Dirección a los marcadores
        @property markers
        @type {any}
        @static
        @final
    */
    static markers ={
        "blue": "res://blue_marker",
        "green": "res://green_marker",
        "red": "res://red_marker",
        "yellow": "res://yellow_marker"
    }

    /**
        Extent por default del mapa.
        @property defaultBounds
        @type {any}
        @static
        @final
    */
    static defaultBounds = {
        north: 14.605,
        east: -77.278,
        south: -1.670,
        west: -49.812
    };

    /**
        Token para el api de mapbox.
        @property
        @type string
        @static
        @final
    */
    static mapBoxTokenKey = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";
}
